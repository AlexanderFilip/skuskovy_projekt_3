#pragma once
#include <QtWidgets>
#include "Viz3D.h"
#include "float.h"

class Vizualizacia;
class GeomObjekt;
class Vertex;
class Face;
class HalfEdge;


struct HermitVektor {
	float Rot;
	float Dlzka;
};

struct Hrana {
	QPoint P0;		//zaciatocny bod
	QPoint P1;		//koncovy bod
	float w;		//obratena smernica
	int deltaY;
	float X;

	bool operator>(const Hrana& h) const {
		return P0.y() > h.P0.y();
	}
};

class ViewerWidget :public QWidget {
	Q_OBJECT
private:
	QString name = "";
	QSize areaSize = QSize(0, 0);
	QImage* img = nullptr;
	QRgb* data = nullptr;
	QPainter* painter = nullptr;
	QVector<QVector<zBufferUnit>> zBuffer;

public:
	ViewerWidget(QString viewerName, QSize imgSize, QWidget* parent = Q_NULLPTR);
	~ViewerWidget();
	void resizeWidget(QSize size);

	//Image functions
	bool setImage(const QImage& inputImg);
	QImage* getImage() { return img; };
	bool isEmpty();

	//Data functions
	QRgb* getData() { return data; }
	void setPixel(int x, int y, const QColor& color= QColor("black"));
	void setPixel(int x, int y, unsigned char r, unsigned char g, unsigned char b);
	//bool isInside(int x, int y) { return (x >= 0 && y >= 0 && x < img->width() && y < img->height()) ? true : false; }

	//Get/Set functions
	QString getName() { return name; }
	void setName(QString newName) { name = newName; }

	void setPainter() { painter = new QPainter(img); }
	void setDataPtr() { data = reinterpret_cast<QRgb*>(img->bits()); }

	int getImgWidth() { return img->width(); };
	int getImgHeight() { return img->height(); };

	void clear(QColor color = Qt::white);



	//KRESLIACE FUNKCIE 
	void KresliUseckuDDA(QPointF P1, QPointF P2, QColor* Color);
	void KresliUseckuBresen(QPointF P1, QPointF P2, QColor* Color);
	void KresliPolygonDDA(QVector<QPointF>* Sur, QColor* Color);
	void KresliPolygonBresen(QVector<QPointF>* Sur, QColor* Color);
	void KresliPolygonBresenBarycentric(QVector<zBufferUnit>* Sur, Vizualizacia* Kamera);
	void KresliPolygony(QList<QVector<zBufferUnit>>* Sur, QColor* Color, Vizualizacia* Kamera);
	void KresliBod(QPointF* P0, int* LineAlg);
	void KresliBod(QPoint* P0, int* LineAlg);
	void KresliBody(QVector<QPointF>* Sur, int* LineAlg);
	void KresliKrivkuHermit(QVector<QPointF>* Sur, QVector<HermitVektor>* HVektory, QColor* Color, int* LineAlg);
	void KresliKrivkuBezier(QVector<QPointF>* Sur, QColor* Color, int* LineAlg);
	void KresliKrivkuBspline(QVector<QPointF>* Sur, QColor* Color, int* LineAlg);
	void KresliZBuffer();

	void CheckZBuffer();

	void KresliVektory(QVector<QPointF>* Sur, QVector<HermitVektor>* HVektory,int* LineAlg);
	void Generuj(QVector<QPointF>* Sur, QColor* Color, int* Algoritmus, int Vypln, QColor* FillColor);
	void GenerujKrivku(QVector<QPointF>* Sur, QVector<HermitVektor>* HVektory, QColor* Color, int* CurveAlg, int* LineAlg);

	void Orezanie(QVector<QPointF>* Sur);
	void OrezaniePolygon(QVector<QPointF>* V);
	void OrezaniePolygon(QVector<zBufferUnit>* V, Vizualizacia* Kamera);
	void OrezanieUsecka(QVector<QPointF>* V);

	void FillScanLine(QVector<QPointF>* Sur, QColor* FillColor);
	void FillScanLineTriangle(QVector<QPointF>* Sur, int* Vyber);
	void FillScanLineTriangle(QVector<QPointF>* Sur, QVector<QColor>* Farby, QVector<double>* zValues, Vizualizacia* Kamera);
	void VyratajFarbuPix(QVector<QPointF>* Sur, QPointF* P, QColor* Color, int* Vyber, QVector<QColor>* Farby);
	void NearestNeighbor(QVector<QPointF>* Sur, QPointF* P, QColor& OutputColor, QVector<QColor>* Farby);
	bool CheckTriangleIntegrity(QVector<QPointF>* Sur);
	void Barycentric(QVector<QPointF>* Sur, QPointF* P, QColor& OutputColor, QVector<QColor>* Farby);

	//POMOCNE FUNKCIE
	void PointSwap(QPointF& P1, QPointF& P2);
	float SkalarSuc(QPointF* P1, QPointF* P2);
	float SkalarSuc(QPoint* P1, QPoint* P2);
	float max(float c1, float c2) { return (c1 >= c2) ? c1 : c2; };
	float min(float c1, float c2) { return (c1 <= c2) ? c1 : c2; };
	bool KontrolaPriesecnikov(QVector<QPointF>& V, QVector<QPointF>& E, QPointF& d);
	void PripravHrany(QVector<QPointF>* Vrcholy, QVector<Hrana>& hrany);
	void TrieditHranyY(QVector<Hrana>& hrany);
	void TrieditHranyX(QList<Hrana>& hrany);
	void UsporiadajTroj(QVector<QPointF>& Sur);

	void resetZBuffer();
	void MatchColors_Z(QVector<QPointF>* nove, QVector<QPointF>* povodne, QVector<QColor>* Farby, QVector<double>* zValues);
	//Pomocne pre farby
	float MinF(float a, float b) { if (a > b) return b; else return a; };
	int Min(int a, int b) { if (a > b) return b; else return a; };
	int Max(int a, int b) { if (a < b) return b; else return a; };
	double MinD(double a, double b) { if (a > b) return b; else return a; };

	double MaxD(double a, double b) { if (a < b) return b; else return a; };

	




public slots:
	void paintEvent(QPaintEvent* event) Q_DECL_OVERRIDE;
};