#include   "Viz3D.h"

Vizualizacia::Vizualizacia()
{
	rezim = 0;
	zenit = 0.0;
	azimut = 0.0;

	truescale = 200.0;
	scale = 1.0;
	transX = 0;
	transY = 0;

	stred = 20.0;
	clipFar = 2000.0;
	clipNear = 0.0;

	calc_n();
	calc_u();
	calc_v();

	//svetla
	Ambient.Rezim = false;
	Ambient.Farba = QColor(200,200,255);
	PointLight.Rezim = true;
	PointLight.Farba = QColor(255, 255, 128);
	PointLight.X = 2.0;
	PointLight.Y = 2.0;
	PointLight.Z = 2.0;
	
	rezimRender = 0;
	rezimShading = 0;

	//zScale = 0.15;
}

void Vizualizacia::calc_n()
{
	n.setX(qSin(zenit) * qCos(azimut));
	n.setY(qSin(zenit) * qSin(azimut));
	n.setZ(qCos(zenit));
}

void Vizualizacia::calc_u()
{
	u.setX(qSin(zenit + M_PI / 2) * qCos(azimut));
	u.setY(qSin(zenit + M_PI / 2) * qSin(azimut));
	u.setZ(qCos(zenit + M_PI / 2));
}

void Vizualizacia::calc_v()
{
	v = QVector3D::crossProduct(u, n);
}
