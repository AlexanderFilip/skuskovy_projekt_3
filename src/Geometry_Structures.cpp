#include "Geometry_Structures.h"

GeomObjekt::GeomObjekt()
{
	Vrcholy = nullptr;
	Steny = nullptr;
	Hrany = nullptr;

	Diffuse = QColor(128, 255, 128);
	Glossy = QColor(128, 128, 128);
	Ambient = QColor(32, 48, 48);
	Roughness = 3;
}

GeomObjekt::~GeomObjekt()
{
	Clear();
}

bool GeomObjekt::isNull()
{

	if (Vrcholy== nullptr || Hrany== nullptr || Steny== nullptr) {
		return true;
	}
	else {
		return false;
	}
}

void GeomObjekt::Clear()
{
	if (!(Vrcholy == nullptr)) Vrcholy->clear();
	if (!(Hrany == nullptr)) Hrany->clear();
	if (!(Steny == nullptr)) Steny->clear();

	Vrcholy = nullptr;
	Hrany = nullptr;
	Steny = nullptr;
}

void GeomObjekt::deleteHranyBatch(int g)
{
	for (int i = 0; i < 30 * 2 * pow(4,g); i++) {
		Hrany->removeFirst();
	}
}

void GeomObjekt::deleteStenyBatch(int g)
{
	for (int i = 0; i < 20 * pow(4, (g)); i++) {
		Steny->removeFirst();
	}
}

Vertex* GeomObjekt::DuplicitnyVrchol(Vertex* v, int g)
{
	double odchylka = 0.08;
	//skontroluje, ci existuje vrchol so zadanymi suradnicami, ak ano, vrati smernik nanho, ak nie, znova vrati vstupny smernik
	for (int i = 0; i < Vrcholy->size(); i++) {		
		if ((VzdialenostVrcholov(v, &(*Vrcholy)[i])) < odchylka) {
			return (&(*Vrcholy)[i]);
		}		
	}
	return v;
}

void GeomObjekt::normalizujVrcholy()
{
	int i;
	double dlzka;
	for (i = 0; i < Vrcholy->size(); i++) {
		dlzka = sqrt(((*Vrcholy)[i].getX() * (*Vrcholy)[i].getX()) + ((*Vrcholy)[i].getY() * (*Vrcholy)[i].getY()) + ((*Vrcholy)[i].getZ() * (*Vrcholy)[i].getZ()));
		(*Vrcholy)[i].setX((*Vrcholy)[i].getX() / dlzka);
		(*Vrcholy)[i].setY((*Vrcholy)[i].getY() / dlzka);
		(*Vrcholy)[i].setZ((*Vrcholy)[i].getZ() / dlzka);
	}
}

void GeomObjekt::preskalujVrcholy(double factor)
{
	int i;
	for (i = 0; i < Vrcholy->size(); i++) {
		(*Vrcholy)[i].setX((*Vrcholy)[i].getX() * factor);
		(*Vrcholy)[i].setY((*Vrcholy)[i].getY() * factor);
		(*Vrcholy)[i].setZ((*Vrcholy)[i].getZ() * factor);

	}
}

void GeomObjekt::najdiParoveHrany()
{
	int i,j;
	for (i = 0; i < Hrany->size(); i++) {

		if (!(*Hrany)[i].hasPair()) {
			for (j = 0; j < Hrany->size(); j++) {
				if ((*Hrany)[i].getBodIndex() == (*Hrany)[j].getHrana_next()->getBodIndex() && (*Hrany)[j].getBodIndex() == (*Hrany)[i].getHrana_next()->getBodIndex()) {
					(*Hrany)[i].setPair(&(*Hrany)[j]);
					(*Hrany)[j].setPair(&(*Hrany)[i]);
					break;
				}
			}
		}
	}
}

void GeomObjekt::CalculateVertexNormals() {

	int i, j;
	QVector3D u, v, n;

	//prejde cez vsetky plosky a vypocita im normaly
	for (i = 0; i < Steny->size(); i++) {
		u.setX((*Steny)[i].getHrana()->getBod()->getX() - (*Steny)[i].getHrana()->getHrana_next()->getBod()->getX());
		u.setY((*Steny)[i].getHrana()->getBod()->getY() - (*Steny)[i].getHrana()->getHrana_next()->getBod()->getY());
		u.setZ((*Steny)[i].getHrana()->getBod()->getZ() - (*Steny)[i].getHrana()->getHrana_next()->getBod()->getZ());

		v.setX((*Steny)[i].getHrana()->getBod()->getX() - (*Steny)[i].getHrana()->getHrana_prev()->getBod()->getX());
		v.setY((*Steny)[i].getHrana()->getBod()->getY() - (*Steny)[i].getHrana()->getHrana_prev()->getBod()->getY());
		v.setZ((*Steny)[i].getHrana()->getBod()->getZ() - (*Steny)[i].getHrana()->getHrana_prev()->getBod()->getZ());

		n = QVector3D::crossProduct(u,v);

		//potom pripocita tuto ploskovu normalu vsetkym trom jej vrcholom

		(*Steny)[i].getHrana()->getBod()->addNormal(n);
		(*Steny)[i].getHrana()->getHrana_next()->getBod()->addNormal(n);
		(*Steny)[i].getHrana()->getHrana_prev()->getBod()->addNormal(n);
	}


	//nakoniec prejde cez vsetky vrcholy a upravi ich normalove vektory na jednotkovu dlzku
	for (i = 0; i < Vrcholy->size(); i++) {
		(*Vrcholy)[i].unifyNormal();
	}

}


void GeomObjekt::CalculateVertexColors(Vizualizacia* Kamera)
{
	bool pocitajV = false;
	QVector3D N, L, R, V;
	QColor Is, Id, Ia, I, Output;
	int i;

	if (Kamera->getRezim() == 0) {
		//pri roznobeznom premietani pouzijem normalovy vektor priemetne
		V = - Kamera->get_n();
	}
	else {	
		//pri stredovom premietani vypocitam specialne pre kazdy bod
		pocitajV = true;
	}

	for (i = 0; i < Vrcholy->size(); i++) {
		//vektor N
		N = *(*Vrcholy)[i].getNormal();
		//vektor L
		L = CalcL(&(*Vrcholy)[i], Kamera);
		//vektor R
		R = 2 * QVector3D::dotProduct(L,N) * N - L;
		//vektor V
		if (pocitajV) {
			V =  CalcV(&(*Vrcholy)[i], Kamera);
		}

		//vypocitam zlozky a spojim ich dokopy
		Is = VypocitajIs(&V, &R, Kamera->getPointColor(), &Glossy, Roughness);
		Id = VypocitajId(&L, &N, Kamera->getPointColor(), &Diffuse);
		Ia = VypocitajIa(Kamera->getAmbientColor(), &Ambient);
		I = VypocitajI(&Is, &Id, &Ia);

		if (FarbyVrcholov.isEmpty()) {
			(*Vrcholy)[i].setColor(&I);
		}
		else {
			//vynasobim ziskanym produktom farbu vo vrchole
			Output.setRed(round((FarbyVrcholov[i].red() * I.red()) / 255.0));
			Output.setGreen(round((FarbyVrcholov[i].green() * I.green()) / 255.0));
			Output.setBlue(round((FarbyVrcholov[i].blue() * I.blue()) / 255.0));

			//nakoniec nastavim farbu pre vrchol
			(*Vrcholy)[i].setColor(&Output);
		}
	}
}
void GeomObjekt::CalculateVertexColors_Batymetry()
{
	int i;
	QColor C0(0, 0, 255);			//blue
	QColor C1(255, 255, 255);		//white
	QColor C2(255, 0, 0);			//red

	double C0_factor, C1_factor, C2_factor;
	FarbyVrcholov.resize(Vrcholy->size());

	//Pre kazdy vrchol vypocitam hodnotu podla vysky Z
	for (i = 0; i < Vrcholy->size(); i++) {

		C2_factor = (*Vrcholy)[i].getZ();		//zistim vzdialenost od nuly, aj smer

		//ak aj to kladny smer, dopocitam vzdialenost od 1
		if (C2_factor >= 0.0) {
			C0_factor = 0.0;
			C1_factor = 1.0 - C2_factor;
		}
		//ak je to zaporny smer, otocim hodnotu na kladnu a tiez dopocitam vzdialenost od 1 ... (-1)
		else {
			C0_factor = C2_factor * (-1.0);
			C2_factor = 0.0;
			C1_factor = 1.0 - C0_factor;
		}

		float Red = round(C0_factor * C0.red() + C1_factor * C1.red() + C2_factor * C2.red());
		float Green = round(C0_factor * C0.green() + C1_factor * C1.green() + C2_factor * C2.green());
		float Blue = round(C0_factor * C0.blue() + C1_factor * C1.blue() + C2_factor * C2.blue());

		FarbyVrcholov[i] = QColor(Red, Green, Blue);
		//(*Vrcholy)[i].setColor(&QColor(Red,Green,Blue));
	}

}

QVector3D GeomObjekt::CalcL(Vertex* P, Vizualizacia* Kamera)
{
	QVector3D L;
	
	L.setX(P->getX() - Kamera->getPointSurX());
	L.setY(P->getY() - Kamera->getPointSurY());
	L.setZ(P->getZ() - Kamera->getPointSurZ());
	L.normalize();

	return L;
}
QVector3D GeomObjekt::CalcV(Vertex* P, Vizualizacia* Kamera)
{
	//pri stredovom premietani pouzijem vektor od kamery po dany vrchol
	QVector3D v = Kamera->get_n() * Kamera->getStred();
	QVector3D result;
	result.setX(P->getX() - v.x());
	result.setY(P->getY() - v.y());
	result.setZ(P->getZ() - v.z());
	result.normalize();

	return result;
}

QColor GeomObjekt::VypocitajIs(QVector3D* V, QVector3D* R, QColor* Il, QColor* rs, double h)
{
	double Ssucin = QVector3D::dotProduct(*V, *R);
	if (Ssucin <= 0) {
		return QColor("black");
	}
	double nasobok = pow(Ssucin, h);

	QColor Is;
	Is.setRed(round((double)Il->red() * (double)rs->red()  * nasobok / 255.0));
	Is.setGreen(round(Il->green() * rs->green() * nasobok / 255.0));
	Is.setBlue(round(Il->blue() * rs->blue() * nasobok / 255.0));
	return Is;
}
QColor GeomObjekt::VypocitajId(QVector3D* L, QVector3D* N, QColor* Il, QColor* rd)
{
	double nasobok = QVector3D::dotProduct(*L, *N);
	if (nasobok <= 0) {
		return QColor("black");
	}
	QColor Id;
	Id.setRed(round((double)Il->red() * (double)rd->red() * nasobok / 255.0));
	Id.setGreen(round(Il->green() * rd->green() * nasobok / 255.0));
	Id.setBlue(round(Il->blue() * rd->blue() * nasobok / 255.0));
	return Id;
}
QColor GeomObjekt::VypocitajIa(QColor* Io, QColor* ra)
{
	QColor Ia;
	Ia.setRed(round((double)Io->red() * (double)ra->red()  / 255.0));
	Ia.setGreen(round(Io->green() * ra->green()  / 255.0));
	Ia.setBlue(round(Io->blue() * ra->blue()  / 255.0));
	return Ia;
}
QColor GeomObjekt::VypocitajI(QColor* Is, QColor* Id, QColor* Ia)
{
	QColor I;
	I.setRed(Min(Is->red() + Id->red() + Ia->red() , 255));
	I.setGreen(Min(Is->green() + Id->green() + Ia->green(), 255));
	I.setBlue(Min(Is->blue() + Id->blue() + Ia->blue(), 255));
	return I;
}



double GeomObjekt::VzdialenostVrcholov(Vertex* u, Vertex* v)
{
	double x2 = (u->getX() - v->getX()) * (u->getX() - v->getX());
	double y2 = (u->getY() - v->getY()) * (u->getY() - v->getY());
	double z2 = (u->getZ() - v->getZ()) * (u->getZ() - v->getZ());
	return sqrt(x2 + y2 + z2);
}

bool GeomObjekt::containsVrchol(Vertex v)
{
	return (Vrcholy->contains(v));
}

void GeomObjekt::ScaleZ(double factor)
{
	factor *= (50.0 / sqrt(Vrcholy->size()));

	for (int i = 0; i < Vrcholy->size(); i++) {
		(*Vrcholy)[i].setZ(HeightBuffer[i] * factor);
	}
}

void GeomObjekt::FillHeightBuffer()
{
	HeightBuffer.resize(Vrcholy->size());
	for (int i = 0; i < Vrcholy->size(); i++) {
		HeightBuffer[i] = (*Vrcholy)[i].getZ();
	}
}


HalfEdge::HalfEdge(Vertex* vo, Face* f, HalfEdge* ep, HalfEdge* en, HalfEdge* pr)
{
	vert_origin = vo;
	face = f;
	edge_prev = ep;
	edge_next = en;
	pair = pr;
}

void HalfEdge::setPoints(Vertex* vo, Vertex* ve, HalfEdge* pr)
{
	vert_origin = vo;
	vert_end = ve;
	pair = pr;
}

void HalfEdge::setAll(Vertex* vo, Face* f, HalfEdge* ep, HalfEdge* en, HalfEdge* pr)
{
	vert_origin = vo;
	face = f;
	edge_prev = ep;
	edge_next = en;
	pair = pr;
}

QString HalfEdge::PrintBodyHrany()
{
	return QString::number(vert_origin->getIndex()) + " " + QString::number(edge_next->getBodIndex()) + " ";
}

QString HalfEdge::PrintBodySteny()
{
	return QString::number(vert_origin->getIndex()) + " " + QString::number(edge_next->vert_origin->getIndex()) + " " + QString::number(edge_prev->vert_origin->getIndex()) + " ";
}

bool HalfEdge::hasFace()
{
	if (face == nullptr) return false;
	else return true;
}

bool HalfEdge::hasPair()
{
	if (pair == nullptr) {
		return false;
	}
	else {
		return true;
	}

}

bool Vertex::operator==(Vertex const& t)
{

	if ( x == t.x && y == t.y && z == t.z && index == t.index && edge == t.edge) {
		return true;
	}
	else {
		return false;
	}
}

QString Vertex::PrintSur()
{
	QString stringX = QString::number(x);		//konverzia z double na qstring
	QString stringY = QString::number(y);
	QString stringZ = QString::number(z);

	return stringX + " " + stringY + " " + stringZ + " ";
}


QString Face::PrintBody()
{
	return edge->PrintBodySteny();
}
