#include   "ViewerWidget.h"

ViewerWidget::ViewerWidget(QString viewerName, QSize imgSize, QWidget* parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	setMouseTracking(true);
	name = viewerName;
	if (imgSize != QSize(0, 0)) {
		img = new QImage(imgSize, QImage::Format_ARGB32);
		img->fill(Qt::white);
		resizeWidget(img->size());
		setPainter();
		setDataPtr();
	}
}
ViewerWidget::~ViewerWidget()
{
	delete painter;
	delete img;
}
void ViewerWidget::resizeWidget(QSize size)
{

	this->resize(size);
	this->setMinimumSize(size);
	this->setMaximumSize(size);

	//priprava zBuffera - nastavenie velkosti
	zBuffer.resize(img->width());
	for (int i = 0; i < img->width(); i++) {
		zBuffer[i].resize(img->height());
	}
}

//Image functions
bool ViewerWidget::setImage(const QImage& inputImg)
{
	if (img != nullptr) {
		delete img;
	}
	img = new QImage(inputImg);
	if (!img) {
		return false;
	}
	resizeWidget(img->size());
	setPainter();

	//priprava zBuffera - nastavenie velkosti
	zBuffer.resize(img->width());
	for (int i = 0; i < img->width(); i++) {
		zBuffer[i].resize(img->height());
	}

	update();

	return true;
}
bool ViewerWidget::isEmpty()
{
	if (img->size() == QSize(0, 0)) {
		return true;
	}
	return false;
}

//Data function
void ViewerWidget::setPixel(int x, int y, const QColor& color)
{
	data[x + y * img->width()] = color.rgb();
}
void ViewerWidget::setPixel(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
	QColor color(r, g, b);
	setPixel(x, y, color);
}

void ViewerWidget::clear(QColor color)
{
	for (size_t x = 0; x < img->width(); x++)
	{
		for (size_t y = 0; y < img->height(); y++)
		{
			setPixel(x, y, color);
		}
	}
	update();
}


void ViewerWidget::KresliUseckuDDA(QPointF P1, QPointF P2, QColor* Color)
{
	P1.setX(round(P1.x()));
	P1.setY(round(P1.y()));
	P2.setX(round(P2.x()));
	P2.setY(round(P2.y()));

	int deltaX = P2.x() - P1.x();
	int deltaY = P2.y() - P1.y();
	float m = (float)deltaY / deltaX;

	if ((m >= 1.0) || (m <= -1.0)) {
		//riadiaca os Y
		if (P1.y() > P2.y()) {
			PointSwap(P1, P2);		//y1 ma byt mensie ako y2
		}
		float surX = P1.x();
		int surY = P1.y();
		do {
			setPixel(round(surX), surY, *Color);
			surY++;
			surX += 1 / m;
		} while (surY < P2.y());
	}
	else {
		//riadiaca os X
		if (P1.x() > P2.x()) {
			PointSwap(P1, P2);		//x1 ma byt mensie ako x2
		}
		int surX = P1.x();
		float surY = P1.y();
		do {
			setPixel(surX, round(surY), *Color);
			surX++;
			surY += m;
		} while (surX < P2.x());
	}
	update();
}
void ViewerWidget::KresliUseckuBresen(QPointF P1, QPointF P2, QColor* Color)
{
	P1.setX(round(P1.x()));
	P1.setY(round(P1.y()));
	P2.setX(round(P2.x()));
	P2.setY(round(P2.y()));

	int deltaX = P2.x() - P1.x();
	int deltaY = P2.y() - P1.y();
	float m = (float)deltaY / deltaX;
	int k1, k2, p, surX, surY;

	//riadiaca os Y

	if (m > 1.0) {

		if (P1.y() > P2.y()) {
			PointSwap(P1, P2);		//y1 ma byt mensie ako y2
			deltaX = P2.x() - P1.x();
			deltaY = P2.y() - P1.y();
		}

		k1 = 2 * deltaX;
		k2 = 2 * deltaX - 2 * deltaY;
		p = 2 * deltaX - deltaY;
		surX = P1.x();
		surY = P1.y();

		do {
			setPixel(surX, surY, *Color);
			surY++;
			if (p > 0) {
				surX++;
				p += k2;
			}
			else {
				p += k1;
			}
		} while (surY < P2.y());
	}

	else if (m < -1.0) {

		if (P1.y() > P2.y()) {
			PointSwap(P1, P2);		//y1 ma byt mensie ako y2
			deltaX = P2.x() - P1.x();
			deltaY = P2.y() - P1.y();
		}

		k1 = 2 * deltaX;
		k2 = 2 * deltaX + 2 * deltaY;
		p = 2 * deltaX + deltaY;
		surX = P1.x();
		surY = P1.y();

		do {
			setPixel(surX, surY, *Color);
			surY++;
			if (p < 0) {
				surX--;
				p += k2;
			}
			else {
				p += k1;
			}
		} while (surY < P2.y());
	}

	//riadiaca os X

	else if ((m >= 0.0) && (m <= 1.0)) {

		if (P1.x() > P2.x()) {
			PointSwap(P1, P2);		//x1 ma byt mensie ako x2
			deltaX = P2.x() - P1.x();
			deltaY = P2.y() - P1.y();
		}

		k1 = 2 * deltaY;
		k2 = 2 * deltaY - 2 * deltaX;
		p = 2 * deltaY - deltaX;
		surX = P1.x();
		surY = P1.y();

		do {
			setPixel(surX, surY, *Color);
			surX++;
			if (p > 0) {
				surY++;
				p += k2;
			}
			else {
				p += k1;
			}
		} while (surX < P2.x());
	}
	else if ((m >= -1.0) && (m < 0.0)) {

		if (P1.x() > P2.x()) {
			PointSwap(P1, P2);		//x1 ma byt mensie ako x2
			deltaX = P2.x() - P1.x();
			deltaY = P2.y() - P1.y();

		}

		k1 = 2 * deltaY;
		k2 = 2 * deltaY + 2 * deltaX;
		p = 2 * deltaY + deltaX;
		surX = P1.x();
		surY = P1.y();

		do {
			setPixel(surX, surY, *Color);
			surX++;
			if (p < 0) {
				surY--;
				p += k2;
			}
			else {
				p += k1;
			}
		} while (surX < P2.x());
	}
	update();
}

void ViewerWidget::KresliPolygonDDA(QVector<QPointF>* Sur, QColor* Color)
{
	int i;
	if (Sur->size() != 2) {
		for (i = 0; i < Sur->size() - 1; i++) {
			KresliUseckuDDA(Sur->at(i), Sur->at(i + 1), Color);
		}
	}
	if (Sur->size() >= 2) {
		KresliUseckuDDA(Sur->at(0), Sur->at(Sur->size() - 1), Color);
	}
}
void ViewerWidget::KresliPolygonBresen(QVector<QPointF>* Sur, QColor* Color)
{
	int i;
	if (Sur->size() != 2) {
		for (i = 0; i < Sur->size() - 1; i++) {
			KresliUseckuBresen(Sur->at(i), Sur->at(i + 1), Color);
		}
	}
	if (Sur->size() >= 2) {
		KresliUseckuBresen(Sur->at(0), Sur->at(Sur->size() - 1), Color);
	}
}

void ViewerWidget::KresliPolygonBresenBarycentric(QVector<zBufferUnit>* Sur, Vizualizacia* Kamera)
{
	if (Sur->size() >= 3) {

		QVector<QPointF> vrcholyTrojuholnika(3);
		QVector<QColor> farbyTrojuholnika(3);
		QVector<double> zTrojuholnika(3);

		for (int i = 1; i <= Sur->size() - 2 ; i++) {
			//rozdelim si jednotlive premenne a samostatne ich poslem do funkcie
			
			vrcholyTrojuholnika[0].setX((*Sur)[0].Sur.x());
			vrcholyTrojuholnika[0].setY((*Sur)[0].Sur.y());
			farbyTrojuholnika[0] = (*Sur)[0].color;
			zTrojuholnika[0] = (*Sur)[0].Z;

			vrcholyTrojuholnika[1].setX((*Sur)[i].Sur.x());
			vrcholyTrojuholnika[1].setY((*Sur)[i].Sur.y());
			farbyTrojuholnika[1] = (*Sur)[i].color;
			zTrojuholnika[1] = (*Sur)[i].Z;

			vrcholyTrojuholnika[2].setX((*Sur)[i + 1].Sur.x());
			vrcholyTrojuholnika[2].setY((*Sur)[i + 1].Sur.y());
			farbyTrojuholnika[2] = (*Sur)[i + 1].color;
			zTrojuholnika[2] = (*Sur)[i + 1].Z;


			FillScanLineTriangle(&vrcholyTrojuholnika, &farbyTrojuholnika, &zTrojuholnika, Kamera);
		}
	}
}

void ViewerWidget::KresliPolygony(QList<QVector<zBufferUnit>>* Sur, QColor* Color, Vizualizacia* Kamera)
{
	clear();
	for (int i = 0; i < Sur->size(); i++) {
		
		if (Kamera->getRenderMode() == 0) {			//SHADED
			OrezaniePolygon(&(*Sur)[i], Kamera);
			KresliPolygonBresenBarycentric(&(*Sur)[i], Kamera);
		}
		else if (Kamera->getRenderMode() == 1) {		//WIREFRAME
			QVector<QPointF> SurTrojuholnika = { QPointF((*Sur)[i][0].Sur.x(), (*Sur)[i][0].Sur.y()) , QPointF((*Sur)[i][1].Sur.x(), (*Sur)[i][1].Sur.y()) , QPointF((*Sur)[i][2].Sur.x(), (*Sur)[i][2].Sur.y()) };
			OrezaniePolygon(&SurTrojuholnika);
			KresliPolygonBresen(&SurTrojuholnika, Color);
		}
	}
	update();
}

void ViewerWidget::KresliBod(QPointF* P0, int* LineAlg)
{
	int i, j;
	QColor farba = QColor("purple");
	int r = 3;		//polomer, pre lepsiu orientaciu
	QPointF Bod0 = QPointF(P0->x() - r, P0->y());
	QPointF Bod1 = QPointF(P0->x() + r + 1, P0->y());
	QPointF Bod2 = QPointF(P0->x(), P0->y() - r);
	QPointF Bod3 = QPointF(P0->x(), P0->y() + r + 1);

	QVector<QPointF> V0 = { Bod0, Bod1 };
	QVector<QPointF> V1 = { Bod2, Bod3 };
	OrezanieUsecka(&V0);
	OrezanieUsecka(&V1);

	if (V0.size() == 2 && V1.size() == 2) {
		if (LineAlg == 0) {
			KresliUseckuBresen(V0[0], V0[1], &farba);
			KresliUseckuBresen(V1[0], V1[1], &farba);
		}
		else {
			KresliUseckuDDA(V0[0], V0[1], &farba);
			KresliUseckuDDA(V1[0], V1[1], &farba);
		}
	}
}
void ViewerWidget::KresliBod(QPoint* P0, int* LineAlg)
{
	int i, j;
	QColor farba = QColor("purple");
	float r = 3.0;		//polomer, pre lepsiu orientaciu
	QPointF Bod0 = QPointF((float)P0->x() - r, (float)P0->y());
	QPointF Bod1 = QPointF((float)P0->x() + r + 1, (float)P0->y());
	QPointF Bod2 = QPointF((float)P0->x(), (float)P0->y() - r);
	QPointF Bod3 = QPointF((float)P0->x(), (float)P0->y() + r + 1);

	QVector<QPointF> V0 = { Bod0, Bod1 };
	QVector<QPointF> V1 = { Bod2, Bod3 };
	OrezanieUsecka(&V0);
	OrezanieUsecka(&V1);

	if (V0.size() == 2 && V1.size() == 2) {
		if (LineAlg == 0) {
			KresliUseckuBresen(V0[0], V0[1], &farba);
			KresliUseckuBresen(V1[0], V1[1], &farba);
		}
		else {
			KresliUseckuDDA(V0[0], V0[1], &farba);
			KresliUseckuDDA(V1[0], V1[1], &farba);
		}
	}
}
void ViewerWidget::KresliBody(QVector<QPointF>* Sur, int* LineAlg)
{
	for (int i = 0; i < Sur->size(); i++) {
		KresliBod(&(*Sur)[i], LineAlg);
	}
}


void ViewerWidget::KresliKrivkuHermit(QVector<QPointF>* Sur, QVector<HermitVektor>* HVektory, QColor* Color, int* LineAlg)
{
	int i;
	QPointF Q0, Q1;
	QPointF D0, D1;
	float F0, F1, F2, F3;
	float deltaT = 0.1 / Sur->size();		//adaptivna presnost vykreslovania

	//na zaciatku sa vypocita, na com budem potom setrit cas
	float Pi_rot0 = HVektory->at(0).Rot * M_PI / 180.0;		
	D0.setX( + cosf(Pi_rot0) * HVektory->at(0).Dlzka);
	D0.setY( - sinf(Pi_rot0) * HVektory->at(0).Dlzka);

	for (i = 1; i < Sur->size(); i++) {

		float Pi_rot1 = HVektory->at(i).Rot * M_PI / 180.0;		//1 sa vypocita nanovo, 0 sa len dosadzuje

		D1.setX( + cosf(Pi_rot1) * HVektory->at(i).Dlzka);
		D1.setY( - sinf(Pi_rot1) * HVektory->at(i).Dlzka);

		float T = deltaT;
		Q0 = Sur->at(i-1);
		do {
			F0 = 2 * T * T * T - 3 * T * T + 1;
			F1 = -2 * T * T * T + 3 * T * T;
			F2 = T * T * T - 2 * T * T + T;
			F3 = T * T * T - T * T;
			Q1 = Sur->at(i - 1) * F0 + Sur->at(i) * F1 + D0 * F2 + D1 * F3;
			
			QVector<QPointF> Qusecka = { Q0,Q1 };		//musim si vytvorit qvector pre orezavaci algoritmus
			OrezanieUsecka(&Qusecka);
			if (Qusecka.size() == 2) {
				if (*LineAlg == 1) {
					KresliUseckuBresen(Qusecka[0], Qusecka[1], Color);
				}
				else {
					KresliUseckuDDA(Qusecka[0], Qusecka[1], Color);
				}
			}
			Q0 = Q1;
			T += deltaT;

		} while (T < 1);

		Pi_rot0 = Pi_rot1;		//potom sa dosadzuje, aby sa setril cas
		D0 = D1;				//dosadi sa stary dotykovy vektor do noveho

		//vykreslenie posledneho segmentu
		QVector<QPointF> Qusecka = { Q0, Sur->at(i) };
		OrezanieUsecka(&Qusecka);
		if (Qusecka.size() == 2) {
			if (*LineAlg == 1) {
				KresliUseckuBresen(Qusecka[0], Qusecka[1], Color);
			}
			else {
				KresliUseckuDDA(Qusecka[0], Qusecka[1], Color);
			}
		}
	}	
}
void ViewerWidget::KresliKrivkuBezier(QVector<QPointF>* Sur, QColor* Color, int* LineAlg)
{
	int i,j;
	QPointF Q0, Q1;
	float deltaT = 0.1 / Sur->size();		//adaptivna presnost vykreslovania
	float T = deltaT;

	QVector<QVector<QPointF>> P(Sur->size());
	for (i = 0; i < Sur->size(); i++) {		
		P[i].resize(Sur->size() - i);		//priprava velkosti pola
		P[0][i] = Sur->at(i);				//vyplnenie prveho riadku
	}

	Q0 = Sur->at(0);

	do {
		for (i = 1; i < Sur->size(); i++) {
			for (j = 0; j < Sur->size() - i; j++) {
				P[i][j] = (1.0 - T) * P[i - 1][j] + T * P[i - 1][j + 1];
			}
		}
		Q1 = P[Sur->size() - 1][0];

		QVector<QPointF> Qusecka = { Q0, Q1 };
		OrezanieUsecka(&Qusecka);
		if (Qusecka.size() == 2) {
			if (*LineAlg == 1) {
				KresliUseckuBresen(Qusecka[0], Qusecka[1], Color);
			}
			else {
				KresliUseckuDDA(Qusecka[0], Qusecka[1], Color);
			}
		}
		Q0 = Q1;
		T += deltaT;

	} while (T <= 1);

	
	QVector<QPointF> Qusecka = { Q0, Sur->at(Sur->size() - 1) };
	OrezanieUsecka(&Qusecka);
	if (Qusecka.size() == 2) {
		if (*LineAlg == 1) {
			KresliUseckuBresen(Qusecka[0], Qusecka[1], Color);
		}
		else {
			KresliUseckuDDA(Qusecka[0], Qusecka[1], Color);
		}
	}

}
void ViewerWidget::KresliKrivkuBspline(QVector<QPointF>* Sur, QColor* Color, int* LineAlg)
{
	int i;
	float deltaT = 0.1 / Sur->size();		//adaptivna presnost vykreslovania
	QPointF Q0, Q1;

	for (i = 3; i < Sur->size(); i++) {

		float T = 0.0;
		float B0 = (-1.0 / 6) * T * T * T + (1.0 / 2) * T * T - (1.0 / 2) * T + (1.0 / 6);
		float B1 = (1.0 / 2) * T * T * T - T * T + (2.0 / 3);
		float B2 = (-1.0 / 2) * T * T * T + (1.0 / 2) * T * T + (1.0 / 2) * T + (1.0 / 6);
		float B3 = (1.0 / 6) * T * T * T;
		Q0 = Sur->at(i - 3) * B0 + Sur->at(i - 2) * B1 + Sur->at(i - 1) * B2 + Sur->at(i) * B3;
		
		do {
			T += deltaT;
			float B0 = (-1.0 / 6) * T * T * T + (1.0 / 2) * T * T - (1.0 / 2) * T + (1.0 / 6);
			float B1 = (1.0 / 2) * T * T * T - T * T + (2.0 / 3);
			float B2 = (-1.0 / 2) * T * T * T + (1.0 / 2) * T * T + (1.0 / 2) * T + (1.0 / 6);
			float B3 = (1.0 / 6) * T * T * T;
			Q1 = Sur->at(i - 3) * B0 + Sur->at(i - 2) * B1 + Sur->at(i - 1) * B2 + Sur->at(i) * B3;
			
			//##### Kontrola a vykreslenie ###############
			QVector<QPointF> Qusecka = { Q0, Q1 };
			OrezanieUsecka(&Qusecka);
			if (Qusecka.size() == 2) {
				if (*LineAlg == 1) {
					KresliUseckuBresen(Qusecka[0], Qusecka[1], Color);
				}
				else {
					KresliUseckuDDA(Qusecka[0], Qusecka[1], Color);
				}
			}
			//############################################

			Q0 = Q1;
			
		} while (T <= 1);
	}
}

void ViewerWidget::KresliZBuffer()
{
	clear();
	//CheckZBuffer();
	int i, j;
	for (i = 0; i < zBuffer.size(); i++) {
		for (j = 0; j < zBuffer[i].size(); j++) {
			if (zBuffer[i][j].color.isValid()) {
				setPixel(i, j, zBuffer[i][j].color);
			}
		}
	}
	update();
}
void ViewerWidget::CheckZBuffer() {

	int i, j;
	int n;
	int RedBuffer, BlueBuffer, GreenBuffer;
	double Buffer_Z;

	for (i = 2; i < zBuffer.size() - 2; i++) {
		for (j = 2; j < zBuffer[i].size() - 2; j++) {
			if (zBuffer[i][j].Z < 0.0 && zBuffer[i][j].Z != -DBL_MAX) {		//ak narazi na pixel, kde fillTriangle nedosiahol

				n = 0;
				RedBuffer = 0;
				BlueBuffer = 0;
				GreenBuffer = 0;
				Buffer_Z = 0.0;
				
				if (zBuffer[i + 1][j].Z > 0.0) {
					RedBuffer += zBuffer[i + 1][j].color.red();
					BlueBuffer += zBuffer[i + 1][j].color.blue();
					GreenBuffer += zBuffer[i + 1][j].color.green();
					Buffer_Z += zBuffer[i + 1][j].Z;
					n++;
				}

				if (zBuffer[i - 1][j].Z > 0.0) {
					RedBuffer += zBuffer[i - 1][j].color.red();
					BlueBuffer += zBuffer[i - 1][j].color.blue();
					GreenBuffer += zBuffer[i - 1][j].color.green();
					Buffer_Z += zBuffer[i - 1][j].Z;
					n++;
				}

				if (zBuffer[i][j - 1].Z > 0.0) {
					RedBuffer += zBuffer[i][j - 1].color.red();
					BlueBuffer += zBuffer[i][j - 1].color.blue();
					GreenBuffer += zBuffer[i][j - 1].color.green();
					Buffer_Z += zBuffer[i][j - 1].Z;
					n++;
				}

				if (zBuffer[i][j + 1].Z > 0.0) {
					RedBuffer += zBuffer[i][j + 1].color.red();
					BlueBuffer += zBuffer[i][j + 1].color.blue();
					GreenBuffer += zBuffer[i][j + 1].color.green();
					Buffer_Z += zBuffer[i][j + 1].Z;
					n++;
				}

				if (n != 0) {
					zBuffer[i][j].color.setRed(RedBuffer / n);
					zBuffer[i][j].color.setBlue(BlueBuffer / n);
					zBuffer[i][j].color.setGreen(GreenBuffer / n);
					zBuffer[i][j].Z = Buffer_Z / n;
				}
			}
		}
	}
}

void ViewerWidget::KresliVektory(QVector<QPointF>* Sur, QVector<HermitVektor>* HVektory, int* LineAlg)
{
	QColor farba = QColor("red");

	for (int i = 0; i < HVektory->size(); i++) {
		QVector<QPointF>V(2);
		V[0] = Sur->at(i);

		float Pi_rot = HVektory->at(i).Rot * M_PI / 180.0;

		V[1].setX(V[0].x() + cosf(Pi_rot)*HVektory->at(i).Dlzka);
		V[1].setY(V[0].y() - sinf(Pi_rot) * HVektory->at(i).Dlzka);

		OrezanieUsecka(&V);
		

		if (V.size() == 2) {

			if (*LineAlg == 1) {
				KresliUseckuBresen(V[0], V[1], &farba);
			}
			else {
				KresliUseckuDDA(V[0], V[1], &farba);
			}
		}
	}
}

void ViewerWidget::Generuj(QVector<QPointF>* Sur, QColor* Color, int* Algoritmus, int Vypln, QColor* FillColor)
{
	QVector<QPointF> V = *Sur;  //dolezite: vytvorim kopiu povodnych suradnic a narabam len s nou
	Orezanie(&V);

	//niekedy bude velkost 0, preto to poriadne otestujem
	if (V.size() == 2) {			
		if (Algoritmus == 0) {
			KresliUseckuDDA(V[0], V[1], Color);
		}
		else {
			KresliUseckuBresen(V[0], V[1], Color);
		}
	}
	else if (V.size() > 2) {

		//VYKRESLENIE POLYGONU
		if (Algoritmus == 0) {
			KresliPolygonDDA(&V, Color);
		}
		else {
			KresliPolygonBresen(&V, Color);
		}

		//VYFARBOVANIE
		if (Vypln != 0) {
			//vyplnujem iba ak je zvolena vypln v UI
			if (Sur->size() > 3) {				//polygon
				FillScanLine(&V, FillColor);
			}
			else if (V.size() == 3) {		//trojuholnik
				FillScanLineTriangle(&V, &Vypln);
			}
		}
	}
}
void ViewerWidget::GenerujKrivku(QVector<QPointF>* Sur, QVector<HermitVektor>* HVektory, QColor* Color, int* CurveAlg, int* LineAlg)
{
	clear();
	KresliBody(Sur, LineAlg);

	if (*CurveAlg == 0) {
		KresliKrivkuHermit(Sur, HVektory, Color, LineAlg);
		KresliVektory(Sur, HVektory, LineAlg);
	}
	else if (*CurveAlg == 1) {
		KresliKrivkuBezier(Sur, Color, LineAlg);
	}
	else if (*CurveAlg == 2) {
		KresliKrivkuBspline(Sur, Color, LineAlg);
	}
}

void ViewerWidget::Orezanie(QVector<QPointF>* V)
{
	if (V->size() == 2) {
		//USECKA
		OrezanieUsecka(V);
	}
	else if (V->size() > 2) {
		//POLYGON
		OrezaniePolygon(V);
	}
}
void ViewerWidget::OrezaniePolygon(QVector<QPointF>* V) {

	//tento vektor V je kopia povodnych bodov, mozem ho upravovat

	QVector<QPointF>W;		//vektor bodov orezaneho polygonu
	W.reserve(6);
	QPointF S, Pi;
	int i, j;
	float Xmin[4] = { 0.0,0.0, -(float)getImgWidth() + 1, -(float)getImgHeight() + 1 };

	for (j = 0; j < 4; j++) {

		if (V->size() != 0) {
			S = V->at(V->size() - 1);		//do S ulozim posledny vrchol
		}

		for (i = 0; i < V->size(); i++) {		//idem po vstupnych bodoch

			if (V->at(i).x() >= Xmin[j]) {		//ak je prvy bod vnutri platna
				if (S.x() >= Xmin[j]) {			//ak je druhy bod tiez vnutri platna, vlozim prvy bod do orezanehopolygonu
					W.push_back(V->at(i));
				}
				else {						//ak druhy bod je mimo platna
					Pi = QPoint(Xmin[j], S.y() + ((Xmin[j] - S.x()) * (V->at(i).y() - S.y()) / (V->at(i).x() - S.x())));		
					W.push_back(Pi);		//vypocitam bod na hrane a vlozim ho do orezaneho polygonu
					W.push_back(V->at(i));		//aj prvy bod vlozim do orezaneho polygonu
				}
			}
			else {							//ak prvy bod je mimo platna
				if (S.x() >= Xmin[j]) {		//ak je druhy bod na platne
					Pi = QPoint(Xmin[j], S.y() + ((Xmin[j] - S.x()) * (V->at(i).y() - S.y()) / (V->at(i).x() - S.x())));
					W.push_back(Pi);		//vypocitam bod na hrane a vlozim do orez. polygonu
				}
				//ak by nebol ani jeden bol na platne, nestane sa nic
			}
			S = V->at(i);		//aktualizujem S
		}

		V->clear();		//resetujem povodny qvektor bodov a prekopirujem donho urezany polygon
		V->squeeze();
		for (i = 0; i < W.size(); i++) {
			V->push_back(QPointF(W[i].y(), -W[i].x()));
		}
		W.clear();
		W.squeeze();
	}
}
void ViewerWidget::OrezaniePolygon(QVector<zBufferUnit>* V, Vizualizacia* Kamera) {

	//tento vektor V je kopia povodnych bodov, mozem ho upravovat

	QVector<zBufferUnit>W;		//vektor bodov orezaneho polygonu
	zBufferUnit S, Pi;
	int i, j;
	W.reserve(6);
	
	float Xmin[4] = { 0.0,0.0, -(float)getImgWidth() + 1, -(float)getImgHeight() + 1 };

	for (j = 0; j < 4; j++) {

		if (V->size() != 0) {
			S = V->at(V->size() - 1);		//do S ulozim posledny vrchol
		}

		for (i = 0; i < V->size(); i++) {		//idem po vstupnych bodoch

			if (V->at(i).Sur.x() >= Xmin[j]) {		//ak je prvy bod vnutri platna
				if (S.Sur.x() >= Xmin[j]) {			//ak je druhy bod tiez vnutri platna, vlozim prvy bod do orezaneho polygonu
					W.push_back(V->at(i));
				}
				else {						//ak druhy bod je mimo platna
					
					Pi.Sur.setX(Xmin[j]);
					Pi.Sur.setY(S.Sur.y() + ((Xmin[j] - S.Sur.x()) * (V->at(i).Sur.y() - S.Sur.y()) / (V->at(i).Sur.x() - S.Sur.x())));

					//vypocet linearneho pomeru farby a Z hodnoty
					double pomer = abs(S.Sur.x() / (abs(V->at(i).Sur.x()) + abs(S.Sur.x())));
					QColor PiColor;
					PiColor.setRed(round((S.color.red() * pomer) + (V->at(i).color.red() * (1.0 - pomer))));
					PiColor.setGreen(round((S.color.green() * pomer) + (V->at(i).color.green() * (1.0 - pomer))));
					PiColor.setBlue(round((S.color.blue() * pomer) + (V->at(i).color.blue() * (1.0 - pomer))));
					Pi.color = PiColor;
					Pi.Z = S.Z * pomer + V->at(i).Z * (1.0 - pomer);

					W.push_back(Pi);		//vypocitam bod na hrane a vlozim ho do orezaneho polygonu
					W.push_back(V->at(i));		//aj prvy bod vlozim do orezaneho polygonu
				}
			}
			else {							//ak prvy bod je mimo platna
				if (S.Sur.x() >= Xmin[j]) {		//ak je druhy bod na platne
					Pi.Sur.setX(Xmin[j]);
					Pi.Sur.setY(S.Sur.y() + ((Xmin[j] - S.Sur.x()) * (V->at(i).Sur.y() - S.Sur.y()) / (V->at(i).Sur.x() - S.Sur.x())));

					//vypocet linearneho pomeru farby a Z hodnoty
					double pomer = abs(S.Sur.x() / (abs(V->at(i).Sur.x()) + abs(S.Sur.x())));
					QColor PiColor;
					PiColor.setRed(round((S.color.red() * pomer) + (V->at(i).color.red() * (1.0 - pomer))));
					PiColor.setGreen(round((S.color.green() * pomer) + (V->at(i).color.green() * (1.0 - pomer))));
					PiColor.setBlue(round((S.color.blue() * pomer) + (V->at(i).color.blue() * (1.0 - pomer))));
					Pi.color = PiColor;
					Pi.Z = S.Z * pomer + V->at(i).Z * (1.0 - pomer);

					W.push_back(Pi);		//vypocitam bod na hrane a vlozim do orez. polygonu
				}
				//ak by nebol ani jeden bol na platne, nestane sa nic
			}
			S = V->at(i);		//aktualizujem S
		}

		V->clear();		//resetujem povodny qvektor bodov a prekopirujem donho urezany polygon
		V->squeeze();
		for (i = 0; i < W.size(); i++) {
			double tempSur = W[i].Sur.x();
			W[i].Sur.setX(W[i].Sur.y());
			W[i].Sur.setY(-tempSur);

			V->push_back(W[i]);
		}
		W.clear();
		W.squeeze();
	}
}

void ViewerWidget::OrezanieUsecka(QVector<QPointF>* V)
{
	QPointF d = V->at(1) - V->at(0);		//smernica usecky

	QVector<QPointF> E(4);
	E[0] = QPointF(0.0, 0.0);
	E[1] = QPointF(0.0, getImgHeight());
	E[2] = QPointF(getImgWidth(), getImgHeight());
	E[3] = QPointF(getImgWidth(), 0.0);

	//##############################################

	if (KontrolaPriesecnikov(*V, E, d)) {
		//skontrolovalo, ci usecka presekla platno
		float tl = 0.0;
		float tu = 1.0;
		QPointF P0 = V->at(0);
		QPointF P1 = V->at(1);

		for (int i = 0; i < 4; i++) {
			QPointF smernicaE = E[(i + 1) % 4] - E[i];
				QPointF n(smernicaE.y(), -smernicaE.x());		//vnutorna normala
				QPointF w = V->at(0) - E[i];

				float dn = SkalarSuc(&d, &n);
				float wn = SkalarSuc(&w, &n);

				if (dn != 0.0) {
					float t = -wn / dn;
						if (dn > 0.0 && t <= 1.0) {
							tl = max(t, tl);
						}
						else if (dn < 0.0 && t >= 0.0) {
							tu = min(t, tu);
						}
				}

		}
		V->clear();
		V->squeeze();

		if (tl < tu) {
			V->push_back(P0 + tl * (P1 - P0));
			V->push_back(P0 + tu * (P1 - P0));
		}
	}
}

void ViewerWidget::FillScanLine(QVector<QPointF>* Sur, QColor* FillColor)
{
	int i,j,k;
	QList<int> naVymazanie;					//list indexov hran, ktore budem mazat, lebo budu mat deltaY==0
	QVector<Hrana> hrany(Sur->size());		//vektor hran, s ktorymi budeme pracovat
	float Ymax = -1.0;						//inicializacia na cyklus

	PripravHrany(Sur, hrany);	
	if (hrany.size() >= 2) {				//podmienka, pretoze v extremnych pripadoch moze nastat chyba
		float Ymin = hrany.at(0).P0.y();	//mam zoradeny vektor "hrany", tak si jednoducho zistim Ymin

		for (k = 0; k < hrany.size(); k++) {
			if (hrany[k].P1.y() > Ymax) Ymax = hrany[k].P1.y();		//zistit Ymax nie je trivialne, potrebujem na to cyklus
		}

		QVector<QList<Hrana>> TH;
		TH.resize(Ymax - Ymin + 2);		//vytvorim qlist o 2 vacsie, aby sa zmestili aj krajne hodnoty, hlavne ked je potrebne orezavanie


		for (i = 0; i < hrany.size(); i++) {			//vyplnim Tabulku Hran
			int poloha = hrany.at(i).P0.y() - Ymin;		//vypocita sa riadok, v ktorom sa ma hrana v TH nachadzat
			TH[poloha].push_back(hrany[i]);				//hrana sa vlozi na polohu v TH
		}

		QList<Hrana> ZAH;
		int Y = Ymin;

		for (i = 0; i < TH.size(); i++) {
			if (!TH[i].isEmpty()) {		//ak riadok v Tabulke Hran nie je prazdny, pridam ho do ZAH

				for (k = 0; k < TH[i].size(); k++) {
					ZAH.push_back(TH[i][k]);		//prekopirujem hrany do ZAH
				}		
			}

			TrieditHranyX(ZAH);		//potriedim hrany podla sur X vzostupne
			j = 0;

			while (j < ZAH.size()) {
				if (j % 2 == 0) {
					//Len pre parne poradie budem zistovat vykreslovanie
					int x_rozdiel = round(ZAH[j + 1].X - ZAH[j].X);

					if (x_rozdiel != 0) {
						//Vykreslovat BODY
						for (k = 1; k <= x_rozdiel; k++) {
							setPixel(floor(ZAH[j].X + k), Y, *FillColor);
						}
					}
				}

				if (ZAH[j].deltaY == 0) {
					//Odstranit hrany, ktorych deltaY je nula
					//doplnam zoznam, ktory sa potom naraz vymaze
					naVymazanie.append(j);
				}

				ZAH[j].deltaY -= 1;
				ZAH[j].X += ZAH[j].w;
				j++;
			}

			for (k = 0; k < naVymazanie.size(); k++) {
				ZAH.removeAt(naVymazanie[k] - k);		//minus k, pretoze qlist sa bude dynamicky zmensovat, tak aby som to kompenzoval
			}
			naVymazanie.clear();
			Y++;
		}
		update();
	}
}
void ViewerWidget::FillScanLineTriangle(QVector<QPointF>* Sur, int* Vyber)
{
	QColor FarbaPixelu;
	QVector<QPointF>Troj = *Sur;				//kopia suradnic trojuholnika, s ktorym budem len vykreslovat
	UsporiadajTroj(Troj);
	QVector<QPointF> e1(2);
	QVector<QPointF> e2(2);
	QVector<QPointF> e3(2);
	QVector<QPointF> e4(2);
	QPointF P;
	float w1, w2, w3, w4;
	float Y,Ymax, X1, X2;
	float i;

	QVector<QColor> Farby(3);
	Farby[0] = QColor(255, 0, 0);		//zatial si to definujem tu, mozno to prelozim neskor
	Farby[1] = QColor(0, 255, 0);
	Farby[2] = QColor(0, 0, 255);

	if ((Troj[0].y() == Troj[1].y() && Troj[1].y() == Troj[2].y()) || (Troj[0].x() == Troj[1].x() && Troj[1].x() == Troj[2].x())) {
		//ak by boli na jednej priamke
		return;
	}
	else {
		if (Troj[0].y() == Troj[1].y()) {
			//vrchna hrana je vodorovna
			e1[0] = Troj[0];
			e1[1] = Troj[2];

			e2[0] = Troj[1];
			e2[1] = Troj[2];

			w1 = 1.0 / ((e1[1].y() - e1[0].y()) / (e1[1].x() - e1[0].x()));
			w2 = 1.0 / ((e2[1].y() - e2[0].y()) / (e2[1].x() - e2[0].x()));
		}
		else if(Troj[1].y() == Troj[2].y()) {
			//spodna hrana je vodorovna
			e1[0] = Troj[0];
			e1[1] = Troj[1];

			e2[0] = Troj[0];
			e2[1] = Troj[2];
		}
		else {
			//vypocitam deliaci bod
			
			float m = (Troj[2].y() - Troj[0].y()) / (Troj[2].x() - Troj[0].x());		//smernica T_0-T_2
			P.setX(((Troj[1].y() - Troj[0].y()) / m) + Troj[0].x());
			P.setY(Troj[1].y());

			if (P.x() < Troj[1].x()) {
				//dlha hrana nalavo
				e1[0] = Troj[0];
				e1[1] = P;

				e2[0] = Troj[0];
				e2[1] = Troj[1];

				e3[0] = P;
				e3[1] = Troj[2];

				e4[0] = Troj[1];
				e4[1] = Troj[2];
			}
			else {
				//dlha hrana napravo
				e1[0] = Troj[0];
				e1[1] = Troj[1];

				e2[0] = Troj[0];
				e2[1] = P;

				e3[0] = Troj[1];
				e3[1] = Troj[2];

				e4[0] = P;
				e4[1] = Troj[2];
			}
		}
	}
	w1 = 1.0 / ((e1[1].y() - e1[0].y()) / (e1[1].x() - e1[0].x()));
	w2 = 1.0 / ((e2[1].y() - e2[0].y()) / (e2[1].x() - e2[0].x()));

	Y = e1[0].y();
	Ymax = e1[1].y();
	X1 = e1[0].x();
	X2 = ceil(e2[0].x());


	while (Y <= Ymax) {
		for (i = 0.0; i <= ceil(X2 - X1); i+= 1.0) {
			//vyplnim riadky medzi X
			VyratajFarbuPix(Sur,&QPointF(X1 + i, Y), &FarbaPixelu, Vyber, &Farby);
			setPixel(round(X1 + i), Y, FarbaPixelu);
		}
		X1 += w1;
		X2 += w2;
		Y++;
	}

	if (!P.isNull()) {		//ak by existoval aj bod P
		w3 = 1.0 / ((e3[1].y() - e3[0].y() + 1.0) / (e3[1].x() - e3[0].x()));
		w4 = 1.0 / ((e4[1].y() - e4[0].y()) / (e4[1].x() - e4[0].x()));
		Y = e3[0].y();
		Ymax = e3[1].y();
		X1 = e3[0].x();
		X2 = ceil(e4[0].x());

		while (Y <= Ymax) {
			for (i = 0.0; i <= ceil(X2 - X1); i+= 1.0) {
				//vyplnim riadky medzi X
				VyratajFarbuPix(Sur, &QPointF(X1 + i, Y), &FarbaPixelu, Vyber, &Farby);
				setPixel(round(X1 + i), Y, FarbaPixelu);
			}
			X1 += w3;
			X2 += w4;
			Y++;
		}

	}
}
void ViewerWidget::FillScanLineTriangle(QVector<QPointF>* Sur, QVector<QColor> *Farby, QVector<double>* zValues, Vizualizacia* Kamera)
{
	int Vyber;
	double zValue;
	QColor FarbaPixelu;
	QVector<QPointF>Troj = *Sur;				//kopia suradnic trojuholnika, s ktorym budem len vykreslovat
	UsporiadajTroj(Troj);
	QVector<QPointF> e1(2);
	QVector<QPointF> e2(2);
	QVector<QPointF> e3(2);
	QVector<QPointF> e4(2);
	QPointF P;
	float w1, w2, w3, w4;
	float Y, Ymax, X1, X2, X1floor, X2ceil;
	double i;

	if(Troj[2]!=Troj[1]) {
		if (Kamera->getShadingMode() == 0) {
			MatchColors_Z(&Troj, Sur, Farby, zValues);
			Vyber = 2;		//pre barycentricku interpolaciu
		}
		else if (Kamera->getShadingMode() == 1) {
			Vyber = 1;		//pre nearest neightbour
		}

		(*Farby)[0].setAlphaF(0.5 + ((*zValues)[0] / (Kamera->getScale() * 10.0)));
		(*Farby)[1].setAlphaF(0.5 + ((*zValues)[1] / (Kamera->getScale() * 10.0)));
		(*Farby)[2].setAlphaF(0.5 + ((*zValues)[2] / (Kamera->getScale() * 10.0)));


		if ((Troj[0].y() == Troj[1].y() && Troj[1].y() == Troj[2].y()) || (Troj[0].x() == Troj[1].x() && Troj[1].x() == Troj[2].x())) {
			//ak by boli na jednej priamke
			return;
		}
		else {
			if (Troj[0].y() == Troj[1].y()) {
				//vrchna hrana je vodorovna
				e1[0] = Troj[0];
				e1[1] = Troj[2];

				e2[0] = Troj[1];
				e2[1] = Troj[2];

				w1 = 1.0 / ((e1[1].y() - e1[0].y()) / (e1[1].x() - e1[0].x()));
				w2 = 1.0 / ((e2[1].y() - e2[0].y()) / (e2[1].x() - e2[0].x()));
			}
			else if (Troj[1].y() == Troj[2].y()) {
				//spodna hrana je vodorovna
				e1[0] = Troj[0];
				e1[1] = Troj[1];

				e2[0] = Troj[0];
				e2[1] = Troj[2];
			}
			else {
				//vypocitam deliaci bod

				float m = (Troj[2].y() - Troj[0].y()) / (Troj[2].x() - Troj[0].x());		//smernica T_0-T_2
				P.setX(((Troj[1].y() - Troj[0].y()) / m) + Troj[0].x());
				P.setY(Troj[1].y());

				if (P.x() < Troj[1].x()) {
					//dlha hrana nalavo
					e1[0] = Troj[0];
					e1[1] = P;

					e2[0] = Troj[0];
					e2[1] = Troj[1];

					e3[0] = P;
					e3[1] = Troj[2];

					e4[0] = Troj[1];
					e4[1] = Troj[2];
				}
				else {
					//dlha hrana napravo
					e1[0] = Troj[0];
					e1[1] = Troj[1];

					e2[0] = Troj[0];
					e2[1] = P;

					e3[0] = Troj[1];
					e3[1] = Troj[2];

					e4[0] = P;
					e4[1] = Troj[2];
				}
			}
		}
		w1 = 1.0 / ((e1[1].y() - e1[0].y()) / (e1[1].x() - e1[0].x()));
		w2 = 1.0 / ((e2[1].y() - e2[0].y()) / (e2[1].x() - e2[0].x()));

		Y = e1[0].y();
		Ymax = e1[1].y();
		X1 = e1[0].x();
		X2 = e2[0].x();


		while (Y <= Ymax) {
			X1floor = round(X1);		//novinka - pracujem so zaokruhlenymi okrajovymi suradnicami
			X2ceil = round(X2);

			for (i = 0.0; i <= (X2ceil - X1floor); i++) {
				//vyplnim riadky medzi X
				VyratajFarbuPix(Sur, &QPointF(X1 + i, Y), &FarbaPixelu, &Vyber,Farby);
				
				zValue = FarbaPixelu.alphaF();
				FarbaPixelu.setAlpha(255);

				if (zValue >= zBuffer[(X1floor + i)][Y].Z) {
					zBuffer[(X1floor + i)][Y].color = FarbaPixelu;
					zBuffer[(X1floor + i)][Y].Z = zValue;
				}
			}
			X1 += w1;
			X2 += w2;
			Y++;
		}

		if (!P.isNull()) {		//ak by existoval aj bod P

			w3 = 1.0 / ((e3[1].y() - e3[0].y()) / (e3[1].x() - e3[0].x()));
			w4 = 1.0 / ((e4[1].y() - e4[0].y()) / (e4[1].x() - e4[0].x()));
			Y = e3[0].y();
			Ymax = e3[1].y();
			X1 = (e3[0].x());
			X2 = (e4[0].x());

			while (Y <= Ymax) {
				X1floor = round(X1);		//novinka - pracujem so zaokruhlenymi okrajovymi suradnicami
				X2ceil = round(X2);

				for (i = 0.0; i <= (X2ceil - X1floor); i+= 1.0) {
					//vyplnim riadky medzi X
					VyratajFarbuPix(Sur, &QPointF(X1 + i, Y), &FarbaPixelu, &Vyber, Farby);
					zValue = FarbaPixelu.alphaF();
					FarbaPixelu.setAlpha(255);

					if (zValue >= zBuffer[(X1floor + i)][Y].Z) {
						zBuffer[(X1floor + i)][Y].color = FarbaPixelu;
						zBuffer[(X1floor + i)][Y].Z = zValue;
					}
				}
				X1 += w3;
				X2 += w4;
				Y++;
			}

		}

	}
}

void ViewerWidget::VyratajFarbuPix(QVector<QPointF>* Sur, QPointF* P, QColor* Color, int* Vyber, QVector<QColor>* Farby) {

	if (*Vyber == 1) {
		NearestNeighbor(Sur,P,*Color,Farby);
	}
	else if (*Vyber == 2) {
		Barycentric(Sur,P,*Color,Farby);
	}
}
void ViewerWidget::NearestNeighbor(QVector<QPointF>* Sur,QPointF* P , QColor& OutputColor, QVector<QColor>* Farby)
{
	float d0 = sqrt((P->x() - Sur->at(0).x()) * (P->x() - Sur->at(0).x()) + (P->y() - Sur->at(0).y()) * (P->y() - Sur->at(0).y()));
	float d1 = sqrt((P->x() - Sur->at(1).x()) * (P->x() - Sur->at(1).x()) + (P->y() - Sur->at(1).y()) * (P->y() - Sur->at(1).y()));
	float d2 = sqrt((P->x() - Sur->at(2).x()) * (P->x() - Sur->at(2).x()) + (P->y() - Sur->at(2).y()) * (P->y() - Sur->at(2).y()));

	if (d0 <= d1 && d0 <= d2) {
		OutputColor = (*Farby)[0];
	}
	else if (d1 < d0 && d1 <= d2) {
		OutputColor = (*Farby)[1];
	}
	else if(d2 < d0 && d2 < d1) {
		OutputColor = (*Farby)[2];
	}
	else {
		OutputColor = QColor(255, 0, 0);
		//debugging
	}
}
bool ViewerWidget::CheckTriangleIntegrity(QVector<QPointF>* Sur) {
	
	if((*Sur)[0].x() == (*Sur)[1].x() && (*Sur)[1].x() == (*Sur)[2].x()){
		return false;
	}
	if ((*Sur)[0].y() == (*Sur)[1].y() && (*Sur)[1].y() == (*Sur)[2].y()) {
		return false;
	}

	return true;
}
void ViewerWidget::Barycentric(QVector<QPointF>* Sur, QPointF* P, QColor& OutputColor, QVector<QColor>* Farby)
{
	
	double lambda_0, lambda_1, lambda_2;

	QVector<QPointF>T = *Sur;				//kopia suradnic trojuholnika, s ktorym budem iba pocitat obsahy
	
	//podla Y - bubblesort
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3 - i - 1; j++) {
			//if ((*Sur)[j].y() > (*Sur)[j + 1].y()) {
			if (T[j].y() > T[j + 1].y()) {

				/*
				QPointF* TempBod = &(*Sur)[j];
				(*Sur)[j] = (*Sur)[j + 1];
				(*Sur)[j + 1] = *TempBod;
				*/
				
				QPointF TempBod = T[j];
				T[j] = T[j + 1];
				T[j + 1] = TempBod;
	
			}
		}
	}

	//podla X
	
	/*
	if (((*Sur)[0].y() == (*Sur)[1].y()) && ((*Sur)[0].x() != (*Sur)[1].x())) {
		if (((*Sur)[0].x() > (*Sur)[1].x())) {
			PointSwap((*Sur)[0], (*Sur)[1]);
		}
	}

	if (((*Sur)[1].y() == (*Sur)[2].y()) && ((*Sur)[1].x() != (*Sur)[2].x())) {
		if (((*Sur)[1].x() > (*Sur)[2].x())) {
			PointSwap((*Sur)[1], (*Sur)[2]);
		}
	}
	*/
	
	
	if ((T[0].y() == T[1].y()) && (T[0].x() != T[1].x())) {
		if ((T[0].x() > T[1].x())) {
			PointSwap(T[0], T[1]);
		}
	}
		
	if ((T[1].y() == T[2].y()) && (T[1].x() != T[2].x())) {
		if ((T[1].x() > T[2].x())) {

			PointSwap(T[1], T[2]);
		}
	}
	
		
	
	float A = abs(((T[1].x() - T[0].x()) * (T[2].y() - T[0].y())) - ((T[1].y() - T[0].y()) * (T[2].x() - T[0].x())));
	float A0 = abs(((T[1].x() - P->x()) * (T[2].y() - P->y())) - ((T[1].y() - P->y()) * (T[2].x() - P->x())));
	float A1 = abs(((T[0].x() - P->x()) * (T[2].y() - P->y())) - ((T[0].y() - P->y()) * (T[2].x() - P->x())));
	

	/*
	float A = abs(((float)((*Sur)[1].x() - (*Sur)[0].x()) * ((*Sur)[2].y() - (*Sur)[0].y())) - (((*Sur)[1].y() - (*Sur)[0].y()) * ((*Sur)[2].x() - (*Sur)[0].x())));
	float A0 = abs(((float)((*Sur)[1].x() - P->x()) * ((*Sur)[2].y() - P->y())) - (((*Sur)[1].y() - P->y()) * ((*Sur)[2].x() - P->x())));
	float A1 = abs(((float)((*Sur)[0].x() - P->x()) * ((*Sur)[2].y() - P->y())) - (((*Sur)[0].y() - P->y()) * ((*Sur)[2].x() - P->x())));
	*/

	if (A0 < 0.001) A0 = 0.0;
	if (A1 < 0.001) A1 = 0.0;

	if (A == 0.0) {
		lambda_0 = 0.0;
		lambda_1 = 0.0;
		lambda_2 = 1.0;
	}
	else {
		lambda_0 = A0 / (float)A;
		lambda_1 = A1 / (float)A;
		lambda_2 = 1.0 - lambda_0 - lambda_1;
	}
		
	OutputColor.setRed(Max(Min((lambda_0 * (*Farby)[0].red() + lambda_1 * (*Farby)[1].red() + lambda_2 * (*Farby)[2].red()), 255), 0));
	OutputColor.setGreen(Max(Min((lambda_0 * (*Farby)[0].green() + lambda_1 * (*Farby)[1].green() + lambda_2 * (*Farby)[2].green()), 255), 0));
	OutputColor.setBlue(Max(Min((lambda_0 * (*Farby)[0].blue() + lambda_1 * (*Farby)[1].blue() + lambda_2 * (*Farby)[2].blue()), 255), 0));
	try {
		OutputColor.setAlphaF(lambda_0 * (*Farby)[0].alphaF() + lambda_1 * (*Farby)[1].alphaF() + lambda_2 * (*Farby)[2].alphaF());
	}
	catch (int c) {}
}



void ViewerWidget::PointSwap(QPointF& P1, QPointF& P2)
{
	QPointF P3 = P1;
	P1 = P2;
	P2 = P3;
}
float ViewerWidget::SkalarSuc(QPointF* P1, QPointF* P2)
{
	return (P1->dotProduct(*P1, *P2));
}
float ViewerWidget::SkalarSuc(QPoint* P1, QPoint* P2)
{
	return (P1->dotProduct(*P1, *P2));
}
bool ViewerWidget::KontrolaPriesecnikov(QVector<QPointF>& V, QVector<QPointF>& E, QPointF& d)
{
	int i;
	if (!((V[0].x() >= 1.0 && V[0].x() <= getImgWidth() &&		//kontrola, ci su dva body mimo platna
		V[0].y() >= 1.0 && V[0].y() <= getImgHeight()) ||
		(V[1].x() >= 1.0 && V[1].x() <= getImgWidth() &&
			V[1].y() >= 1.0 && V[1].y() <= getImgHeight())))
	{
		//ak su oba body mimo platna, otestuje, kolko priesecnikov existuje medzi nasou useckou a hranami platna
		//ak to nie su dva priesecniky, nevypocitaju sa ziadne body a funkcia sa ukonci
		//algoritmus som si vymyslel sam, rovnice pre priesecnik dvoch useciek som nasiel na internete
		QVector<QPointF> smerniceE(4);
		int pretnutia = 0;
		for (i = 0; i < 4; i++) {
			smerniceE[i] = E[(i + 1) % 4] - E[i];
			float s, t;
			s = (-d.y() * (V[0].x() - E[i].x()) + d.x() * (V[0].y() - E[i].y())) / (-smerniceE[i].x() * d.y() + d.x() * smerniceE[i].y());
			t = (smerniceE[i].x() * (V[0].y() - E[i].y()) - smerniceE[i].y() * (V[0].x() - E[i].x())) / (-smerniceE[i].x() * d.y() + d.x() * smerniceE[i].y());
			if (s > 0 && s < 1 && t > 0 && t < 1) {
				pretnutia++;
			}
		}
		if (pretnutia != 2) {
			V.clear();
			V.squeeze();
			return false;
		}
	}

	//vrati true bud ak su oba body na platne alebo ak existuju dva priesecniky platna useckou
	return true;	
}

void ViewerWidget::PripravHrany(QVector<QPointF>* Vrcholy, QVector<Hrana>& hrany)
{
	float citatel,menovatel,m;
	QVector<int>vymazat;

	for (int i = 0; i < Vrcholy->size(); i++) {
		if (Vrcholy->at((i + 1) % Vrcholy->size()).y() < Vrcholy->at(i).y()) {	
				//ak je nasledujuci bod vyssie ako aktualny
			hrany[i].P0.setX(Vrcholy->at((i + 1) % Vrcholy->size()).x());
			hrany[i].P0.setY(Vrcholy->at((i + 1) % Vrcholy->size()).y());
			hrany[i].P1.setX(Vrcholy->at(i).x());
			hrany[i].P1.setY(Vrcholy->at(i).y());
		}
		else {
				//aktualny bod je vyssie
			hrany[i].P0.setX(Vrcholy->at(i).x());
			hrany[i].P0.setY(Vrcholy->at(i).y());
			hrany[i].P1.setX(Vrcholy->at((i + 1) % Vrcholy->size()).x());
			hrany[i].P1.setY(Vrcholy->at((i + 1) % Vrcholy->size()).y());
		}

		citatel = hrany[i].P1.y() - hrany[i].P0.y();
		menovatel = hrany[i].P1.x() - hrany[i].P0.x();

		if (citatel != 0.0 && menovatel != 0.0) {
			m = citatel / menovatel;
			hrany[i].w = 1.0 / m;	//vypocitam smernicu
		}
		
		else if (citatel == 0.0) {
			vymazat.push_back(i);
		}
		else if (menovatel == 0.0) {
			hrany[i].w = 0.0;
		}
		
		hrany[i].P1.setY(hrany[i].P1.y() - 1);		//skratim y suradnicu o 1
		hrany[i].deltaY = hrany[i].P1.y() - hrany[i].P0.y();		//pripravim si rozdiel ypsilonov
		hrany[i].X = hrany[i].P0.x();		//pripravim si X
	}

	for (int i = 0; i < vymazat.size(); i++) {
		hrany.removeAt(vymazat[i]-i);
	}

	TrieditHranyY(hrany);	//triedit vektora hrany podla bodu y
}
void ViewerWidget::TrieditHranyY(QVector<Hrana>& hrany) {	
	for (int i = 0; i < hrany.size(); i++) {
		for (int j = 0; j < hrany.size() - i - 1; j++) {
			if (hrany[j] > hrany[j + 1]) {
				Hrana TempHrana = hrany[j];
				hrany[j] = hrany[j + 1];
				hrany[j + 1] = TempHrana;
			}
		}
	}
}
void ViewerWidget::TrieditHranyX(QList<Hrana>& hrany) {
	for (int i = 0; i < hrany.size(); i++) {
		for (int j = 0; j < hrany.size() - i - 1; j++) {
			if (hrany[j].X > hrany[j + 1].X) {
				Hrana TempHrana = hrany[j];
				hrany[j] = hrany[j + 1];
				hrany[j + 1] = TempHrana;
			}
		}
	}
}

void ViewerWidget::UsporiadajTroj(QVector<QPointF>& Sur)
{
	//podla Y - bubblesort
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3 - i - 1; j++) {
			if (Sur[j].y() > Sur[j + 1].y()) {
				QPointF TempBod = Sur[j];
				Sur[j] = Sur[j + 1];
				Sur[j + 1] = TempBod;
			}
		}
	}

	//podla X
	if (Sur[0].y() == Sur[1].y()) {
		if ((Sur[0].x() > Sur[1].x())) {
			PointSwap(Sur[0], Sur[1]);
		}
	}
	else if (Sur[1].y() == Sur[2].y()) {
		if ((Sur[1].x() > Sur[2].x())) {
			PointSwap(Sur[1], Sur[2]);
		}
	}
}

void ViewerWidget::resetZBuffer()
{
	int i;
	for (i = 0; i < zBuffer.size(); i++) {
		zBuffer[i].clear();
		zBuffer[i].resize(getImgHeight());
	}
}

void ViewerWidget::MatchColors_Z(QVector<QPointF>* nove, QVector<QPointF>* povodne, QVector<QColor>* Farby, QVector<double>* zValues)
{
	int i, j;
	QVector<QColor> tempFarby(3);
	tempFarby[0] = (*Farby)[0];
	tempFarby[1] = (*Farby)[1];
	tempFarby[2] = (*Farby)[2];

	QVector<double> tempZ(3);
	tempZ[0] = (*zValues)[0];
	tempZ[1] = (*zValues)[1];
	tempZ[2] = (*zValues)[2];


	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			if ((*nove)[i] == (*povodne)[j]) {
				(*Farby)[i] = tempFarby[j];
				(*zValues)[i] = tempZ[j];
			}
		}
	}
	
}

//Slots
void ViewerWidget::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	QRect area = event->rect();
	painter.drawImage(area, *img, area);
}
