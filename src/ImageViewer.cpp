#include "ImageViewer.h"
#include "qmath.h"

ImageViewer::ImageViewer(QWidget* parent)
	: QMainWindow(parent), ui(new Ui::ImageViewerClass)
{
	ui->setupUi(this);

	//zapne okno pri starte
	openNewTabForImg(new ViewerWidget("default canvas", QSize(900, 900)));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);

	ui->ButtonShear->setEnabled(true);
	ui->ButtonRot->setEnabled(true);
	ui->ButtonScale->setEnabled(true);
	ui->ButtonSymm->setEnabled(true);
	ui->SpinShear->setEnabled(true);

	ui->label_3->setEnabled(false);
	ui->Farba_vypln->setEnabled(false);

	ui->GroupKrivka->setVisible(false);
	ui->GroupHermit->setVisible(false);

	ui->GroupKamera->setVisible(false);
	ui->GroupIcosfera->setVisible(false);
	ui->GroupTrans3D->setVisible(false);

	//RENDER
	ui->GroupRender->setVisible(false);
	ui->GroupMaterial->setEnabled(false);
	
	LoadUIInput();
}

//ViewerWidget functions
ViewerWidget* ImageViewer::getViewerWidget(int tabId)
{
	QScrollArea* s = static_cast<QScrollArea*>(ui->tabWidget->widget(tabId));
	if (s) {
		ViewerWidget* vW = static_cast<ViewerWidget*>(s->widget());
		return vW;
	}
	return nullptr;
}
ViewerWidget* ImageViewer::getCurrentViewerWidget()
{
	return getViewerWidget(ui->tabWidget->currentIndex());
}

// Event filters
bool ImageViewer::eventFilter(QObject* obj, QEvent* event)
{
	if (obj->objectName() == "ViewerWidget") {
		return ViewerWidgetEventFilter(obj, event);
	}
	return false;
}

//ViewerWidget Events
bool ImageViewer::ViewerWidgetEventFilter(QObject* obj, QEvent* event)
{
	ViewerWidget* w = static_cast<ViewerWidget*>(obj);

	if (!w) {
		return false;
	}

	if (event->type() == QEvent::MouseButtonPress) {
		ViewerWidgetMouseButtonPress(w, event);
	}
	else if (event->type() == QEvent::MouseButtonRelease) {
		ViewerWidgetMouseButtonRelease(w, event);
	}
	else if (event->type() == QEvent::MouseMove) {
		ViewerWidgetMouseMove(w, event);
	}
	else if (event->type() == QEvent::Leave) {
		ViewerWidgetLeave(w, event);
	}
	else if (event->type() == QEvent::Enter) {
		ViewerWidgetEnter(w, event);
	}
	else if (event->type() == QEvent::Wheel) {
		ViewerWidgetWheel(w, event);
	}

	return QObject::eventFilter(obj, event);
}
void ImageViewer::ViewerWidgetMouseButtonPress(ViewerWidget* w, QEvent* event)
{
	QMouseEvent* e = static_cast<QMouseEvent*>(event);

	//LEFT CLICK 
	if (e->button() == Qt::LeftButton) {

		//ak je 3D Vizualizacia sfery
		if (ui->Vyber->currentIndex() == 2 || ui->Vyber->currentIndex() == 3) {
			SurCursor.resize(3);
			SurCursor[0] = e->pos();
		}
		else {
			//teraz uz objekt je nakresleny
			if (ui->ButtonNovy->isEnabled()) {	//ButtonNovy pouzivam na hodnotenie, ci sa kresli novy objekt alebo je nakresleny
				SurCursor.resize(3);
				SurCursor[0] = e->pos();
			}

			//objekt sa prave kresli
			else {

				if (Suradnice.isEmpty()) {		//klika sa prvy bod objektu
					Suradnice.push_back(e->pos());

					if (ui->Vyber->currentIndex() == 1) {		//ak to je prvy bod krivky, vykresli krizik
						QPoint Pos = e->pos();
						getCurrentViewerWidget()->KresliBod(&Pos, &LineAlg);
					}

					ui->label_2->setEnabled(false);		//uzamkne zmenu objektu
					ui->Vyber->setEnabled(false);
				}
				else {
					Suradnice.push_back(e->pos());

					//POLYGON - ak chcem kreslit polygon, bude pridavat body do vektoru a postupne vykreslovat
					if (ui->Vyber->currentIndex() == 0) {
						QVector<QPointF>TempUsecka = { Suradnice[(Suradnice.size() - 2)] ,  Suradnice[(Suradnice.size() - 1)] };

						getCurrentViewerWidget()->Generuj(&TempUsecka, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);
					}

					//KRIVKA - ak kreslim krivku, bude pridavat body do vektoru a vykreslovat body
					else if (ui->Vyber->currentIndex() == 1) {
						QPoint Pos = e->pos();
						getCurrentViewerWidget()->KresliBod(&Pos, &LineAlg);
					}
				}
			}
		}

	}


	//RIGHT CLICK POLYGON
	else if (e->button() == Qt::RightButton & ui->Vyber->currentIndex() == 0 & !ui->ButtonNovy->isEnabled() & Suradnice.size() > 1) {
		//ak kliknem prave tlacidlo a kreslim polygon, tak ten polygon uzavriem

		QVector<QPointF>TempUsecka = { Suradnice[0] ,  Suradnice[(Suradnice.size() - 1)] };

		getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);

		ui->ButtonNovy->setEnabled(true);		//ak dokreslim polygon, zastavi sa kreslenie
		ui->GroupTrans->setEnabled(true);
		ui->Vyplnenie->setEnabled(true);

		if (Suradnice.size() == 2) {		//ak sa vytvori usecka, neda sa nastavit vyplnanie
			ui->label_vypln->setEnabled(false);
			ui->Vypln->setEnabled(false);
			ui->label_3->setEnabled(false);
			ui->Farba_vypln->setEnabled(false);
		}
		else if (Suradnice.size() == 3) {		//ak je to trojuholnik prida 2 moznosti vyplnania
			ui->label_vypln->setEnabled(true);
			ui->Vypln->setEnabled(true);
			ui->Vypln->addItem("Nearest Neighbour");
			ui->Vypln->addItem("Barycentrická interpolácia");
			ui->label_3->setEnabled(false);		//pri trojuholniku sa neda vybrat farba
			ui->Farba_vypln->setEnabled(false);
		}
		else {		//ak je to polygon, prida len jednofarebnu moznost vyplnania
			ui->label_vypln->setEnabled(true);
			ui->Vypln->setEnabled(true);
			ui->Vypln->addItem("Jednofarebná výplň");
		}

		
	
	}

	//RIGHT CLICK CURVE
	else if (e->button() == Qt::RightButton & ui->Vyber->currentIndex() == 1 & !ui->ButtonNovy->isEnabled() & Suradnice.size() > 1) {

		//kontrola, ci je dost riadiacich bodov
		if ((CurveAlg == 0 && Suradnice.size() >= 2) || (CurveAlg == 1 && Suradnice.size() >= 3) || (CurveAlg == 2 && Suradnice.size() >= 4)) {		

			//UI
			ui->ButtonNovy->setEnabled(true);
			ui->GroupKrivka->setEnabled(false);
			ui->Vyplnenie->setEnabled(true);
			ui->GroupTrans->setEnabled(true);

			
			if (CurveAlg == 0) {
				ui->GroupHermit->setEnabled(true);
			}
			else {
				ui->GroupHermit->setEnabled(false);
			}

			HVektory.resize(Suradnice.size());
			ui->SpinPoint->setMaximum(Suradnice.size() - 1);		//uprava vektorov bude obmedzena na pocet tvoriacich bodov

			for (int i = 0; i < HVektory.size(); i++) {		//Naplnenie vektorov defaultnymi suradnicami
				HVektory[i].Dlzka = 100.0;
				HVektory[i].Rot = 0.0;
			}

			getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg, &LineAlg);
		}
	}

}
void ImageViewer::ViewerWidgetMouseButtonRelease(ViewerWidget* w, QEvent* event)
{
	QMouseEvent* e = static_cast<QMouseEvent*>(event);

	//polygon alebo krivka
	if (ui->ButtonNovy->isEnabled() && SurCursor.size()==3 && e->button() == Qt::LeftButton && (ui->Vyber->currentIndex() == 0 || ui->Vyber->currentIndex() == 1)) {
		int i;
		SurCursor[1] = e->pos();
		int DeltaX = SurCursor[1].x() - SurCursor[0].x();
		int DeltaY = SurCursor[1].y() - SurCursor[0].y();

		//zmena suradnic
		for (i = 0; i < Suradnice.size(); i++) {
			Suradnice[i].setX(Suradnice[i].x() + DeltaX);
			Suradnice[i].setY(Suradnice[i].y() + DeltaY);
		}

		getCurrentViewerWidget()->clear();

		if (ui->Vyber->currentIndex() == 0) {
			getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);

		}
		else if (ui->Vyber->currentIndex() == 1) {
			getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg, &LineAlg);
		}
	}
}
void ImageViewer::ViewerWidgetMouseMove(ViewerWidget* w, QEvent* event)
{
	QMouseEvent* e = static_cast<QMouseEvent*>(event);

	//vizualizacia 3D sfery a Mapy
	
	if (SurCursor.size() == 3 && e->buttons() == Qt::LeftButton && (ui->Vyber->currentIndex() == 2 || ui->Vyber->currentIndex() == 3)) {
		int i;
		SurCursor[1] = e->pos();
		int DeltaX = SurCursor[1].x() - SurCursor[0].x();
		int DeltaY = - (SurCursor[1].y() - SurCursor[0].y());
		SurCursor[0] = e->pos();
		double factor = 300.0;		//faktor rychlosti posuvania


		//zmena hodnot v pamati
		Kamera.setAzimut(Kamera.getAzimut() + (double)DeltaX / factor);
		
		if (Kamera.getAzimut() < 0.0) {				//prechod cez okraje, vrati sa naspat
			Kamera.setAzimut(2 * M_PI);
		}
		else if (Kamera.getAzimut() > 2.0 * M_PI) {
			Kamera.setAzimut(0.0);
		}


		Kamera.setZenit(Kamera.getZenit() + (double)DeltaY / factor);

		if (Kamera.getZenit() < 0.0) {				//prechod cez okraje, vrati sa naspat
			Kamera.setZenit(M_PI);
		}
		else if(Kamera.getZenit() > M_PI) {
			Kamera.setZenit(0);
		}
		
		//zastavenie signalov
		ui->zenit->blockSignals(true);
		ui->SliderZenit->blockSignals(true);
		ui->azimut->blockSignals(true);
		ui->SliderAzimut->blockSignals(true);

		//zmena hodnot v UI
		ui->zenit->setValue(Kamera.getZenit() / M_PI * 180.0);
		ui->SliderZenit->setValue(round(ui->zenit->value()));

		ui->azimut->setValue(Kamera.getAzimut() / M_PI * 180.0);
		ui->SliderAzimut->setValue(round(ui->azimut->value()));

		//spustenie signalov
		ui->zenit->blockSignals(false);
		ui->SliderZenit->blockSignals(false);
		ui->azimut->blockSignals(false);
		ui->SliderAzimut->blockSignals(false);

		Refresh3D();
		
	}

	if (ui->ButtonNovy->isEnabled() && SurCursor.size() == 3 && e->buttons() == Qt::LeftButton && (ui->Vyber->currentIndex() == 0 || ui->Vyber->currentIndex() == 1)) {

		int i;
		SurCursor[1] = e->pos();
		int DeltaX = SurCursor[1].x() - SurCursor[0].x();
		int DeltaY = SurCursor[1].y() - SurCursor[0].y();

		QVector<QPointF>TempSur;

		//vytvorenie docasnych suradnic
		for (i = 0; i < Suradnice.size(); i++) {

			TempSur.push_back(QPoint(Suradnice[i].x() + DeltaX, Suradnice[i].y() + DeltaY));
			//Suradnice[i].setX(Suradnice[i].x() + DeltaX);
			//Suradnice[i].setY(Suradnice[i].y() + DeltaY);
		}

		getCurrentViewerWidget()->clear();

		if (ui->Vyber->currentIndex() == 0) {
			getCurrentViewerWidget()->Generuj(&TempSur, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);
		}
		else if (ui->Vyber->currentIndex() == 1) {
			getCurrentViewerWidget()->GenerujKrivku(&TempSur, &HVektory, &PainterColor, &CurveAlg, &LineAlg);
		}
	}
}
void ImageViewer::ViewerWidgetLeave(ViewerWidget* w, QEvent* event)
{
}
void ImageViewer::ViewerWidgetEnter(ViewerWidget* w, QEvent* event)
{
}
void ImageViewer::ViewerWidgetWheel(ViewerWidget* w, QEvent* event)
{
	QWheelEvent* wheelEvent = static_cast<QWheelEvent*>(event);

	//scale
	if (ui->ButtonNovy->isEnabled()) {
		int i;
		float ScaleValue;

		if (wheelEvent->angleDelta().y() > 0) {
			ScaleValue = 1.25;
		}
		else {
			ScaleValue = 0.75;
		}

		int BaseX = Suradnice[0].x();
		int BaseY = Suradnice[0].y();

		getCurrentViewerWidget()->clear();

		for (i = 1; i < Suradnice.size(); i++) {
			Suradnice[i].setX(BaseX + ((Suradnice[i].x() - BaseX) * ScaleValue));
			Suradnice[i].setY(BaseY + ((Suradnice[i].y() - BaseY) * ScaleValue));
		}

		//adaptívne skaluje aj dlzku vektorov
		if (!HVektory.isEmpty()) {
			for (i = 0; i < HVektory.size(); i++) {
				HVektory[i].Dlzka = HVektory[i].Dlzka * ScaleValue;
			}
			ui->SpinCurveLength->setValue(HVektory[ui->SpinPoint->value()].Dlzka);	//upravi hodnotu v spinboxe

			if (HVektory[ui->SpinPoint->value()].Dlzka <= ui->SliderCurveLength->maximum()) {
				//ak je hodnota mensia ako maximalna hodnota slidera, nastavi tam spravnu hodnotu
				ui->SliderCurveLength->setValue(HVektory[ui->SpinPoint->value()].Dlzka);
			}
			else {
				//ak je hodnota vacsia, nastavi tam maximalnu moznu hodnotu
				ui->SliderCurveLength->setValue(ui->SliderCurveLength->maximum());
			}
		}

		if (ui->Vyber->currentIndex() == 0) {
			getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor,&LineAlg, ui->Vypln->currentIndex(), &FillColor);

		}
		else if (ui->Vyber->currentIndex() == 1) {
			getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg, &LineAlg);
		}
	}
}

//ImageViewer Events
void ImageViewer::closeEvent(QCloseEvent* event)
{
	if (QMessageBox::Yes == QMessageBox::question(this, "Close Confirmation", "Are you sure you want to exit?", QMessageBox::Yes | QMessageBox::No))
	{
		event->accept();
	}
	else {
		event->ignore();
	}
}

//Image functions
void ImageViewer::openNewTabForImg(ViewerWidget* vW)
{
	QScrollArea* scrollArea = new QScrollArea;
	scrollArea->setWidget(vW);

	scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidgetResizable(true);
	scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

	vW->setObjectName("ViewerWidget");
	vW->installEventFilter(this);

	QString name = vW->getName();

	ui->tabWidget->addTab(scrollArea, name);
}
bool ImageViewer::openImage(QString filename)
{
	QFileInfo fi(filename);

	QString name = fi.baseName();
	openNewTabForImg(new ViewerWidget(name, QSize(0, 0)));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);

	ViewerWidget* w = getCurrentViewerWidget();

	QImage loadedImg(filename);
	return w->setImage(loadedImg);
}
bool ImageViewer::saveImage(QString filename)
{
	QFileInfo fi(filename);
	QString extension = fi.completeSuffix();
	ViewerWidget* w = getCurrentViewerWidget();

	QImage* img = w->getImage();
	return img->save(filename, extension.toStdString().c_str());
}
void ImageViewer::clearImage()
{
	ViewerWidget* w = getCurrentViewerWidget();
	w->clear();
}
void ImageViewer::setBackgroundColor(QColor color)
{
	ViewerWidget* w = getCurrentViewerWidget();
	w->clear(color);
}

void ImageViewer::LoadUIInput()
{
	Kamera.setZScale(ui->ZScaleSlider->value() / 20.0);

	Kamera.setPointSurX(ui->PointX->value());
	Kamera.setPointSurY(ui->PointY->value());
	Kamera.setPointSurZ(ui->PointZ->value());
}

void ImageViewer::GenerateIcosahedron(GeomObjekt* Objekt_Gen, int hustota)
{
	//vycisti pamat, ak je ulozeny stary objekt
	if (!Objekt_Gen->isNull()) {
		Objekt_Gen->Clear();
	}

	int i;
	QList<Vertex>* Vrcholy;		//pripravim si smernik na pole vrcholov, aby sa mi to nevymazalo potom
	Vrcholy = new QList<Vertex>;		//dynamicky alokujem pamat pre 12 vrcholov
	Vrcholy->reserve(12);

	QList<Face>* Steny;
	Steny = new QList<Face>;
	Steny->reserve(20);

	QList<HalfEdge>* Polohrany;
	Polohrany = new QList<HalfEdge>;
	Polohrany->reserve(60);

	double Zrez = (1.0 + sqrt(5.0)) / 2.0;		//suradnica zo zlateho rezu

		//dopredu si vyplnim hrany a steny, aby som s nimi potom vedel narabat
	for (i = 0; i < 60; i++) {
		*Polohrany << HalfEdge();
	}
	for (i = 0; i < 20; i++) {
		*Steny << Face();
	}

	//####################################################

	{
		//modry obdlznik
		*Vrcholy << Vertex(0, 0.0, +1.0, +Zrez);
		*Vrcholy << Vertex(1, 0.0, -1.0, +Zrez);
		*Vrcholy << Vertex(2, 0.0, +1.0, -Zrez);
		*Vrcholy << Vertex(3, 0.0, -1.0, -Zrez);

		//zlty obdlznik
		*Vrcholy << Vertex(4, +1.0, +Zrez, 0.0);
		*Vrcholy << Vertex(5, +1.0, -Zrez, 0.0);
		*Vrcholy << Vertex(6, -1.0, +Zrez, 0.0);
		*Vrcholy << Vertex(7, -1.0, -Zrez, 0.0);

		//zeleny obdlznik
		*Vrcholy << Vertex(8, +Zrez, 0.0, +1.0);
		*Vrcholy << Vertex(9, +Zrez, 0.0, -1.0);
		*Vrcholy << Vertex(10, -Zrez, 0.0, +1.0);
		*Vrcholy << Vertex(11, -Zrez, 0.0, -1.0);

		//####################################################

		(*Polohrany)[0].setAll(&(*Vrcholy)[1], &(*Steny)[0], &(*Polohrany)[4], &(*Polohrany)[2], &(*Polohrany)[1]);
		(*Polohrany)[1].setAll(&(*Vrcholy)[0], &(*Steny)[1], &(*Polohrany)[7], &(*Polohrany)[9], &(*Polohrany)[0]);

		(*Polohrany)[2].setAll(&(*Vrcholy)[0], &(*Steny)[0], &(*Polohrany)[0], &(*Polohrany)[4], &(*Polohrany)[3]);
		(*Polohrany)[3].setAll(&(*Vrcholy)[8], &(*Steny)[15], &(*Polohrany)[18], &(*Polohrany)[20], &(*Polohrany)[2]);

		(*Polohrany)[4].setAll(&(*Vrcholy)[8], &(*Steny)[0], &(*Polohrany)[2], &(*Polohrany)[0], &(*Polohrany)[5]);
		(*Polohrany)[5].setAll(&(*Vrcholy)[1], &(*Steny)[16], &(*Polohrany)[15], &(*Polohrany)[16], &(*Polohrany)[4]);

		(*Polohrany)[6].setAll(&(*Vrcholy)[0], &(*Steny)[19], &(*Polohrany)[23], &(*Polohrany)[24], &(*Polohrany)[7]);
		(*Polohrany)[7].setAll(&(*Vrcholy)[10], &(*Steny)[1], &(*Polohrany)[9], &(*Polohrany)[1], &(*Polohrany)[6]);

		(*Polohrany)[8].setAll(&(*Vrcholy)[10], &(*Steny)[18], &(*Polohrany)[12], &(*Polohrany)[10], &(*Polohrany)[9]);
		(*Polohrany)[9].setAll(&(*Vrcholy)[1], &(*Steny)[1], &(*Polohrany)[1], &(*Polohrany)[7], &(*Polohrany)[8]);

		(*Polohrany)[10].setAll(&(*Vrcholy)[1], &(*Steny)[18], &(*Polohrany)[8], &(*Polohrany)[12], &(*Polohrany)[11]);
		(*Polohrany)[11].setAll(&(*Vrcholy)[7], &(*Steny)[17], &(*Polohrany)[32], &(*Polohrany)[14], &(*Polohrany)[10]);

		(*Polohrany)[12].setAll(&(*Vrcholy)[7], &(*Steny)[18], &(*Polohrany)[10], &(*Polohrany)[8], &(*Polohrany)[13]);
		(*Polohrany)[13].setAll(&(*Vrcholy)[10], &(*Steny)[12], &(*Polohrany)[27], &(*Polohrany)[30], &(*Polohrany)[12]);

		(*Polohrany)[14].setAll(&(*Vrcholy)[1], &(*Steny)[17], &(*Polohrany)[11], &(*Polohrany)[32], &(*Polohrany)[15]);
		(*Polohrany)[15].setAll(&(*Vrcholy)[5], &(*Steny)[16], &(*Polohrany)[16], &(*Polohrany)[5], &(*Polohrany)[14]);

		(*Polohrany)[16].setAll(&(*Vrcholy)[8], &(*Steny)[16], &(*Polohrany)[5], &(*Polohrany)[15], &(*Polohrany)[17]);
		(*Polohrany)[17].setAll(&(*Vrcholy)[5], &(*Steny)[11], &(*Polohrany)[36], &(*Polohrany)[34], &(*Polohrany)[16]);

		(*Polohrany)[18].setAll(&(*Vrcholy)[4], &(*Steny)[15], &(*Polohrany)[20], &(*Polohrany)[3], &(*Polohrany)[19]);
		(*Polohrany)[19].setAll(&(*Vrcholy)[8], &(*Steny)[10], &(*Polohrany)[35], &(*Polohrany)[38], &(*Polohrany)[18]);

		(*Polohrany)[20].setAll(&(*Vrcholy)[0], &(*Steny)[15], &(*Polohrany)[3], &(*Polohrany)[18], &(*Polohrany)[21]);
		(*Polohrany)[21].setAll(&(*Vrcholy)[4], &(*Steny)[14], &(*Polohrany)[40], &(*Polohrany)[22], &(*Polohrany)[20]);

		(*Polohrany)[22].setAll(&(*Vrcholy)[0], &(*Steny)[14], &(*Polohrany)[21], &(*Polohrany)[40], &(*Polohrany)[23]);
		(*Polohrany)[23].setAll(&(*Vrcholy)[6], &(*Steny)[19], &(*Polohrany)[24], &(*Polohrany)[6], &(*Polohrany)[22]);

		(*Polohrany)[24].setAll(&(*Vrcholy)[10], &(*Steny)[19], &(*Polohrany)[6], &(*Polohrany)[23], &(*Polohrany)[25]);
		(*Polohrany)[25].setAll(&(*Vrcholy)[6], &(*Steny)[13], &(*Polohrany)[28], &(*Polohrany)[26], &(*Polohrany)[24]);

		(*Polohrany)[26].setAll(&(*Vrcholy)[10], &(*Steny)[13], &(*Polohrany)[25], &(*Polohrany)[28], &(*Polohrany)[27]);
		(*Polohrany)[27].setAll(&(*Vrcholy)[11], &(*Steny)[12], &(*Polohrany)[30], &(*Polohrany)[13], &(*Polohrany)[26]);

		(*Polohrany)[28].setAll(&(*Vrcholy)[11], &(*Steny)[13], &(*Polohrany)[26], &(*Polohrany)[25], &(*Polohrany)[29]);
		(*Polohrany)[29].setAll(&(*Vrcholy)[6], &(*Steny)[7], &(*Polohrany)[43], &(*Polohrany)[44], &(*Polohrany)[28]);

		(*Polohrany)[30].setAll(&(*Vrcholy)[7], &(*Steny)[12], &(*Polohrany)[13], &(*Polohrany)[27], &(*Polohrany)[31]);
		(*Polohrany)[31].setAll(&(*Vrcholy)[11], &(*Steny)[6], &(*Polohrany)[47], &(*Polohrany)[48], &(*Polohrany)[30]);

		(*Polohrany)[32].setAll(&(*Vrcholy)[5], &(*Steny)[17], &(*Polohrany)[14], &(*Polohrany)[11], &(*Polohrany)[33]);
		(*Polohrany)[33].setAll(&(*Vrcholy)[7], &(*Steny)[5], &(*Polohrany)[49], &(*Polohrany)[50], &(*Polohrany)[32]);

		(*Polohrany)[34].setAll(&(*Vrcholy)[8], &(*Steny)[11], &(*Polohrany)[17], &(*Polohrany)[36], &(*Polohrany)[35]);
		(*Polohrany)[35].setAll(&(*Vrcholy)[9], &(*Steny)[10], &(*Polohrany)[38], &(*Polohrany)[19], &(*Polohrany)[34]);

		(*Polohrany)[36].setAll(&(*Vrcholy)[9], &(*Steny)[11], &(*Polohrany)[34], &(*Polohrany)[17], &(*Polohrany)[37]);
		(*Polohrany)[37].setAll(&(*Vrcholy)[5], &(*Steny)[4], &(*Polohrany)[51], &(*Polohrany)[52], &(*Polohrany)[36]);

		(*Polohrany)[38].setAll(&(*Vrcholy)[4], &(*Steny)[10], &(*Polohrany)[19], &(*Polohrany)[35], &(*Polohrany)[39]);
		(*Polohrany)[39].setAll(&(*Vrcholy)[9], &(*Steny)[9], &(*Polohrany)[55], &(*Polohrany)[56], &(*Polohrany)[38]);

		(*Polohrany)[40].setAll(&(*Vrcholy)[6], &(*Steny)[14], &(*Polohrany)[22], &(*Polohrany)[21], &(*Polohrany)[41]);
		(*Polohrany)[41].setAll(&(*Vrcholy)[4], &(*Steny)[8], &(*Polohrany)[57], &(*Polohrany)[42], &(*Polohrany)[40]);

		(*Polohrany)[42].setAll(&(*Vrcholy)[6], &(*Steny)[8], &(*Polohrany)[41], &(*Polohrany)[57], &(*Polohrany)[43]);
		(*Polohrany)[43].setAll(&(*Vrcholy)[2], &(*Steny)[7], &(*Polohrany)[44], &(*Polohrany)[29], &(*Polohrany)[42]);

		(*Polohrany)[44].setAll(&(*Vrcholy)[11], &(*Steny)[7], &(*Polohrany)[29], &(*Polohrany)[43], &(*Polohrany)[45]);
		(*Polohrany)[45].setAll(&(*Vrcholy)[2], &(*Steny)[3], &(*Polohrany)[59], &(*Polohrany)[46], &(*Polohrany)[44]);

		(*Polohrany)[46].setAll(&(*Vrcholy)[11], &(*Steny)[3], &(*Polohrany)[45], &(*Polohrany)[59], &(*Polohrany)[47]);
		(*Polohrany)[47].setAll(&(*Vrcholy)[3], &(*Steny)[6], &(*Polohrany)[48], &(*Polohrany)[31], &(*Polohrany)[46]);

		(*Polohrany)[48].setAll(&(*Vrcholy)[7], &(*Steny)[6], &(*Polohrany)[31], &(*Polohrany)[47], &(*Polohrany)[49]);
		(*Polohrany)[49].setAll(&(*Vrcholy)[3], &(*Steny)[5], &(*Polohrany)[50], &(*Polohrany)[33], &(*Polohrany)[48]);

		(*Polohrany)[50].setAll(&(*Vrcholy)[5], &(*Steny)[5], &(*Polohrany)[33], &(*Polohrany)[49], &(*Polohrany)[51]);
		(*Polohrany)[51].setAll(&(*Vrcholy)[3], &(*Steny)[4], &(*Polohrany)[52], &(*Polohrany)[37], &(*Polohrany)[50]);

		(*Polohrany)[52].setAll(&(*Vrcholy)[9], &(*Steny)[4], &(*Polohrany)[37], &(*Polohrany)[51], &(*Polohrany)[53]);
		(*Polohrany)[53].setAll(&(*Vrcholy)[3], &(*Steny)[2], &(*Polohrany)[58], &(*Polohrany)[54], &(*Polohrany)[52]);

		(*Polohrany)[54].setAll(&(*Vrcholy)[9], &(*Steny)[2], &(*Polohrany)[53], &(*Polohrany)[58], &(*Polohrany)[55]);
		(*Polohrany)[55].setAll(&(*Vrcholy)[2], &(*Steny)[9], &(*Polohrany)[56], &(*Polohrany)[39], &(*Polohrany)[54]);

		(*Polohrany)[56].setAll(&(*Vrcholy)[4], &(*Steny)[9], &(*Polohrany)[39], &(*Polohrany)[55], &(*Polohrany)[57]);
		(*Polohrany)[57].setAll(&(*Vrcholy)[2], &(*Steny)[8], &(*Polohrany)[42], &(*Polohrany)[41], &(*Polohrany)[56]);

		(*Polohrany)[58].setAll(&(*Vrcholy)[2], &(*Steny)[2], &(*Polohrany)[54], &(*Polohrany)[53], &(*Polohrany)[59]);
		(*Polohrany)[59].setAll(&(*Vrcholy)[3], &(*Steny)[3], &(*Polohrany)[46], &(*Polohrany)[45], &(*Polohrany)[58]);

		//####################################################

		(*Steny)[0].setEdge(&(*Polohrany)[0]);
		(*Steny)[1].setEdge(&(*Polohrany)[1]);
		(*Steny)[2].setEdge(&(*Polohrany)[54]);
		(*Steny)[3].setEdge(&(*Polohrany)[59]);
		(*Steny)[4].setEdge(&(*Polohrany)[52]);


		(*Steny)[5].setEdge(&(*Polohrany)[50]);
		(*Steny)[6].setEdge(&(*Polohrany)[31]);
		(*Steny)[7].setEdge(&(*Polohrany)[29]);
		(*Steny)[8].setEdge(&(*Polohrany)[41]);
		(*Steny)[9].setEdge(&(*Polohrany)[39]);

		(*Steny)[10].setEdge(&(*Polohrany)[19]);
		(*Steny)[11].setEdge(&(*Polohrany)[34]);
		(*Steny)[12].setEdge(&(*Polohrany)[13]);
		(*Steny)[13].setEdge(&(*Polohrany)[25]);
		(*Steny)[14].setEdge(&(*Polohrany)[21]);

		(*Steny)[15].setEdge(&(*Polohrany)[3]);
		(*Steny)[16].setEdge(&(*Polohrany)[5]);
		(*Steny)[17].setEdge(&(*Polohrany)[14]);
		(*Steny)[18].setEdge(&(*Polohrany)[10]);
		(*Steny)[19].setEdge(&(*Polohrany)[6]);
	}

	//####################################################

	Objekt_Gen->setVrcholy(Vrcholy);
	Objekt_Gen->setHrany(Polohrany);
	Objekt_Gen->setSteny(Steny);

	if (hustota != 0) {
		SubdivideIcosahedron(Objekt_Gen, hustota);
	}

	Objekt_Gen->normalizujVrcholy();
	Objekt_Gen->CalculateVertexNormals();

	QMessageBox msgBox;
	msgBox.setText("Objekt bol úspešne vygenerovaný.");
	msgBox.exec();
}
void ImageViewer::SubdivideIcosahedron(GeomObjekt* Objekt_Gen, int hustota)
{
	int i, j;
	long double x0, x1,y0,y1,z0,z1;
	long double xs, ys, zs;	//stredove suradnice
	Vertex *vrchol0, *vrchol1, *vrchol2, *tempvrchol;
	int nStien;
	
	for (j = 0; j < hustota; j++) {
		nStien = Objekt_Gen->getStenySize();

		for (i = 0; i < nStien; i++) {
			
			//####### - PRVY VRCHOL - ####################
			{
				x0 = Objekt_Gen->getStenaAt(i)->getHrana()->getBod()->getX();	//povodny bod, sur x
				x1 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_next()->getBod()->getX();		//nasledujuci bod, sur x
				xs = (x0 + x1) / 2.0;

				y0 = Objekt_Gen->getStenaAt(i)->getHrana()->getBod()->getY();	//povodny bod, sur y
				y1 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_next()->getBod()->getY();		//nasledujuci bod, sur y
				ys = (y0 + y1) / 2.0;

				z0 = Objekt_Gen->getStenaAt(i)->getHrana()->getBod()->getZ();	//povodny bod, sur z
				z1 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_next()->getBod()->getZ();		//nasledujuci bod, sur z
				zs = (z0 + z1) / 2.0;


				vrchol0 = new Vertex(Objekt_Gen->getVrcholySize(), xs, ys, zs);
				tempvrchol = vrchol0;
				vrchol0 = Objekt_Gen->DuplicitnyVrchol(vrchol0, j);		//overi sa duplicita, ak existuje, nastavi sa na tento smernik

				if (vrchol0 != tempvrchol) {
					delete tempvrchol;		//ak dany bod uz existuje, uvolnim pamat
				}
				else {
					Objekt_Gen->addVrchol(vrchol0);		//ak bod este neexistuje, pridam ho do objektu a zatial si ho zapamatam
					vrchol0 = Objekt_Gen->DuplicitnyVrchol(vrchol0, j);
				}
			}

			//####### - DRUHY VRCHOL - ###################
			{
				x0 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_next()->getBod()->getX();	//povodny bod, sur x
				x1 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_prev()->getBod()->getX();		//nasledujuci bod, sur x
				xs = (x0 + x1) / 2.0;

				y0 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_next()->getBod()->getY();	//povodny bod, sur y
				y1 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_prev()->getBod()->getY();		//nasledujuci bod, sur y
				ys = (y0 + y1) / 2.0;

				z0 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_next()->getBod()->getZ();	//povodny bod, sur z
				z1 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_prev()->getBod()->getZ();		//nasledujuci bod, sur z
				zs = (z0 + z1) / 2.0;


				vrchol1 = new Vertex(Objekt_Gen->getVrcholySize(), xs, ys, zs);
				tempvrchol = vrchol1;
				vrchol1 = Objekt_Gen->DuplicitnyVrchol(vrchol1,j);		//overi sa duplicita, ak existuje, nastavi sa na tento smernik
				if (vrchol1 != tempvrchol) {
					delete tempvrchol;		//ak dany bod uz existuje, uvolnim pamat
				}
				else {
					Objekt_Gen->addVrchol(vrchol1);		//ak bod este neexistuje, pridam ho do objektu a zatial si ho zapamatam
					vrchol1 = Objekt_Gen->DuplicitnyVrchol(vrchol1, j);
				}
			}

			//####### - TRETI VRCHOL - ##################
			{
				x0 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_prev()->getBod()->getX();	//povodny bod, sur x
				x1 = Objekt_Gen->getStenaAt(i)->getHrana()->getBod()->getX();		//nasledujuci bod, sur x
				xs = (x0 + x1) / 2.0;

				y0 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_prev()->getBod()->getY();	//povodny bod, sur y
				y1 = Objekt_Gen->getStenaAt(i)->getHrana()->getBod()->getY();		//nasledujuci bod, sur y
				ys = (y0 + y1) / 2.0;

				z0 = Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_prev()->getBod()->getZ();	//povodny bod, sur z
				z1 = Objekt_Gen->getStenaAt(i)->getHrana()->getBod()->getZ();		//nasledujuci bod, sur z
				zs = (z0 + z1) / 2.0;


				vrchol2 = new Vertex(Objekt_Gen->getVrcholySize(), xs, ys, zs);
				tempvrchol = vrchol2;
				vrchol2 = Objekt_Gen->DuplicitnyVrchol(vrchol2,j);		//overi sa duplicita, ak existuje, nastavi sa na tento smernik
				if (vrchol2 != tempvrchol) {
					delete tempvrchol;		//ak dany bod uz existuje, uvolnim pamat
				}
				else {
					Objekt_Gen->addVrchol(vrchol2);		//ak bod este neexistuje, pridam ho do objektu a zatial si ho zapamatam
					vrchol2 = Objekt_Gen->DuplicitnyVrchol(vrchol2, j);
				}
			}

			//####### - NOVE HRANY - ##################

			Face* stena0 = new Face();
			Face* stena1 = new Face();
			Face* stena2 = new Face();
			Face* stena3 = new Face();

			HalfEdge* e0 = new HalfEdge();
			HalfEdge* e1 = new HalfEdge();
			HalfEdge* e2 = new HalfEdge();
			HalfEdge* e3 = new HalfEdge();
			HalfEdge* e4 = new HalfEdge();
			HalfEdge* e5 = new HalfEdge();
			HalfEdge* e6 = new HalfEdge();
			HalfEdge* e7 = new HalfEdge();
			HalfEdge* e8 = new HalfEdge();
			HalfEdge* e9 = new HalfEdge();
			HalfEdge* e10 = new HalfEdge();
			HalfEdge* e11 = new HalfEdge();

			//STREDNY TROJUHOLNIK (0)

			e0->setAll(vrchol0, stena0, e4, e2, e1);
			e2->setAll(vrchol1, stena0, e0, e4, e3);
			e4->setAll(vrchol2, stena0, e2, e0, e4);
			stena0->setEdge(e0);

			//TROJUHOLNIK (1)

			e6->setAll(Objekt_Gen->getStenaAt(i)->getHrana()->getBod(), stena1, e7, e5);		//parove polohrany este nepoznam, doplnim ich nakoniec
			e5->setAll(vrchol0, stena1, e6, e7, e4);
			e7->setAll(vrchol2, stena1, e5, e6);
			stena1->setEdge(e6);

			//TROJUHOLNIK (2)

			e8->setAll(vrchol0, stena2, e1, e9);		//parove polohrany este nepoznam, doplnim ich nakoniec
			e9->setAll(Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_next()->getBod(), stena2, e8, e1);
			e1->setAll(vrchol1, stena2, e9, e8, e0);
			stena2->setEdge(e8);

			//TROJUHOLNIK (3)

			e3->setAll(vrchol2, stena3, e11, e10, e2);		//parove polohrany este nepoznam, doplnim ich nakoniec
			e10->setAll(vrchol1, stena3, e3, e11);
			e11->setAll(Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_prev()->getBod(), stena3, e10, e3);
			stena3->setEdge(e3);

			Objekt_Gen->addHrana(e0);
			Objekt_Gen->addHrana(e1);
			Objekt_Gen->addHrana(e2);
			Objekt_Gen->addHrana(e3);
			Objekt_Gen->addHrana(e4);
			Objekt_Gen->addHrana(e5);
			Objekt_Gen->addHrana(e6);
			Objekt_Gen->addHrana(e7);
			Objekt_Gen->addHrana(e8);
			Objekt_Gen->addHrana(e9);
			Objekt_Gen->addHrana(e10);
			Objekt_Gen->addHrana(e11);

			Objekt_Gen->addStena(stena0);
			Objekt_Gen->addStena(stena1);
			Objekt_Gen->addStena(stena2);
			Objekt_Gen->addStena(stena3);

			//upravim hrany naviazujuce sa na povodne vrcholy
			Objekt_Gen->getStenaAt(i)->getHrana()->getBod()->setEdge(e6);
			Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_next()->getBod()->setEdge(e9);
			Objekt_Gen->getStenaAt(i)->getHrana()->getHrana_prev()->getBod()->setEdge(e11);
			

			//upravim hrany naviazujuce sa na nove vrcholy
			vrchol0->setEdge(e0);
			vrchol1->setEdge(e2);
			vrchol2->setEdge(e4);
	
		}

		Objekt_Gen->deleteStenyBatch(j);
		Objekt_Gen->deleteHranyBatch(j);
		Objekt_Gen->najdiParoveHrany();
	}

}

bool ImageViewer::ExportObject(GeomObjekt* O)
{
	QString fileName = QFileDialog::getSaveFileName(this, "Save file", "", tr(".vtk File (*vtk)"));		//dialog pre vyber miesta na ulozenie
	if (fileName.isEmpty()) { return false; }			//kontrola adresy

	if (!fileName.endsWith(".vtk")) {		//pridanie koncovky, ak nie je napisana manualne
		fileName += ".vtk";
	}

	QFile file(fileName);                              //zapisanie do suboru
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return false;

	int i;
	QTextStream out(&file);				//do out budem vypisovat veci na ulozenie

	out << "# vtk DataFile Version 3.0\n";
	out << "vtk output\nASCII\nDATASET POLYDATA\n";
	
	//VRCHOLY
	out << "POINTS " << O->getVrcholySize() << " float\n";
	for (i = 0; i < O->getVrcholySize(); i++) {
		out << O->PrintVrcholySurAt(i) << "\n";
	}


	//HRANY
	out << "\nLINES " << O->getHranySize() /2 << " " << (O->getHranySize() / 2) *3 << "\n";

	for (i = 0; i < O->getHranySize(); i++) {
		if(O->getPairHranaIndexAt(i) < O->getHranaIndexAt(i)) {		//porovnavam indexy zaciatocnych vrcholov, aby som kazdu polhranu napisal iba raz
			out << "2 " << O->PrintHranyBodyAt(i) << "\n";	
		}
	}


	//STENY
	out << "\nPOLYGONS " << O->getStenySize() << " " << O->getStenySize()*4 << "\n";
	for (i = 0; i < O->getStenySize(); i++) {
		out << "3 " << O->PrintStenyBodyAt(i) << "\n";
	}
	
	file.close();
	return true;
}
bool ImageViewer::ImportObject(GeomObjekt* O)
{
	//INICIALIZACIA

	QString fileName;
	bool correct = true;
	QList<Vertex>* Vrcholy = new QList<Vertex>;
	QList<HalfEdge>* Hrany = new QList<HalfEdge>;
	QList<Face>* Steny = new QList<Face>;

	unsigned char format;	//0 = VTK, 1 = DAT

	//ZISKANIE ADRESY SUBORU

	if (ui->Vyber->currentIndex() == 2) {
		fileName = QFileDialog::getOpenFileName(this, "Open file", "", tr(".vtk File (*vtk)"));
		format = 0;
	}
	else if (ui->Vyber->currentIndex() == 3) {
		fileName = QFileDialog::getOpenFileName(this, "Open file", "", tr(".dat File (*dat)"));
		format = 1;
	}

	//KONTROLA ADRESY A OTVORENIE SUBORU

	if (fileName.isEmpty()) { return false; }


	if (!O->isNull()) {
		O->Clear();
	}
	
	QFile ObjectFile(fileName);            
	if (!ObjectFile.open(QIODevice::ReadOnly)) {      //kontrola spravneho otvorenia suboru
		QMessageBox msgBox;
		msgBox.setText("Subor neexistuje.");
		msgBox.exec();
		return false;
	}

	if (format == 0) {

		correct = CheckVtkHeader(&ObjectFile);

		if (!correct) {
			QMessageBox msgBox;
			msgBox.setText("V nahravani suboru nastala chyba, sekcia: Hlavicka");
			msgBox.exec();
			return false;
		}

		correct = FillVtkPoints(&ObjectFile, Vrcholy);

		if (!correct) {
			QMessageBox msgBox;
			msgBox.setText("V nahravani suboru nastala chyba, sekcia: Vrcholy");
			msgBox.exec();
			return false;
		}

		correct = FillVtkEdges(&ObjectFile, Hrany, Vrcholy);

		if (!correct) {
			QMessageBox msgBox;
			msgBox.setText("V nahravani suboru nastala chyba, sekcia: Hrany");
			msgBox.exec();
			return false;
		}

		correct = FillVtkFaces(&ObjectFile, Steny, Hrany, Vrcholy);

		if (!correct) {
			QMessageBox msgBox;
			msgBox.setText("V nahravani suboru nastala chyba, sekcia: Steny");
			msgBox.exec();
			return false;
		}

	}

	if (format == 1) {

		correct = FillDatPoints(&ObjectFile, Vrcholy);

		if (!correct) {
			QMessageBox msgBox;
			msgBox.setText("V nahravani suboru nastala chyba. Sekcia: Vrcholy");
			msgBox.exec();
			return false;
		}
		

		correct = FillDatEdges(Vrcholy, Hrany, Steny);

		if (!correct) {
			QMessageBox msgBox;
			msgBox.setText("V nahravani suboru nastala chyba. Sekcia: Hrany/Plosky");
			msgBox.exec();
			return false;
		}

		correct = RescaleDatPoints(Vrcholy);

		if (!correct) {
			QMessageBox msgBox;
			msgBox.setText("V nahravani suboru nastala chyba. Sekcia: Rescale Vrcholy");
			msgBox.exec();
			return false;
		}
	}

	O->setVrcholy(Vrcholy);
	O->setHrany(Hrany);
	O->setSteny(Steny);
	O->CalculateVertexNormals();
	if (format == 0) {
		O->najdiParoveHrany();
	}
	else if (format == 1) {
		O->CalculateVertexColors_Batymetry();
		O->FillHeightBuffer();
		O->setAmb(QColor(64, 64, 64));
		O->setDiff(QColor(255, 255, 255));
		O->setGloss(QColor(255, 255, 255));
		O->setRough(3.0);
	}

	return true;
}


void ImageViewer::Refresh3D()
{
	getCurrentViewerWidget()->resetZBuffer();

	//SHADED
	if (Kamera.getRenderMode() == 0) {

		if (Objekt3DSur != nullptr) Objekt3DSur->clear();		//pripravi si list pre trojice 2D suradnic
		Objekt3DSur = new QList<QVector<zBufferUnit>>;				//inicializacia, aby to uz nebol nullptr

		//SFERA
		if (ui->Vyber->currentIndex() == 2) {
			if (GenObjekt != nullptr && ui->radio_Gen->isChecked()) {		//generovany objekt
				GenObjekt->CalculateVertexColors(&Kamera);
				Calculate3DObject(GenObjekt, Objekt3DSur);

			}
			else if (ImpObjekt != nullptr && ui->radio_Imp->isChecked()) {		//importovany objekt
				ImpObjekt->CalculateVertexColors(&Kamera);
				Calculate3DObject(ImpObjekt, Objekt3DSur);
			}
		}
		//MAPA
		if (ui->Vyber->currentIndex() == 3) {
			if (ImpObjekt != nullptr) {
				ImpObjekt->CalculateVertexColors(&Kamera);
				ImpObjekt->ScaleZ(Kamera.getZScale());
				Calculate3DObject(ImpObjekt, Objekt3DSur);
			}
		}

		//potom vykresli 2D polygony
		if (Objekt3DSur != nullptr) {
			getCurrentViewerWidget()->KresliPolygony(Objekt3DSur, &PainterColor, &Kamera);
			getCurrentViewerWidget()->KresliZBuffer();
		}

	}


	//WIREFRAME
	else if (Kamera.getRenderMode() == 1) {

		if (Objekt3DSur != nullptr) Objekt3DSur->clear();		//pripravi si list pre trojice 2D suradnic
		Objekt3DSur = new QList<QVector<zBufferUnit>>;				//inicializacia, aby to uz nebol nullptr

		//SFERA
		if (ui->Vyber->currentIndex() == 2) {
			if (GenObjekt != nullptr && ui->radio_Gen->isChecked()) {		//prekonvertuje generovany objekt na 2D sur
				Calculate3DObject(GenObjekt, Objekt3DSur);
			}			//praca s generovanym objektom
			else if (ImpObjekt != nullptr && ui->radio_Imp->isChecked()) {		//prekonvertuje importovany objekt na 2D sur
				Calculate3DObject(ImpObjekt, Objekt3DSur);
			}			//praca s importovanym objektom
		}
		//MAPA
		else if (ui->Vyber->currentIndex() == 3) {
			if (ImpObjekt != nullptr) {			//pripravi si prazdny objekt na ukladanie dat
				ImpObjekt->ScaleZ(Kamera.getZScale());
				Calculate3DObject(ImpObjekt, Objekt3DSur);
			}
		}

		//potom vykresli 2D polygony
		if (Objekt3DSur != nullptr) {
			getCurrentViewerWidget()->KresliPolygony(Objekt3DSur, &PainterColor, &Kamera);
		}
	}
}
void ImageViewer::Calculate3DObject(GeomObjekt* O, QList<QVector<zBufferUnit>>* Sur3d)
{
	Sur3d->reserve(O->getStenySize());
	if (Kamera.getRezim() == 0) CalculateIsometric(O, *Sur3d);
	else if (Kamera.getRezim() == 1) CalculatePerspective(O, *Sur3d);
}
void ImageViewer::CalculatePerspective(GeomObjekt* O, QList<QVector<zBufferUnit>>& Sur3d)
{
	int i;
	double stareX, stareY, stareZ, noveX, noveY, noveZ, p, dist;

	//vyjadrenie roviny ax + by + cz + d = 0, d=0, pretoze rovina prechadza pociatkom sustavy
	double a = Kamera.get_n_x();		//a,b,c su suradnice normaloveho vektora
	double b = Kamera.get_n_y();
	double c = Kamera.get_n_z();

	QVector3D u = Kamera.get_u();			//ulozim si jednotkove vektory priemetne, aby som s nimi vedel lepsie narabat
	QVector3D v = Kamera.get_v();
	QVector3D S = Kamera.get_n();			//priprava suradnic stredu premietania
	S.setX(S.x() * Kamera.getStred());
	S.setY(S.y() * Kamera.getStred());
	S.setZ(S.z() * Kamera.getStred());

	double s, t;						//parametre linearnej kombinacie
	QVector<zBufferUnit> Body(3);
	double height = getCurrentViewerWidget()->getImgHeight() / 2.0;		//najdenie stredu platna
	double width = getCurrentViewerWidget()->getImgWidth() / 2.0;

	for (i = 0; i < O->getStenySize(); i++) {
		//1. hrana
		{
			Vertex staryBod = *O->getStenaAt(i)->getHrana()->getBod();
			stareX = staryBod.getX();
			stareY = staryBod.getY();
			stareZ = staryBod.getZ();

			//porovnanie s near_clipping a far_clipping
			dist = calcDistance(stareX, stareY, stareZ);
			if (dist < Kamera.getClipNear() || dist > Kamera.getClipFar()) {
				continue;
			}

			p = ((a * stareX + b * stareY + c * stareZ) / (a * (S.x() - stareX) + b * (S.y() - stareY) + c * (S.z() - stareZ)));		//medzivypocet, p je v prednaske zapisane ako t

			//suradnice bodov na priemetni, ale v 3D
			noveX = stareX + (stareX - S.x()) * p;
			noveY = stareY + (stareY - S.y()) * p;
			noveZ = stareZ + (stareZ - S.z()) * p;

			//vypocet linearnej kombinacie pre ziskanie suradnic z 3D v 2D
			//pre vypocet s zadavam podmienky, aby nenastali problemy v zobrazeniach rovnobeznych s osami
			t = (u.x() * noveY - u.y() * noveX) / (v.y() * u.x() - v.x() * u.y());
			if (Kamera.getZenit() == M_PI_2) {
				s = (noveZ - t * v.z()) / u.z();
			}
			else if (Kamera.getAzimut() == M_PI_2 || Kamera.getAzimut() == 3 * M_PI_2) {
				s = (noveY - t * v.y()) / u.y();
			}
			else {
				s = (noveX - t * v.x()) / u.x();
			}

			Body[0].Sur.setX(width + t * Kamera.getScale() + Kamera.getTransX());		//preskalovanie novych suradnic velkostou objektu (zoom)
			Body[0].Sur.setY(height + s * Kamera.getScale() + Kamera.getTransY());
			Body[0].Z = QVector3D(stareX, stareY, stareZ).distanceToPlane(QVector3D(0.0, 0.0, 0.0), Kamera.get_n());		//metoda na vypocet vzdialenosti od roviny
			try {
				Body[0].color = *staryBod.getColor();
			}
			catch (int c) {}

			
		}

		//2. hrana
		{
			Vertex staryBod = *O->getStenaAt(i)->getHrana()->getHrana_next()->getBod();
			stareX = staryBod.getX();
			stareY = staryBod.getY();
			stareZ = staryBod.getZ();
			p = ((a * stareX + b * stareY + c * stareZ) / (a * (S.x() - stareX) + b * (S.y() - stareY) + c * (S.z() - stareZ)));		//medzivypocet

			//porovnanie s near_clipping a far_clipping
			dist = calcDistance(stareX, stareY, stareZ);
			if (dist < Kamera.getClipNear() || dist > Kamera.getClipFar()) {
				continue;
			}

			//suradnice bodov na priemetni, ale v 3D
			noveX = stareX + (stareX - S.x()) * p;
			noveY = stareY + (stareY - S.y()) * p;
			noveZ = stareZ + (stareZ - S.z()) * p;

			//vypocet linearnej kombinacie pre ziskanie suradnic z 3D v 2D
			//pre vypocet s zadavam podmienky, aby nenastali problemy v zobrazeniach rovnobeznych s osami
			t = (u.x() * noveY - u.y() * noveX) / (v.y() * u.x() - v.x() * u.y());
			if (Kamera.getZenit() == M_PI_2) {
				s = (noveZ - t * v.z()) / u.z();
			}
			else if (Kamera.getAzimut() == M_PI_2 || Kamera.getAzimut() == 3 * M_PI_2) {
				s = (noveY - t * v.y()) / u.y();
			}
			else {
				s = (noveX - t * v.x()) / u.x();
			}

			Body[1].Sur.setX(width + t * Kamera.getScale() + Kamera.getTransX());		//preskalovanie novych suradnic velkostou objektu (zoom)
			Body[1].Sur.setY(height + s * Kamera.getScale() + Kamera.getTransY());
			Body[1].Z = QVector3D(stareX, stareY, stareZ).distanceToPlane(QVector3D(0.0, 0.0, 0.0), Kamera.get_n());		//metoda na vypocet vzdialenosti od roviny
			try {
				Body[1].color = *staryBod.getColor();
			}
			catch (int c) {}
		}

		//3. hrana
		{
			Vertex staryBod = *O->getStenaAt(i)->getHrana()->getHrana_prev()->getBod();
			stareX = staryBod.getX();
			stareY = staryBod.getY();
			stareZ = staryBod.getZ();
			p = ((a * stareX + b * stareY + c * stareZ) / (a * (S.x() - stareX) + b * (S.y() - stareY) + c * (S.z() - stareZ)));		//medzivypocet


			//porovnanie s near_clipping a far_clipping
			dist = calcDistance(stareX, stareY, stareZ);
			if (dist < Kamera.getClipNear() || dist > Kamera.getClipFar()) {
				continue;
			}

			//suradnice bodov na priemetni, ale v 3D
			noveX = stareX + (stareX - S.x()) * p;
			noveY = stareY + (stareY - S.y()) * p;
			noveZ = stareZ + (stareZ - S.z()) * p;

			//vypocet linearnej kombinacie pre ziskanie suradnic z 3D v 2D
			//pre vypocet s zadavam podmienky, aby nenastali problemy v zobrazeniach rovnobeznych s osami
			t = (u.x() * noveY - u.y() * noveX) / (v.y() * u.x() - v.x() * u.y());
			if (Kamera.getZenit() == M_PI_2) {
				s = (noveZ - t * v.z()) / u.z();
			}
			else if (Kamera.getAzimut() == M_PI_2 || Kamera.getAzimut() == 3 * M_PI_2) {
				s = (noveY - t * v.y()) / u.y();
			}
			else {
				s = (noveX - t * v.x()) / u.x();
			}

			Body[2].Sur.setX(width + t * Kamera.getScale() + Kamera.getTransX());		//preskalovanie novych suradnic velkostou objektu (zoom)
			Body[2].Sur.setY(height + s * Kamera.getScale() + Kamera.getTransY());
			Body[2].Z = QVector3D(stareX, stareY, stareZ).distanceToPlane(QVector3D(0.0, 0.0, 0.0), Kamera.get_n());		//metoda na vypocet vzdialenosti od roviny
			try {
				Body[2].color = *staryBod.getColor();
			}
			catch (int c) {}
		}

		Sur3d << Body;			//qvektory bodov si ulozim, aby som ich potom zobrazil
	}
}
void ImageViewer::CalculateIsometric(GeomObjekt* O, QList<QVector<zBufferUnit>>& Sur3d)
{
	int i;
	double stareX, stareY, stareZ, noveX, noveY, noveZ, p, dist;

	//vyjadrenie roviny ax + by + cz + d = 0, d=0, pretoze rovina prechadza pociatkom sustavy
	double a = Kamera.get_n_x();		//a,b,c su suradnice normaloveho vektora
	double b = Kamera.get_n_y();
	double c = Kamera.get_n_z();
	
	QVector3D u = Kamera.get_u();			//ulozim si jednotkove vektory priemetne, aby som s nimi vedel lepsie narabat
	QVector3D v = Kamera.get_v();
	double s, t;						//parametre linearnej kombinacie
	QVector<zBufferUnit> Body(3);

	double height = getCurrentViewerWidget()->getImgHeight() / 2.0;		//najdenie stredu platna
	double width = getCurrentViewerWidget()->getImgWidth() / 2.0;


	for (i = 0; i < O->getStenySize(); i++) {

		//0. hrana
		{
			Vertex staryBod = *O->getStenaAt(i)->getHrana()->getBod();
			stareX = staryBod.getX();
			stareY = staryBod.getY();
			stareZ = staryBod.getZ();
			p = ((a * stareX + b * stareY + c * stareZ) / (a * a + b * b + c * c));		//medzivypocet

			//porovnanie s near_clipping a far_clipping
			dist = calcDistance(stareX, stareY, stareZ);
			if (dist < Kamera.getClipNear() || dist > Kamera.getClipFar()) {
				continue;
			}

			//suradnice bodov na priemetni, ale v 3D
			noveX = stareX - a * p;
			noveY = stareY - b * p;
			noveZ = stareZ - c * p;

			//vypocet linearnej kombinacie pre ziskanie suradnic z 3D v 2D
			//pre vypocet s zadavam podmienky, aby nenastali problemy v zobrazeniach rovnobeznych s osami
			t = (u.x() * noveY - u.y() * noveX) / (v.y() * u.x() - v.x() * u.y());
			if (Kamera.getZenit() == M_PI_2) {
				s = (noveZ - t * v.z()) / u.z();
			}
			else if (Kamera.getAzimut() == M_PI_2 || Kamera.getAzimut() == 3 * M_PI_2) {
				s = (noveY - t * v.y()) / u.y();
			}
			else {
				s = (noveX - t * v.x()) / u.x();
			}


			Body[0].Sur.setX(width + t * Kamera.getScale() + Kamera.getTransX());		//preskalovanie novych suradnic velkostou objektu (zoom)
			Body[0].Sur.setY(height + s * Kamera.getScale() + Kamera.getTransY());
			Body[0].Z = QVector3D(stareX, stareY, stareZ).distanceToPlane(QVector3D(0.0, 0.0, 0.0), Kamera.get_n());		//metoda na vypocet vzdialenosti od roviny
			try {
				Body[0].color = *staryBod.getColor();
			}
			catch (int c) {}
		}

		//1. hrana
		{
			Vertex staryBod = *O->getStenaAt(i)->getHrana()->getHrana_next()->getBod();
			stareX = staryBod.getX();
			stareY = staryBod.getY();
			stareZ = staryBod.getZ();
			p = ((a * stareX + b * stareY + c * stareZ) / (a * a + b * b + c * c));
	
			//porovnanie s near_clipping a far_clipping
			dist = calcDistance(stareX, stareY, stareZ);
			if (dist < Kamera.getClipNear() || dist > Kamera.getClipFar()) {
				continue;
			}

			//suradnice bodov na priemetni v 3D
			noveX = stareX - a * p;
			noveY = stareY - b * p;
			noveZ = stareZ - c * p;


			//vypocet linearnej kombinacie pre ziskanie suradnic z 3D v 2D
			//pre vypocet s zadavam podmienky, aby nenastali problemy v zobrazeniach rovnobeznych s osami
			t = (u.x() * noveY - u.y() * noveX) / (v.y() * u.x() - v.x() * u.y());
			if (Kamera.getZenit() == M_PI_2) {
				s = (noveZ - t * v.z()) / u.z();
			}
			else if (Kamera.getAzimut() == M_PI_2 || Kamera.getAzimut() == 3 * M_PI_2) {
				s = (noveY - t * v.y()) / u.y();
			}
			else {
				s = (noveX - t * v.x()) / u.x();
			}

			Body[1].Sur.setX(width + t * Kamera.getScale() + Kamera.getTransX());		//preskalovanie novych suradnic velkostou objektu (zoom)
			Body[1].Sur.setY(height + s * Kamera.getScale() + Kamera.getTransY());
			Body[1].Z = QVector3D(stareX, stareY, stareZ).distanceToPlane(QVector3D(0.0, 0.0, 0.0), Kamera.get_n());		//metoda na vypocet vzdialenosti od roviny
			try {
				Body[1].color = *staryBod.getColor();
			}
			catch (int c) {}
		}

		//2. hrana
		{

			Vertex staryBod = *O->getStenaAt(i)->getHrana()->getHrana_prev()->getBod();
			stareX = staryBod.getX();
			stareY = staryBod.getY();
			stareZ = staryBod.getZ();
			p = ((a * stareX + b * stareY + c * stareZ) / (a * a + b * b + c * c));

			//porovnanie s near_clipping a far_clipping
			dist = calcDistance(stareX, stareY, stareZ);
			if (dist < Kamera.getClipNear() || dist > Kamera.getClipFar()) {
				continue;
			}

			//suradnice bodov na priemetni
			noveX = stareX - a * p;
			noveY = stareY - b * p;
			noveZ = stareZ - c * p;

			//vypocet linearnej kombinacie pre ziskanie suradnic z 3D v 2D
			//pre vypocet s zadavam podmienky, aby nenastali problemy v zobrazeniach rovnobeznych s osami
			t = (u.x() * noveY - u.y() * noveX) / (v.y() * u.x() - v.x() * u.y());
			if (Kamera.getZenit() == M_PI_2) {
				s = (noveZ - t * v.z()) / u.z();
			}
			else if(Kamera.getAzimut() == M_PI_2 || Kamera.getAzimut() == 3 * M_PI_2) {
				s = (noveY - t * v.y()) / u.y();
			}
			else {
				s = (noveX - t * v.x()) / u.x();
			}
			
			Body[2].Sur.setX(width + t * Kamera.getScale() + Kamera.getTransX());		//preskalovanie novych suradnic velkostou objektu (zoom)
			Body[2].Sur.setY(height + s * Kamera.getScale() + Kamera.getTransY());
			Body[2].Z = QVector3D(stareX, stareY, stareZ).distanceToPlane(QVector3D(0.0, 0.0, 0.0), Kamera.get_n());		//metoda na vypocet vzdialenosti od roviny
			try {
				Body[2].color = *staryBod.getColor();
			}
			catch (int c) {}
		}


		Sur3d << Body;			//qvektory bodov si ulozim, aby som ich potom zobrazil
	}
}
double ImageViewer::calcDistance(double x, double y, double z)
{
	QVector3D vrchol(x, y, z);
	double scale = Kamera.getScale();

	vrchol.setX(vrchol.x() * scale);
	vrchol.setY(vrchol.y() * scale);
	vrchol.setZ(vrchol.z() * scale);

	QVector3D n = Kamera.get_n() * 1000;

	return (n.distanceToPoint(vrchol) );
}


bool ImageViewer::CheckVtkHeader(QFile* VtkObj)
{
	//kontrola hlavicky
	QString line;

	if ((line = VtkObj->readLine()) != "# vtk DataFile Version 3.0\r\n") {    
		return false;
	}
	if ((line = VtkObj->readLine()) != "vtk output\r\n") {
		return false;
	}
	if ((line = VtkObj->readLine()) != "ASCII\r\n") {
		return false;
	}
	if ((line = VtkObj->readLine()) != "DATASET POLYDATA\r\n") {
		return false;
	}
	return true;
}


//Import .VTK
bool ImageViewer::FillVtkPoints(QFile* VtkObj, QList<Vertex>* Verts)
{
	QString line;
	int nVrcholov;
	int fSur;		//format suradnic, 0 == float, 1 == int
	int i;
	bool ok;
	float surx, sury, surz;

	line = VtkObj->readLine();
	if (line.split(' ').at(0) != "POINTS") {
		return false;
	}

	nVrcholov = line.split(" ").at(1).toInt(&ok, 10);

	if (!ok) return false;		//ak sa zle prepise pocet vrcholov, ukonci funkciu

	//kontrola formatu suradnic
	if (line.split(" ").at(2) == "float\r\n") {
		fSur = 0;
	}
	else if (line.split(" ").at(2) == "int\r\n") {
		fSur = 1;
	}
	else {
		return false;
	}

	Verts->reserve(nVrcholov);
	for (i = 0; i < nVrcholov; i++) {
		line = VtkObj->readLine();
		surx = line.split(" ").at(0).toFloat(&ok);
		sury = line.split(" ").at(1).toFloat(&ok);
		surz = line.split(" ").at(2).toFloat(&ok);
		if (!ok) return false;		//kontrola konverzie
		*Verts << Vertex(i, surx, sury, surz);
	}
	return true;
}
bool ImageViewer::FillVtkEdges(QFile* VtkObj, QList<HalfEdge>* Edges, QList<Vertex>* Verts)
{
	int i;
	QString line;
	int nHran;
	bool ok;
	int vert_origin_index;
	int vert_end_index;
	Vertex* vert_origin;
	Vertex* vert_end;
	HalfEdge* edge_pair_next;
	HalfEdge* edge_pair_prev;

	if ((line = VtkObj->readLine()) == "\r\n") {		//toleruje medzeru, ale nemusi tam byt
		line = VtkObj->readLine();
	}

	if (line.split(" ").at(0) != "LINES") {
		return false;
	}

	nHran = line.split(" ").at(1).toInt(&ok, 10);
	if (!ok) return false;		//ak sa zle prepise pocet vrcholov, ukonci funkciu


	Edges->reserve(nHran * 2);
	for (i = 0; i < nHran * 2; i++) {		//naplnim qlist prazdnymi hranami, aby som vedel pouzivat i+1 -te hrany
		*Edges << HalfEdge();
	}

	for (i = 0; i < nHran; i++) {
		line = VtkObj->readLine();
		vert_origin_index = line.split(" ").at(1).toInt(&ok);
		vert_end_index = line.split(" ").at(2).toInt(&ok);
		if (!ok) return false;		//kontrola konverzie do int


		vert_origin = &(*Verts)[vert_origin_index];
		vert_end = &(*Verts)[vert_end_index];
		edge_pair_next = &(*Edges)[(i * 2) + 1];
		edge_pair_prev = &(*Edges)[i * 2];

		(*Edges)[i*2].setPoints(vert_origin, vert_end, edge_pair_next);
		(*Edges)[(i*2)+1].setPoints(vert_end, vert_origin, edge_pair_prev);
	}
	return true;
}
bool ImageViewer::FillVtkEdgesInfo(QList<Face>* Faces, QList<HalfEdge>* Edges, QList<Vertex>* Verts, QList<QList<int>>* BodyStien)
{
	int i,j,k;
	int ownFace;	//vlastna stena, potrebne si to docasne ulozit
	int ZvysnyBodIndex;

	for (i = 0; i < Edges->size(); i++) {	//cyklus prechadza vsetkymi polhranami
		ZvysnyBodIndex = -2;
		ownFace = -1;

		for (j = 0; j < BodyStien->size() ; j++) {	//kontroluje, ci stenu tvoria jej body	
			
			if ((*Edges)[i].hasFace()) {
				break;
			}
			
			for (k = 0; k < 3; k++) {

				//porovnava dvojice bodov steny: ak sadne zaciatocny aj koncovy bod, ulozi stenu do hrany
				if ((*Edges)[i].getBodIndex() == (*BodyStien)[j][k] && (*Edges)[i].getBodEndIndex() == (*BodyStien)[j][(k + 1) % 3]) 
				{
					(*Edges)[i].setFace(&(*Faces)[j]);
					ownFace = j;
					break;
				}
			}
		}

		//teraz uz hrana ma svoju stenu

		//tymto cyklom zistim pre danu hranu, aky index ma zvysny bod steny/trojuholnika
		while (ZvysnyBodIndex < 0) {
			if ((*BodyStien)[ownFace][-ZvysnyBodIndex] == (*Edges)[i].getBodIndex() || (*BodyStien)[ownFace][-ZvysnyBodIndex] == (*Edges)[i].getBodEndIndex()) {
				ZvysnyBodIndex++;
			}
			else {
				ZvysnyBodIndex *= -1;
			}
		}
		
		for (j = 0; j < Edges->size(); j++) {

				//koncovy bod je zac. bod dalsej hrany			a		koncovy bod dalsej hrany je zvysny bod trojuholnika
			if ((*Edges)[j].getBodIndex() == (*Edges)[i].getBodEndIndex() && (*Edges)[j].getBodEndIndex() == (*BodyStien)[ownFace][ZvysnyBodIndex]) {
				//vtedy je hrana [j] nasledujucou hranou
				(*Edges)[i].setEdgeNext(&(*Edges)[j]);
			}

			if ((*Edges)[j].getBodIndex() == (*BodyStien)[ownFace][ZvysnyBodIndex] && (*Edges)[j].getBodEndIndex() == (*Edges)[i].getBodIndex()) {
				//vtedy je hrana [j] predchadzajucou hranou
				(*Edges)[i].setEdgePrev(&(*Edges)[j]);
			}
		}
	}
	


	return true;
}
bool ImageViewer::FillVtkFaces(QFile* VtkObj, QList<Face>* Faces, QList<HalfEdge>* Edges, QList<Vertex>* Verts)
{
	QString line;
	int i,j;
	int nStien;
	bool ok;
	int id0, id1, id2;
	QList<QList<int>> BodyStien;

	if ((line = VtkObj->readLine()) == "\r\n") {		//toleruje medzeru, ale nemusi tam byt
		line = VtkObj->readLine();
	}

	if (line.split(" ").at(0) != "POLYGONS") {
		return false;
	}

	nStien = line.split(" ").at(1).toInt(&ok, 10);
	if (!ok) return false;		//ak sa zle prepise pocet stien, ukonci funkciu


	Faces->reserve(nStien);
	for (i = 0; i < nStien; i++) {		//naplnil list prazdnymi stenami
		*Faces << Face();
	}
	BodyStien.reserve(nStien);		//pripravim si zapis vrcholov stien

	for (i = 0; i < nStien; i++) {		
		line = VtkObj->readLine();
		id0 = line.split(" ").at(1).toInt(&ok);
		id1 = line.split(" ").at(2).toInt(&ok);
		id2 = line.split(" ").at(3).toInt(&ok);
		if (!ok) return false;				//kontrola konverzie do int

		BodyStien << QList<int>{id0, id1, id2};

		for (j = 0; j < Edges->size(); j++) {		//cyklus prechadza vsetkymi ulozenymi hranami
			if (id0 == (*Edges)[j].getBodIndex() && id1 == (*Edges)[j].getBodEndIndex()) {		//porovnava body steny: ak sadne zaciatocny aj koncovy bod, ulozi hranu do steny
				(*Faces)[i].setEdge(&(*Edges)[j]);
				break;
			}
		}
	}

	ok = FillVtkEdgesInfo(Faces, Edges, Verts, &BodyStien);
	if (!ok) return false;

	return true;
}

//Import .DAT
bool ImageViewer::FillDatPoints(QFile* ObjectFile, QList<Vertex>* Verts) {

	QByteArray line;
	QVector<double> sur(3);
	bool ok;
	int i = 0;

	while (!ObjectFile->atEnd()) {
		line = ObjectFile->readLine();

		sur[0] = line.split(' ').at(0).toDouble(&ok);
		if (!ok)	return false;

		sur[1] = line.split(' ').at(1).toDouble(&ok);
		if (!ok) 	return false;

		sur[2] = line.split(' ').at(2).toDouble(&ok);
		if (!ok) 	return false;

		*Verts << Vertex(i, sur[0], sur[1], sur[2]);
		i++;
	}
	return true;
}
bool ImageViewer::FillDatEdges(QList<Vertex>* Verts, QList<HalfEdge>* Edges, QList<Face>* Faces)
{
	int j = 0;
	int i;
	int locVert;	//location of vertex
	int locEdge;
	int locPoly;
	

	int nStlpcov = 0;
	int nRiadkov = INT_MAX;
	int nHran;
	int nPlosok;

	Vertex* vert_origin;
	Vertex* vert_end;
	HalfEdge* edge_pair_next;
	HalfEdge* edge_pair_prev;

	//###### VYPLNENIE OBJEKTOV ######

	while (j < nRiadkov - 1) {

		//najprv vytvaram iba vrchne trojuholniky riadku, az kym nenarazim na prechod na dalsi riadok
		i = 0;
		
		while ((*Verts)[j * nStlpcov + i].getY() == (*Verts)[j * nStlpcov + i + 1].getY()) {

			locVert = j * nStlpcov + i;
			locEdge = (6 * j * (nStlpcov - 1)) + i * 3;
			locPoly = i + j * (nStlpcov - 1) * 2;

			*Edges << HalfEdge();
			*Edges << HalfEdge();
			*Edges << HalfEdge();
			*Faces << Face();

			vert_origin = &(*Verts)[locVert];
			vert_end = &(*Verts)[locVert + 1];

			(*Edges)[locEdge].setPoints(vert_origin, vert_end);		//iba kazdej tretej viem uz teraz urcit body, vodorovna hrana smerom doprava

			//vsetkym polohranam urcim cele poradie polohran
			(*Edges)[locEdge].setEdgeNext(&(*Edges)[locEdge + 1]);
			(*Edges)[locEdge].setEdgePrev(&(*Edges)[locEdge + 2]);

			(*Edges)[locEdge + 1].setEdgeNext(&(*Edges)[locEdge + 2]);
			(*Edges)[locEdge + 1].setEdgePrev(&(*Edges)[locEdge]);

			(*Edges)[locEdge + 2].setEdgeNext(&(*Edges)[locEdge]);
			(*Edges)[locEdge + 2].setEdgePrev(&(*Edges)[locEdge + 1]);

			(*Faces)[locPoly].setEdge(&(*Edges)[locEdge]);		//ploske nastavim jej polohranu

			i++;
		}


		//akonahle skonci cyklus, dopocitam vsetko ostatne
		
		if (nRiadkov == INT_MAX) {
			//pri prvom skoku ulozim pocet stlpcov a vypocitam pocet riadkov

			nStlpcov = i + 1;
			nRiadkov = Verts->size() / nStlpcov;
			nPlosok = nStlpcov - 1;		//pocet plosok v kazdom riadku (iba jedna polovica z nich, napr. horne)
		}

		//uz vytvorenym polohranam dopocitam ich vrcholy
		for (i = 0; i < nStlpcov - 1; i++) {

			locVert = j * nStlpcov + i;
			locEdge = (6 * j * (nStlpcov - 1)) + i * 3;

			vert_origin = &(*Verts)[locVert + 1];					//vrchol vpravo hore
			vert_end = &(*Verts)[locVert + nStlpcov];				//vrchol vlavo dole
			(*Edges)[locEdge + 1].setPoints(vert_origin, vert_end);		//diagonalna hrana tvaru: "/" , smerom dolu

			vert_origin = &(*Verts)[locVert + nStlpcov];			//vrchol vlavo dole
			vert_end = &(*Verts)[locVert];						//vrchol vlavo hore
			(*Edges)[locEdge + 2].setPoints(vert_origin, vert_end);		//zvisla hrana smerom hore
		}


		//vytvorim si spodne plosky riadku
		for (i = 0; i < nPlosok; i++) {
			*Faces << Face();
		}

		//spodnym ploskam vypocitam hrany
		for (i = 0; i < nStlpcov - 1;i++) {

			locVert = j * nStlpcov + i;
			locEdge = (6 * j * (nStlpcov - 1)) + i * 3 + (nStlpcov - 1) * 3;
			locPoly =  2 * j * (nStlpcov - 1) + (nStlpcov - 1) + i;

			for (int k = 0; k < 3; k++) {
				*Edges << HalfEdge();
			}

			//novym polohranam nastavim zaciatocne a konecne vrcholy
			vert_origin = &(*Verts)[locVert + nStlpcov + 1];		//vpravo dole
			vert_end = &(*Verts)[locVert + nStlpcov];				//vlavo dole

			(*Edges)[locEdge].setPoints(vert_origin, vert_end);		//vodorovna hrana smerom dolava

			vert_origin = &(*Verts)[locVert + nStlpcov];		//vlavo dole
			vert_end = &(*Verts)[locVert + 1];				//vpravo hore

			(*Edges)[locEdge + 1].setPoints(vert_origin, vert_end);		//diagonalna hrana smerom doprava

			vert_origin = &(*Verts)[locVert + 1];		//vpravo hore
			vert_end = &(*Verts)[locVert + nStlpcov + 1];				//vpravo dole

			(*Edges)[locEdge + 2].setPoints(vert_origin, vert_end);		//diagonalna hrana smerom doprava


			//novym polohranam urcim cele poradie polohran
			(*Edges)[locEdge].setEdgeNext(&(*Edges)[locEdge + 1]);
			(*Edges)[locEdge].setEdgePrev(&(*Edges)[locEdge + 2]);

			(*Edges)[locEdge + 1].setEdgeNext(&(*Edges)[locEdge + 2]);
			(*Edges)[locEdge + 1].setEdgePrev(&(*Edges)[locEdge]);

			(*Edges)[locEdge + 2].setEdgeNext(&(*Edges)[locEdge]);
			(*Edges)[locEdge + 2].setEdgePrev(&(*Edges)[locEdge + 1]);

			(*Faces)[locPoly].setEdge(&(*Edges)[locEdge]);		//ploske nastavim jej polohranu
		}

		j++;
	}
	

	//##### PRIRADENIE PAROVYCH HRAN ######

	for (i = 0; i < nRiadkov - 1; i++) {			// i = kroky po riadkoch
		for (j = 0; j < nStlpcov - 1; j++) {		// j = kroky po stlpcoch
			int HORIZ_vrchna_id, HORIZ_spodna_id, VERT_lava_id, VERT_prava_id, DIAG_vrchna_id, DIAG_spodna_id;

			if (i >= 1) {
				HORIZ_vrchna_id = i * 6 * (nStlpcov - 1) + j * 3;								//horizontalna hrana nasmerovana doprava
				HORIZ_spodna_id = i * 6 * (nStlpcov - 1) + j * 3 - ((nStlpcov * 3) - 3);		//horizontalna hrana nasmerovana dolava

				(*Edges)[HORIZ_vrchna_id].setPair(&(*Edges)[HORIZ_spodna_id]);
				(*Edges)[HORIZ_spodna_id].setPair(&(*Edges)[HORIZ_vrchna_id]);
			}

			if (j >= 1) {
				VERT_lava_id = 2 + i * 6 * (nStlpcov - 1) + j * 3;								//vertikalna hrana nasmerovana hore
				VERT_prava_id = (nStlpcov * 3) - 1 + (6 * i * (nStlpcov - 1)) + (j - 1) * 3;	//vertikalna hrana nasmerovana dolu

				(*Edges)[VERT_lava_id].setPair(&(*Edges)[VERT_prava_id]);
				(*Edges)[VERT_prava_id].setPair(&(*Edges)[VERT_lava_id]);
			}

			DIAG_vrchna_id = 1 + i * 6 * (nStlpcov - 1) + j * 3;								// diagonalna hrana nasmerovana dolu-dolava
			DIAG_spodna_id = (nStlpcov * 3) - 2 + j * 3 + (6 * i * (nStlpcov - 1));				// diagonalna hrana nasmerovana hore-doprava

			(*Edges)[DIAG_vrchna_id].setPair(&(*Edges)[DIAG_spodna_id]);
			(*Edges)[DIAG_spodna_id].setPair(&(*Edges)[DIAG_vrchna_id]);

		}
	}
	return true;
}

bool ImageViewer::RescaleDatPoints(QList<Vertex>* Verts)
{
	double Xmin = (*Verts)[0].getX();
	double Ymin = (*Verts)[0].getY();
	double Xmax = (*Verts)[Verts->size() - 1].getX();
	double Ymax = (*Verts)[Verts->size() - 1].getY();
	double Zmin = DBL_MAX;
	double Zmax = DBL_MIN;

	double Xposun, Yposun, Zposun;
	double Xscale, Yscale, Zscale;


	//cyklus pre najdenie Zmin a Zmax
	for (int i = 0; i < Verts->size(); i++) {
		if ((*Verts)[i].getZ() < Zmin) Zmin = (*Verts)[i].getZ();
		if ((*Verts)[i].getZ() > Zmax) Zmax = (*Verts)[i].getZ();
	}

	//vypocet posunu v osi X,Y,Z
	Xposun = -(Xmin + ((Xmax - Xmin) / 2.0));
	Yposun = -(Ymin + ((Ymax - Ymin) / 2.0));
	Zposun = -(Zmin + ((Zmax - Zmin) / 2.0));

	//vypocet faktora skalovania v osi X,Y,Z
	Xscale = (1.0 / (Xmax + Xposun)) * 5;
	Yscale = (1.0 / (Ymax + Yposun)) * 5;
	Zscale = 1.0 / (Zmax + Zposun);

	//prepocitanie vsetkych suradnic
	for (int i = 0; i < Verts->size(); i++) {
		double tempX = (*Verts)[i].getX();

		(*Verts)[i].setX(((*Verts)[i].getY() + Yposun) * (-Yscale));
		(*Verts)[i].setY((tempX + Xposun) * (-Xscale));
		(*Verts)[i].setZ(((*Verts)[i].getZ() + Zposun) * Zscale);
	}

	return true;
}



//Slots

//Tabs slots
void ImageViewer::on_tabWidget_tabCloseRequested(int tabId)
{
	ViewerWidget* vW = getViewerWidget(tabId);
	delete vW; //vW->~ViewerWidget();
	ui->tabWidget->removeTab(tabId);

	if (tabId == 0) {
		ui->groupBox->setEnabled(false);
	}
}
void ImageViewer::on_actionRename_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ViewerWidget* w = getCurrentViewerWidget();
	bool ok;
	QString text = QInputDialog::getText(this, QString("Rename image"), tr("Image name:"), QLineEdit::Normal, w->getName(), &ok);
	if (ok & !text.trimmed().isEmpty())
	{
		w->setName(text);
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), text);
	}
}

//Image slots
void ImageViewer::on_actionNew_triggered()
{
	newImgDialog = new NewImageDialog(this);
	connect(newImgDialog, SIGNAL(accepted()), this, SLOT(newImageAccepted()));
	newImgDialog->exec();
}
void ImageViewer::newImageAccepted()
{
	NewImageDialog* newImgDialog = static_cast<NewImageDialog*>(sender());

	int width = newImgDialog->getWidth();
	int height = newImgDialog->getHeight();
	QString name = newImgDialog->getName();
	openNewTabForImg(new ViewerWidget(name, QSize(width, height)));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);
	ui->groupBox->setEnabled(true);

	if (ui->Vyber->currentIndex() == 0) {
		getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);

	}
	else if (ui->Vyber->currentIndex() == 1) {
		getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg, &LineAlg);
	}
}
void ImageViewer::on_actionOpen_triggered()
{
	QString folder = settings.value("folder_img_load_path", "").toString();

	QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
	QString fileName = QFileDialog::getOpenFileName(this, "Load image", folder, fileFilter);
	if (fileName.isEmpty()) { return; }

	QFileInfo fi(fileName);
	settings.setValue("folder_img_load_path", fi.absoluteDir().absolutePath());

	if (!openImage(fileName)) {
		msgBox.setText("Unable to open image.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.exec();
	}
}
void ImageViewer::on_actionSave_as_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image to save.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	QString folder = settings.value("folder_img_save_path", "").toString();

	ViewerWidget* w = getCurrentViewerWidget();

	QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
	QString fileName = QFileDialog::getSaveFileName(this, "Save image", folder + "/" + w->getName(), fileFilter);
	if (fileName.isEmpty()) { return; }

	QFileInfo fi(fileName);
	settings.setValue("folder_img_save_path", fi.absoluteDir().absolutePath());

	if (!saveImage(fileName)) {
		msgBox.setText("Unable to save image.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.exec();
	}
	else {
		msgBox.setText(QString("File %1 saved.").arg(fileName));
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
	}
}
void ImageViewer::on_actionClear_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	clearImage();
}
void ImageViewer::on_actionSet_background_color_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}

	QColor backgroundColor = QColorDialog::getColor(Qt::white, this, "Select color of background");
	if (backgroundColor.isValid()) {
		setBackgroundColor(backgroundColor);
	}
}

//Color
void ImageViewer::on_Farba_pressed()
{
	QColor selectedColor = QColorDialog::getColor(Qt::white, this, "Select color of background");
	if (selectedColor.isValid()) {
		QString style = "background-color: rgb(%1, %2, %3);";
		ui->Farba->setStyleSheet(style.arg(selectedColor.red()).arg(selectedColor.green()).arg(selectedColor.blue()));
		PainterColor = selectedColor;
		
		if (ui->Vyber->currentIndex() == 0) {
			getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);

		}
		else if (ui->Vyber->currentIndex() == 1) {
			getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg, &LineAlg);
		}
	}
}
void ImageViewer::on_Farba_vypln_pressed()
{
	QColor selectedColor = QColorDialog::getColor(Qt::white, this, "Select filling color");
	if (selectedColor.isValid()) {
		QString style = "background-color: rgb(%1, %2, %3);";
		ui->Farba_vypln->setStyleSheet(style.arg(selectedColor.red()).arg(selectedColor.green()).arg(selectedColor.blue()));
		FillColor = selectedColor;

		getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);
	}
}

void ImageViewer::on_Vypln_currentIndexChanged(int)
{
	//Vykresli
	getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);
	
	//De/Aktivacia nastavenia farby
	if (ui->Vypln->currentIndex() == 0 || Suradnice.size() <= 3) {
		ui->label_3->setEnabled(false);
		ui->Farba_vypln->setEnabled(false);
	}
	else {
		ui->label_3->setEnabled(true);
		ui->Farba_vypln->setEnabled(true);
	}
}
void ImageViewer::on_Vyber_currentIndexChanged(int)
{
	//POLYGON
	if (ui->Vyber->currentIndex() == 0) {
		ui->GroupKrivka->setVisible(false);
		ui->GroupHermit->setVisible(false);

		ui->groupBox_2->setVisible(true);
		ui->Vyplnenie->setVisible(true);
		ui->GroupTrans->setVisible(true);

		ui->ButtonNovy->setVisible(true);

		ui->GroupKamera->setVisible(false);
		ui->GroupIcosfera->setVisible(false);
		ui->GroupTrans3D->setVisible(false);
		ui->GroupRender->setVisible(false);
	}
	//KRIVKA
	else if (ui->Vyber->currentIndex() == 1) {
		ui->GroupKrivka->setVisible(true);
		ui->GroupKrivka->setEnabled(true);
		ui->RadioHermit->setChecked(true);

		ui->groupBox_2->setVisible(true);
		ui->Vyplnenie->setVisible(true);
		ui->GroupTrans->setVisible(true);

		ui->ButtonNovy->setVisible(true);


		if (ui->RadioHermit->isChecked()) {		//viditelnost nastaveni hermitovskej krivky
			ui->GroupHermit->setVisible(true);
			ui->GroupHermit->setEnabled(false);
		}
		else {
			ui->GroupHermit->setVisible(false);
		}

		ui->GroupKamera->setVisible(false);
		ui->GroupIcosfera->setVisible(false);
		ui->GroupTrans3D->setVisible(false);
		ui->GroupRender->setVisible(false);
	}

	//SFERA
	else if (ui->Vyber->currentIndex() == 2) {
		ui->GroupKrivka->setVisible(false);
		ui->GroupHermit->setVisible(false);
		ui->groupBox_2->setVisible(false);
		ui->Vyplnenie->setVisible(false);
		ui->GroupTrans->setVisible(false);
		ui->Batymetria->setVisible(false);

		ui->ButtonNovy->setVisible(false);

		ui->GroupKamera->setVisible(true);
		ui->GroupIcosfera->setVisible(true);
		ui->GroupTrans3D->setVisible(true);


		//kvoli mape
		ui->GroupPointLight->setVisible(true);
		ui->GroupAmbient->setVisible(true);
		ui->GroupMaterial->setVisible(true);
		ui->GroupShading->setVisible(true);
		ui->GroupIcosfera->setVisible(true);
		ui->GroupCameraSettings->setVisible(true);

		ui->label_15->setVisible(true);
		ui->stred_prem->setVisible(true);
		ui->SliderStredPrem->setVisible(true);

		ui->GroupRender->setVisible(true);

		ui->label_9->setText("Zenit [°]");
		ui->label_10->setText("Azimut [°]");

		Refresh3D();
	}
	//MAPA
	else if (ui->Vyber->currentIndex() == 3) {

		ui->GroupKrivka->setVisible(false);
		ui->GroupHermit->setVisible(false);
		ui->groupBox_2->setVisible(false);
		ui->Vyplnenie->setVisible(false);
		ui->GroupTrans->setVisible(false);
		ui->ButtonNovy->setVisible(false);

		ui->GroupKamera->setVisible(true);
		ui->GroupIcosfera->setVisible(true);
		ui->GroupTrans3D->setVisible(true);
		ui->Batymetria->setVisible(true);

		ui->GroupPointLight->setVisible(false);
		ui->GroupAmbient->setVisible(false);
		ui->GroupMaterial->setVisible(false);
		ui->GroupIcosfera->setVisible(false);
		//ui->GroupCameraSettings->setVisible(false);

		ui->label_15->setVisible(false);
		ui->stred_prem->setVisible(false);
		ui->SliderStredPrem->setVisible(false);
		ui->GroupRender->setVisible(true);


		//spravny popis rotacii
		ui->label_9->setText("Rotacia osi X[°]");
		ui->label_10->setText("Rotacia osi Z[°]");
		
		//nastavenie farby svetla pre mapu
		Kamera.setAmbientColor(QColor(255, 255, 255));
		Kamera.setPointColor(QColor(255, 255, 255));
		ui->PointColor->setStyleSheet("background-color: rgb(255, 255, 255);");
		ui->AmbColor->setStyleSheet("background-color: rgb(255, 255, 255);");
		
		
		ui->SliderScale3D->blockSignals(true);
		ui->SliderScale3D->setValue(45);
		Kamera.setScale(45 / 100.0);
		ui->SliderScale3D->blockSignals(false);
		

		//resetovanie polohy kamery
		ui->zenit->blockSignals(true);
		ui->zenit->setValue(20);
		Kamera.setZenit(20.0 / 180.0 * M_PI);
		ui->zenit->blockSignals(false);


		//nastavenie polohy bodoveho svetla
		ui->DialSvetlo->blockSignals(true);
		ui->DialSvetlo->setValue(120);
		ui->DialSvetlo->blockSignals(false);

		double uhol = (120.0 * M_PI) / 180.0;
		double stareX = 3.0;
		double stareY = 0.0;
		Kamera.setPointSurX(stareX* cos(uhol) - stareY * sin(uhol));
		Kamera.setPointSurY(stareY* cos(uhol) + stareX * sin(uhol));

		Refresh3D();
	}
}

//CurveAlg Change
void ImageViewer::on_RadioHermit_pressed()
{
	ui->GroupHermit->setVisible(true);
	CurveAlg = 0;
}
void ImageViewer::on_RadioBezier_pressed()
{
	ui->GroupHermit->setVisible(false);
	CurveAlg = 1;
}
void ImageViewer::on_RadioBspline_pressed()
{
	ui->GroupHermit->setVisible(false);
	CurveAlg = 2;
}


//LineAlg Change
void ImageViewer::on_DDA_pressed()
{
	LineAlg = 1;
}
void ImageViewer::on_Bresenham_pressed()
{
	LineAlg = 0;
}

//UI Buttons
void ImageViewer::on_ButtonNovy_pressed()
{
	Suradnice.clear();		//vymaze data z vektora
	Suradnice.squeeze();	//uvolni pamat
	SurCursor.clear();
	SurCursor.squeeze();

	//Upravi vyber vyplne
	ui->Vypln->removeItem(1);
	ui->Vypln->removeItem(1);
	ui->label_3->setEnabled(false);
	ui->Farba_vypln->setEnabled(false);

	ui->Vyplnenie->setEnabled(false);
	ui->ButtonNovy->setEnabled(false);
	ui->GroupTrans->setEnabled(false);
	getCurrentViewerWidget()->clear();

	//Vyber objektu + krivky
	ui->label_2->setEnabled(true);
	ui->Vyber->setEnabled(true);
	ui->GroupKrivka->setEnabled(true);
	ui->GroupHermit->setEnabled(false);
}		
//###################

void ImageViewer::on_ButtonRot_pressed()
{
	//rot
	if (remainder(ui->SpinRot->value(),360.0) != 0.0) {		//testuje, ci sa vobec ma otacat, ci uhol rotovania nie je nasobkom 360
		int i;
		double RotAngle = ui->SpinRot->value() / 180.0 * M_PI;
		double BaseX = Suradnice[0].x();
		double BaseY = Suradnice[0].y();

		getCurrentViewerWidget()->clear();

		for (i = 1; i < Suradnice.size(); i++) {
			int CurrX = Suradnice[i].x();
			int CurrY = Suradnice[i].y();
			Suradnice[i].setX((CurrX - BaseX) * qCos(RotAngle) + (CurrY - BaseY) * qSin(RotAngle) + BaseX);
			Suradnice[i].setY(-(CurrX - BaseX) * qSin(RotAngle) + (CurrY - BaseY) * qCos(RotAngle) + BaseY);
		}

		if (ui->Vyber->currentIndex() == 0) {
			getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);
		}
		else if (ui->Vyber->currentIndex() == 1) {
			getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg, &LineAlg);
		}
	}
}
void ImageViewer::on_ButtonScale_pressed()
{
	//scale
	int i;
	double ScaleValue = ui->SpinScale->value();
	double BaseX = Suradnice[0].x();
	double BaseY = Suradnice[0].y();

	getCurrentViewerWidget()->clear();

	for (i = 1; i < Suradnice.size(); i++) {
		Suradnice[i].setX(BaseX + ((Suradnice[i].x() - BaseX) * ScaleValue));
		Suradnice[i].setY(BaseY + ((Suradnice[i].y() - BaseY) * ScaleValue));
	}

	//adaptívne skaluje aj dlzku vektorov
	if (!HVektory.isEmpty()) {
		for (i = 0; i < HVektory.size(); i++) {
			HVektory[i].Dlzka = HVektory[i].Dlzka * ScaleValue;
		}
		ui->SpinCurveLength->setValue(HVektory[ui->SpinPoint->value()].Dlzka);	//upravi hodnotu v spinboxe

		if (HVektory[ui->SpinPoint->value()].Dlzka <= ui->SliderCurveLength->maximum()) {
			//ak je hodnota mensia ako maximalna hodnota slidera, nastavi tam spravnu hodnotu
			ui->SliderCurveLength->setValue(HVektory[ui->SpinPoint->value()].Dlzka);
		}
		else {
			//ak je hodnota vacsia, nastavi tam maximalnu moznu hodnotu
			ui->SliderCurveLength->setValue(ui->SliderCurveLength->maximum());
		}
	}

	if (ui->Vyber->currentIndex() == 0) {
		getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor, &LineAlg, ui->Vypln->currentIndex(), &FillColor);

	}
	else if (ui->Vyber->currentIndex() == 1) {
		getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg, &LineAlg);
	}
} 
void ImageViewer::on_ButtonShear_pressed()
{
	//shear
	int i;
	double ShearValue = ui->SpinShear->value() / 10.0;	//delim 10, aby sa s tym narabalo pohodlnejsie

	for (i = 1; i < Suradnice.size(); i++) {
		Suradnice[i].setX(Suradnice[i].x() + ShearValue * (Suradnice[i].y() - Suradnice[0].y()));
	}

	getCurrentViewerWidget()->clear();

	if (ui->Vyber->currentIndex() == 0) {
		getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor,&LineAlg, ui->Vypln->currentIndex(), &FillColor);

	}
	else if (ui->Vyber->currentIndex() == 1) {
		getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg,&LineAlg);
	}
}
void ImageViewer::on_ButtonSymm_pressed()
{
	//symmetry
	if (Suradnice.size() > 2) {		//polygon
		int i;
		double u = Suradnice[1].x() - Suradnice[0].x();
		double v = Suradnice[1].y() - Suradnice[0].y();

		double a = v;
		double b = -u;
		double c = -a * Suradnice[0].x() - b * Suradnice[0].y();

		for (i = 2; i < Suradnice.size(); i++) {
			double BaseX = Suradnice[i].x();
			double BaseY = Suradnice[i].y();
			Suradnice[i].setX(BaseX - 2 * a * (a * BaseX + b * BaseY + c) / (a * a + b * b));
			Suradnice[i].setY(BaseY - 2 * b * (a * BaseX + b * BaseY + c) / (a * a + b * b));
		}

		getCurrentViewerWidget()->clear();

		if (ui->Vyber->currentIndex() == 0) {
			getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor,&LineAlg, ui->Vypln->currentIndex(), &FillColor);

		}
		else if (ui->Vyber->currentIndex() == 1) {
			getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg,&LineAlg);
		}
	}
	else {		//usecka
		Suradnice[1].setY(Suradnice[0].y() - (Suradnice[1].y() - Suradnice[0].y()));		//symetria cez os x
		getCurrentViewerWidget()->clear();
		
		if (ui->Vyber->currentIndex() == 0) {
			getCurrentViewerWidget()->Generuj(&Suradnice, &PainterColor,&LineAlg, ui->Vypln->currentIndex(), &FillColor);

		}
		else if (ui->Vyber->currentIndex() == 1) {
			getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg,&LineAlg);
		}
	}
}

void ImageViewer::on_VtkExport_pressed()
{
	QMessageBox msgBox;
	if (!GenObjekt->isNull()) {
		if (ExportObject(GenObjekt)) {
			msgBox.setText("Objekt bol úspešne exportovaný.");
		}
		else {
			msgBox.setText("Objekt sa nepodarilo exportovať.");
		}	
	}
	else {
		msgBox.setText("Objekt ešte nebol vygenerovaný");
	}

	msgBox.exec();
}
void ImageViewer::on_VtkImport_pressed()
{
	ImpObjekt = new GeomObjekt();				//importovany objekt
	QMessageBox msgBox;

	if (ImportObject(ImpObjekt)) {
		msgBox.setText("Objekt bol úspešne importovaný.");
	}
	else {
		msgBox.setText("Objekt sa nepodarilo importovať.");
	}
	msgBox.exec();
	ui->radio_Imp->setChecked(true);
	ui->GroupMaterial->setEnabled(true);
	Refresh3D();
}
void ImageViewer::on_GenIcos_pressed()
{
	GenObjekt = new GeomObjekt();				//generovany objekt
	GenerateIcosahedron(GenObjekt, ui->SpinIcos->value());
	ui->radio_Gen->setChecked(true);

	ui->GroupMaterial->setEnabled(true);
	Refresh3D();
}


//UI Nastavenia
void ImageViewer::on_SpinRot_valueChanged(double)
{
	ui->SliderRot->setValue(round(ui->SpinRot->value()));
}
void ImageViewer::on_SliderRot_valueChanged(int)
{
	ui->SpinRot->setValue(ui->SliderRot->value());
}

void ImageViewer::on_SpinScale_valueChanged(double)
{
	ui->SliderScale->setValue(round(ui->SpinScale->value()));
}
void ImageViewer::on_SliderScale_valueChanged(int)
{
	ui->SpinScale->setValue(ui->SliderScale->value());
}

void ImageViewer::on_SpinShear_valueChanged(double)
{
	ui->SliderShear->setValue(round(ui->SpinShear->value()));
}
void ImageViewer::on_SliderShear_valueChanged(int)
{
	ui->SpinShear->setValue(ui->SliderShear->value());
}

void ImageViewer::on_SpinCurveRot_valueChanged(double)
{
	int id = ui->SpinPoint->value();
	ui->SliderCurveRot->setValue(ui->SpinCurveRot->value());
	HVektory[id].Rot = ui->SpinCurveRot->value();

	getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg,&LineAlg);
}
void ImageViewer::on_SliderCurveRot_valueChanged(int)
{
	int id = ui->SpinPoint->value();
	ui->SpinCurveRot->setValue(ui->SliderCurveRot->value());

	HVektory[id].Rot = ui->SliderCurveRot->value();

	getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg,&LineAlg);
}

void ImageViewer::on_SpinCurveLength_valueChanged(double)
{
	int id = ui->SpinPoint->value();
	ui->SliderCurveLength->setValue(ui->SpinCurveLength->value());

	HVektory[id].Dlzka = ui->SpinCurveLength->value();

	getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg,&LineAlg);
}
void ImageViewer::on_SliderCurveLength_valueChanged(int)
{
	int id = ui->SpinPoint->value();
	ui->SpinCurveLength->setValue(ui->SliderCurveLength->value());

	HVektory[id].Dlzka = ui->SliderCurveLength->value();

	getCurrentViewerWidget()->GenerujKrivku(&Suradnice, &HVektory, &PainterColor, &CurveAlg,&LineAlg);
}

void ImageViewer::on_SpinPoint_valueChanged(int)
{
	int id = ui->SpinPoint->value();
	ui->SpinCurveRot->setValue(HVektory[id].Rot);
	ui->SliderCurveRot->setValue(HVektory[id].Rot);
	ui->SpinCurveLength->setValue(HVektory[id].Dlzka);
	ui->SliderCurveLength->setValue(HVektory[id].Dlzka);
}

void ImageViewer::on_zenit_valueChanged(double)
{
	ui->SliderZenit->setValue(round(ui->zenit->value()));
	Kamera.setZenit(ui->zenit->value() / 180.0 * M_PI);
	Refresh3D();
}
void ImageViewer::on_SliderZenit_valueChanged(int)
{
	ui->zenit->setValue(ui->SliderZenit->value());
}

void ImageViewer::on_azimut_valueChanged(double)
{
	ui->SliderAzimut->setValue(round(ui->azimut->value()));
	Kamera.setAzimut(ui->azimut->value() / 180.0 * M_PI);
	Refresh3D();
}
void ImageViewer::on_SliderAzimut_valueChanged(int)
{
	ui->azimut->setValue(ui->SliderAzimut->value());
}

void ImageViewer::on_prem_rovn_pressed()
{
	Kamera.setRezim(0);
	Refresh3D();
}
void ImageViewer::on_prem_stred_pressed()
{
	Kamera.setRezim(1);
	Refresh3D();
}

void ImageViewer::on_clip_near_valueChanged(double)
{
	ui->SliderClipNear->setValue(ui->clip_near->value());
	Kamera.setClipNear(ui->clip_near->value());
	Refresh3D();
}
void ImageViewer::on_SliderClipNear_valueChanged(int)
{
	ui->clip_near->setValue(ui->SliderClipNear->value());
}

void ImageViewer::on_clip_far_valueChanged(double)
{
	ui->SliderClipFar->setValue(ui->clip_far->value());
	Kamera.setClipFar(ui->clip_far->value());
	Refresh3D();
}
void ImageViewer::on_SliderClipFar_valueChanged(int)
{
	ui->clip_far->setValue(ui->SliderClipFar->value());

}

void ImageViewer::on_stred_prem_valueChanged(double)
{
	ui->SliderStredPrem->setValue(ui->stred_prem->value());
	Kamera.setStred(ui->stred_prem->value());
	Refresh3D();
}

void ImageViewer::on_SliderStredPrem_valueChanged(int)
{
	ui->stred_prem->setValue(ui->SliderStredPrem->value());
	Kamera.setStred(ui->stred_prem->value());
	Refresh3D();
}

void ImageViewer::on_transX_valueChanged(int)
{
	Kamera.setTransX(ui->transX->value());
	Refresh3D();
}
void ImageViewer::on_transY_valueChanged(int)
{
	Kamera.setTransY(-(ui->transY->value()));
	Refresh3D();
}

void ImageViewer::on_SliderScale3D_valueChanged(int)
{
	Kamera.setScale(ui->SliderScale3D->value() / 100.0);
	Refresh3D();
}

void ImageViewer::on_radio_Gen_pressed()
{
	ui->radio_Gen->setChecked(true);
	if (GenObjekt != nullptr) {
		ui->GroupMaterial->setEnabled(true);
	}
	else {
		ui->GroupMaterial->setEnabled(false);
	}


	Refresh3D();
}
void ImageViewer::on_radio_Imp_pressed()
{
	ui->radio_Imp->setChecked(true);

	if (ImpObjekt != nullptr) {
		ui->GroupMaterial->setEnabled(true);
	}
	else {
		ui->GroupMaterial->setEnabled(false);
	}

	Refresh3D();
}

void ImageViewer::on_radioRendered_pressed()
{
	Kamera.setRenderMode(0);
	Refresh3D();
}
void ImageViewer::on_radioWireframe_pressed()
{
	Kamera.setRenderMode(1);
	Refresh3D();
}


void ImageViewer::on_radioGouraud_pressed()
{
	Kamera.setShadingMode(0);
	Refresh3D();
}
void ImageViewer::on_radioKonst_pressed()
{
	Kamera.setShadingMode(1);
	Refresh3D();
}

void ImageViewer::on_PointX_valueChanged(double)
{
	Kamera.setPointSurX(ui->PointX->value());
	Refresh3D();
}
void ImageViewer::on_PointY_valueChanged(double)
{
	Kamera.setPointSurY(ui->PointY->value());
	Refresh3D();
}
void ImageViewer::on_PointZ_valueChanged(double)
{
	Kamera.setPointSurZ(ui->PointZ->value());
	Refresh3D();
}

void ImageViewer::on_AmbColor_pressed()
{
	QColor selectedColor = QColorDialog::getColor(Qt::white, this, "Select point");
	if (selectedColor.isValid()) {
		Kamera.setAmbientColor(selectedColor);

		QString style = "background-color: rgb(%1, %2, %3);";
		ui->AmbColor->setStyleSheet(style.arg(selectedColor.red()).arg(selectedColor.green()).arg(selectedColor.blue()));

		Refresh3D();
	}
}
void ImageViewer::on_PointColor_pressed()
{
	QColor selectedColor = QColorDialog::getColor(Qt::white, this, "Select point");
	if (selectedColor.isValid()) {
		Kamera.setPointColor(selectedColor);
		
		QString style = "background-color: rgb(%1, %2, %3);";
		ui->PointColor->setStyleSheet(style.arg(selectedColor.red()).arg(selectedColor.green()).arg(selectedColor.blue()));

		Refresh3D();
	}
}

void ImageViewer::on_spinRoughness_valueChanged(double)
{
	if (GenObjekt != nullptr)	GenObjekt->setRough(ui->spinRoughness->value());
	if (ImpObjekt != nullptr)	ImpObjekt->setRough(ui->spinRoughness->value());

	Refresh3D();
}
void ImageViewer::on_matDiff_pressed()
{
	QColor selectedColor = QColorDialog::getColor(Qt::white, this, "Select point");
	if (selectedColor.isValid()) {
		
		//Cez konkretne podmienky
		
		if (GenObjekt != nullptr)	GenObjekt->setDiff(selectedColor);
		if (ImpObjekt != nullptr)	ImpObjekt->setDiff(selectedColor);

	
		
	
		QString style = "background-color: rgb(%1, %2, %3);";
		ui->matDiff->setStyleSheet(style.arg(selectedColor.red()).arg(selectedColor.green()).arg(selectedColor.blue()));

		Refresh3D();
	}
}
void ImageViewer::on_matGloss_pressed()
{
	QColor selectedColor = QColorDialog::getColor(Qt::white, this, "Select point");
	if (selectedColor.isValid()) {
		if (GenObjekt != nullptr)	GenObjekt->setGloss(selectedColor);
		if (ImpObjekt != nullptr)	ImpObjekt->setGloss(selectedColor);

		QString style = "background-color: rgb(%1, %2, %3);";
		ui->matGloss->setStyleSheet(style.arg(selectedColor.red()).arg(selectedColor.green()).arg(selectedColor.blue()));

		Refresh3D();
	}
}
void ImageViewer::on_matAmb_pressed()
{
	QColor selectedColor = QColorDialog::getColor(Qt::white, this, "Select point");
	if (selectedColor.isValid()) {
		if (GenObjekt != nullptr)	GenObjekt->setAmb(selectedColor);
		if (ImpObjekt != nullptr)	ImpObjekt->setAmb(selectedColor);

		QString style = "background-color: rgb(%1, %2, %3);";
		ui->matAmb->setStyleSheet(style.arg(selectedColor.red()).arg(selectedColor.green()).arg(selectedColor.blue()));

		Refresh3D();
	}
}

void ImageViewer::on_MapaImport_pressed()
{
	ImpObjekt = new GeomObjekt();				//importovany objekt
	QMessageBox msgBox;

	if (ImportObject(ImpObjekt)) {
		msgBox.setText("Batymetrický objekt bol úspešne importovaný.");
	}
	else {
		msgBox.setText("Batymetrický objekt sa nepodarilo importovať.");
	}
	msgBox.exec();


	if (!ImpObjekt->isNull()) {
		Refresh3D();
	}
	else {
		delete ImpObjekt;
	}
	
}

void ImageViewer::on_ZScaleSlider_valueChanged(int)
{
	Kamera.setZScale(ui->ZScaleSlider->value() / 20.0);
	//Kamera.setZScale(1.0 / (150.0 - ui->ZScaleSlider->value()));
	if (ImpObjekt != nullptr) {
		ImpObjekt->CalculateVertexNormals();
	}
	Refresh3D();
}

void ImageViewer::on_DialSvetlo_valueChanged(int)
{
	double uhol = ((double)ui->DialSvetlo->value() * M_PI) / 180.0;

	double stareX = 3.0;
	double stareY = 0.0;

	Kamera.setPointSurX( stareX * cos(uhol) - stareY * sin(uhol));
	Kamera.setPointSurY( stareY * cos(uhol) + stareX * sin(uhol));
	Refresh3D();
}

