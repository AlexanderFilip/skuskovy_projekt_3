#pragma once
#include <QtWidgets>

struct zBufferUnit {
	QColor color;
	QPointF Sur;
	double Z = -DBL_MAX;
};

struct svetlo {
	bool Rezim;		//0 - ambient, 1 - point light
	double X,Y,Z;
	QColor Farba;
};

class Vizualizacia {
	int rezim;		//0 = rovinne, 1 = stredove
	double zenit;
	double azimut;

	double truescale;
	double scale;
	int transX;
	int transY;

	double stred;
	double clipFar;
	double clipNear;

	//bazove vektory
	QVector3D n;
	QVector3D u;
	QVector3D v;

	//svetla
	svetlo Ambient;
	svetlo PointLight;

	//render
	char rezimRender;		//0 - rendered, 1 - wireframe, 2 - both
	char rezimShading;		//0 - Gouraud, 1 - konstante

	//batymetria
	double zScale;

	
public:
	Vizualizacia();

	void setRezim(int r) { rezim = r; };
	void setZenit(double z) { zenit = z; calc_n();  calc_u(); calc_v(); };		//nastavi zenit a prepocita vektory priemetne
	void setAzimut(double a) { azimut = a; calc_n();  calc_u(); calc_v(); };	//nastavi azimut a prepocita vektory priemetne
	void setScale(double s) { scale = s; truescale = s * 200; };		//600 sa mi zda z rozlisenia monitorov ako vhodna hodnota
	void setTransX(int x) { transX = x; };
	void setTransY(int y) { transY = y; };
	void setStred(double s) { stred = s; };
	void setClipFar(double cf) { clipFar = cf; };
	void setClipNear(double cn) { clipNear = cn; };

	int getRezim() { return rezim; };
	double getZenit() { return zenit; };
	double getAzimut() { return azimut; };
	double getScale() { return truescale; };
	int getTransX() { return transX; };
	int getTransY() { return transY; };
	double getStred() { return stred; };
	double getClipFar() { return clipFar; };
	double getClipNear() { return clipNear; };

	void calc_n();
	void calc_u();
	void calc_v();

	QVector3D get_u() { return u; };
	QVector3D get_v() { return v; };
	QVector3D get_n() { return n; };

	double get_n_x() { return n.x(); };
	double get_n_y() { return n.y(); };
	double get_n_z() { return n.z(); };

	//svetla

	void setAmbientColor(QColor c) { Ambient.Farba = c; };
	QColor* getAmbientColor() { return &Ambient.Farba; };

	void setPointSurX(double x) { PointLight.X = x; };
	void setPointSurY(double y) { PointLight.Y = y; };
	void setPointSurZ(double z) { PointLight.Z = z; };
	void setPointColor(QColor c) { PointLight.Farba = c; };

	double getPointSurX() { return PointLight.X; };
	double getPointSurY() { return PointLight.Y; };
	double getPointSurZ() { return PointLight.Z; };
	QColor* getPointColor() { return &PointLight.Farba; };

	//render

	void setRenderMode(char m) { rezimRender = m; };
	void setShadingMode(char m) { rezimShading = m; };

	char getRenderMode() { return rezimRender; };
	char getShadingMode() { return rezimShading; };

	//batymetria
	double getZScale() { return zScale; };
	void setZScale(double z) { zScale = z; };
};