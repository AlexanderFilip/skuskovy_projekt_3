#pragma once

#include <QtWidgets>
#include "Viz3D.h"

class HalfEdge;

//struct vektor;


class Vertex {
	int index;
	double x, y, z;

	QVector3D Normal;
	QColor Color = QColor(255,255,255);
	HalfEdge* edge;
public:
	Vertex() {};
	Vertex(int i, double surX, double surY, double surZ) { index = i; x = surX; y = surY; z = surZ; };
	bool operator ==(Vertex const& t);

	void setEdge(HalfEdge* E) { edge = E; };
	void setAll(int i, double surX, double surY, double surZ) { index = i; x = surX; y = surY; z = surZ; };
	void setX(double surX) { x = surX; };
	void setY(double surY) { y = surY; };
	void setZ(double surZ) { z = surZ; };

	double getX() { return x; };
	double getY() { return y; };
	double getZ() { return z; };
	int getIndex() { return index; };
	QString PrintSur();

	void setNormal(double X, double Y, double Z) { Normal.setX(X); Normal.setY(Y); Normal.setZ(Z); };
	void addNormal(QVector3D v) { Normal += v; };
	void unifyNormal() { Normal.normalize(); };
	QVector3D* getNormal() { return &Normal; };
	void setColor(QColor* c) { Color = *c; };
	QColor* getColor() { return &Color; };
};



class Face {
	HalfEdge* edge;
public:
	Face() {};
	Face(HalfEdge* E) { edge = E; };
	void setEdge(HalfEdge* E) { edge = E; };
	HalfEdge* getHrana() { return edge; };

	QString PrintBody();
};


class HalfEdge {
	Vertex* vert_origin;
	Vertex* vert_end;		//iba pre import
	Face* face;
	HalfEdge* edge_prev, * edge_next;
	HalfEdge* pair;
public:
	HalfEdge() { face = nullptr; };
	HalfEdge(Vertex* vo, Face* f, HalfEdge* ep, HalfEdge* en, HalfEdge* pr);
	bool hasFace();
	bool hasPair();

	//SET
	void setAll(Vertex* vo, Face* f, HalfEdge* ep, HalfEdge* en, HalfEdge* pr = nullptr);

	void setPoints(Vertex* vo, Vertex* ve, HalfEdge* pr = nullptr);
	void setFace(Face* f) { face = f; };
	void setEdgePrev(HalfEdge* ep) { edge_prev = ep; };
	void setEdgeNext(HalfEdge* en) { edge_next = en; };
	void setPair(HalfEdge* pr) { pair = pr; };

	//PRINT
	QString PrintBodyHrany();
	QString PrintBodySteny();

	//STENA
	Face* getStena() { return face; };


	//BOD
	Vertex* getBod() { return vert_origin; };
	int getBodIndex() { return vert_origin->getIndex(); };
	int getBodEndIndex() { return vert_end->getIndex(); };

	//HRANA
	HalfEdge* getHrana_next() { return edge_next; };
	HalfEdge* getHrana_prev() { return edge_prev; };
	HalfEdge* getHrana_pair() { return pair; };
};



class GeomObjekt {
	QList<Vertex>* Vrcholy;
	QList<Face>* Steny;
	QList<HalfEdge>* Hrany;

	//Material
	QColor Diffuse, Glossy, Ambient;
	double Roughness;

	//Mapa
	QVector<QColor> FarbyVrcholov;
	QVector<double> HeightBuffer;

public:
	GeomObjekt();
	~GeomObjekt();
	
	bool isNull();
	void Clear();


	void setVrcholy(QList<Vertex>* V) { Vrcholy = V; };
	void setSteny(QList<Face>* S) { Steny = S; };
	void setHrany(QList<HalfEdge>* H) { Hrany = H; };


	//pridavanie
	void addVrchol(Vertex* v) { Vrcholy->append(*v); };
	void addHrana(HalfEdge* e) { Hrany->append(*e); };
	void addStena(Face* s) { Steny->append(*s); };

	//mazanie
	void deleteHranyBatch(int g);
	void deleteStenyBatch(int g);

	//vrcholy
	int getVrcholySize() { return Vrcholy->size(); };
	QString PrintVrcholySurAt(int i) { return (*Vrcholy)[i].PrintSur(); };
	double getVrcholSurZAt(int i) { return (*Vrcholy)[i].getZ(); };
	Vertex* DuplicitnyVrchol(Vertex* v, int g);
	void normalizujVrcholy();

	void preskalujVrcholy(double factor);
	double VzdialenostVrcholov(Vertex* u, Vertex* v);
	bool containsVrchol(Vertex v);
	void ScaleZ(double factor);
	void FillHeightBuffer();

	//hrany
	int getHranySize() { return Hrany->size(); };
	HalfEdge* getHranaAt(int i) { return &(*Hrany)[i]; };
	QString PrintHranyBodyAt(int i) { return (*Hrany)[i].PrintBodyHrany(); };
	void najdiParoveHrany();
	int getPairHranaIndexAt(int i) { return (*Hrany)[i].getHrana_pair()->getBodIndex(); };
	int getHranaIndexAt(int i) { return (*Hrany)[i].getBodIndex(); };


	//steny
	int getStenySize() { return Steny->size(); };
	Face* getStenaAt(int i) { return &(*Steny)[i]; };
	QString PrintStenyBodyAt(int i) { return (*Steny)[i].PrintBody(); };

	//Farby
	void setRough(double r) { Roughness = r; };
	void setDiff(QColor c) { Diffuse = c; };
	void setGloss(QColor c) { Glossy = c; };
	void setAmb(QColor c) { Ambient = c; };

	double getRough() { return Roughness; };
	QColor* getDiff() { return &Diffuse; };
	QColor* getGloss() { return &Glossy; };
	QColor* getAmb() { return &Ambient; };

	//normaly vrcholov
	void CalculateVertexNormals();
	QVector3D* getNormalAt(int i) { return (*Vrcholy)[i].getNormal(); };

	//render
	void CalculateVertexColors(Vizualizacia* Kamera);
	void CalculateVertexColors_Batymetry();

	//render - pomocne
	QVector3D CalcL(Vertex* P, Vizualizacia* Kamera);
	QVector3D CalcV(Vertex* P, Vizualizacia* Kamera);
	int Max(int a, int b) { if (a > b) return a; else return b; };
	int Min(int a, int b) { if (a > b) return b; else return a; };

	QColor VypocitajIs(QVector3D* V, QVector3D* R, QColor* Il, QColor* rs, double h);
	QColor VypocitajId(QVector3D* L, QVector3D* N, QColor* Il, QColor* rd);
	QColor VypocitajIa(QColor* Io, QColor* ra);
	QColor VypocitajI(QColor* Is, QColor* Id, QColor* Ia);

	
};