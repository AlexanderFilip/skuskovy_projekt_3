#pragma once

#include <QtWidgets/QMainWindow>
#include <QtWidgets>
#include "ui_ImageViewer.h"
#include "Viz3D.h"
#include "ViewerWidget.h"
#include "NewImageDialog.h"
#include "Geometry_Structures.h"


struct HermitVektor;
class HalfEdge;

class GeomObjekt;
class Vertex;
class Face;
class HalfEdge;
class Vizualizacia;



class ImageViewer : public QMainWindow
{
	Q_OBJECT

public:
	ImageViewer(QWidget* parent = Q_NULLPTR);

private:
	Ui::ImageViewerClass* ui;
	NewImageDialog* newImgDialog;

	QSettings settings;
	QMessageBox msgBox;

	QVector<QPointF> Suradnice;
	QVector<HermitVektor> HVektory;

	QVector<QPoint> SurCursor;			//0=povodne, 1=aktualne
	QColor PainterColor = (QColor("black"));
	QColor FillColor=(QColor("red"));
	int CurveAlg = 0;					//index algoritmu pre krivky
	int LineAlg = 0;					//index algoritmu pre usecky

	GeomObjekt* GenObjekt=nullptr;
	GeomObjekt* ImpObjekt=nullptr;

	Vizualizacia Kamera;
	QList<QVector<zBufferUnit>>* Objekt3DSur=nullptr;


	//ViewerWidget functions
	ViewerWidget* getViewerWidget(int tabId);
	ViewerWidget* getCurrentViewerWidget();

	//Event filters
	bool eventFilter(QObject* obj, QEvent* event);

	//ViewerWidget Events
	bool ViewerWidgetEventFilter(QObject* obj, QEvent* event);
	void ViewerWidgetMouseButtonPress(ViewerWidget* w, QEvent* event);
	void ViewerWidgetMouseButtonRelease(ViewerWidget* w, QEvent* event);
	void ViewerWidgetMouseMove(ViewerWidget* w, QEvent* event);
	void ViewerWidgetLeave(ViewerWidget* w, QEvent* event);
	void ViewerWidgetEnter(ViewerWidget* w, QEvent* event);
	void ViewerWidgetWheel(ViewerWidget* w, QEvent* event);

	//ImageViewer Events
	void closeEvent(QCloseEvent* event);

	//Image functions
	void openNewTabForImg(ViewerWidget* vW);
	bool openImage(QString filename);
	bool saveImage(QString filename);
	void clearImage();
	void setBackgroundColor(QColor color);

	//Inline functions
	inline bool isImgOpened() { return ui->tabWidget->count() == 0 ? false : true; }

	//NACITANIE DAT
	void LoadUIInput();

	//Vyplnenie geom. objektu
	void GenerateIcosahedron(GeomObjekt* Objekt_Gen, int hustota=0);
	void SubdivideIcosahedron(GeomObjekt* Objekt_Gen, int hustota);

	bool ExportObject(GeomObjekt* O);
	bool ImportObject(GeomObjekt* O);


	//3D vizualizacia
	void Refresh3D();
	void Calculate3DObject(GeomObjekt* O, QList<QVector<zBufferUnit>>* Sur3d);
	void CalculatePerspective(GeomObjekt* O, QList<QVector<zBufferUnit>>& Sur3d);
	void CalculateIsometric(GeomObjekt* O, QList<QVector<zBufferUnit>>& Sur3d);
	double calcDistance(double x, double y, double z);


	//Kontrola Formatovania
	bool CheckVtkHeader(QFile* VtkObj);
	bool FillVtkPoints(QFile* VtkObj, QList<Vertex>* Verts);
	bool FillVtkEdges(QFile* VtkObj, QList<HalfEdge>* Edges, QList<Vertex>* Verts);
	bool FillVtkEdgesInfo(QList<Face>* Faces, QList<HalfEdge>* Edges, QList<Vertex>* Verts, QList<QList<int>>* BodyStien);
	bool FillVtkFaces(QFile* VtkObj, QList<Face>* Faces, QList<HalfEdge>* Edges, QList<Vertex>* Verts);

	
	//Batymetria
	//Import Dat
	bool FillDatPoints(QFile* ObjectFile, QList<Vertex>* Verts);
	bool FillDatEdges(QList<Vertex>* Verts, QList<HalfEdge>* Edges, QList<Face>* Faces);
	bool RescaleDatPoints(QList<Vertex>* Verts);



private slots:
	//Tabs slots
	void on_tabWidget_tabCloseRequested(int tabId);
	void on_actionRename_triggered();

	//Image slots
	void on_actionNew_triggered();
	void newImageAccepted();
	void on_actionOpen_triggered();
	void on_actionSave_as_triggered();
	void on_actionClear_triggered();
	void on_actionSet_background_color_triggered();

	void on_Farba_pressed();
	void on_Farba_vypln_pressed();
	void on_Vypln_currentIndexChanged(int);
	void on_Vyber_currentIndexChanged(int);

	//Curves
	void on_RadioHermit_pressed();
	void on_RadioBezier_pressed();
	void on_RadioBspline_pressed();

	//Line ALGORITMY
	void on_DDA_pressed();
	void on_Bresenham_pressed();

	void on_ButtonNovy_pressed();
	void on_ButtonRot_pressed();
	void on_ButtonScale_pressed();
	void on_ButtonShear_pressed();
	void on_ButtonSymm_pressed();


	//Import, Export
	void on_VtkExport_pressed();
	void on_VtkImport_pressed();
	
	void on_GenIcos_pressed();

	//Value Changes
	void on_SpinRot_valueChanged(double);
	void on_SliderRot_valueChanged(int);

	void on_SpinScale_valueChanged(double);
	void on_SliderScale_valueChanged(int);

	void on_SpinShear_valueChanged(double);
	void on_SliderShear_valueChanged(int);
	
	void on_SpinCurveRot_valueChanged(double);
	void on_SliderCurveRot_valueChanged(int);

	void on_SpinCurveLength_valueChanged(double);
	void on_SliderCurveLength_valueChanged(int);

	void on_SpinPoint_valueChanged(int);

	//3D Camera

	void on_zenit_valueChanged(double);
	void on_SliderZenit_valueChanged(int);

	void on_azimut_valueChanged(double);
	void on_SliderAzimut_valueChanged(int);

	void on_prem_rovn_pressed();
	void on_prem_stred_pressed();

	void on_clip_near_valueChanged(double);
	void on_SliderClipNear_valueChanged(int);

	void on_clip_far_valueChanged(double);
	void on_SliderClipFar_valueChanged(int);

	void on_stred_prem_valueChanged(double);
	void on_SliderStredPrem_valueChanged(int);

	void on_transX_valueChanged(int);
	void on_transY_valueChanged(int);
	void on_SliderScale3D_valueChanged(int);

	void on_radio_Gen_pressed();
	void on_radio_Imp_pressed();

	//Render
	void on_radioRendered_pressed();
	void on_radioWireframe_pressed();

	void on_radioGouraud_pressed();
	void on_radioKonst_pressed();

	//Svetlo
	void on_PointX_valueChanged(double);
	void on_PointY_valueChanged(double);
	void on_PointZ_valueChanged(double);

	void on_PointColor_pressed();
	void on_AmbColor_pressed();

	//Material
	void on_spinRoughness_valueChanged(double);
	void on_matDiff_pressed();
	void on_matGloss_pressed();
	void on_matAmb_pressed();

	//MAPA
	void on_MapaImport_pressed();
	void on_ZScaleSlider_valueChanged(int);
	void on_DialSvetlo_valueChanged(int);
};
